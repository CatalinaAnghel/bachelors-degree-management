# README #

##Smart Project Allocation##

Smart Project Allocation (SPA) is a platform used for the project allocation process.

By using SPA, the students will be able to choose the best supervisor based on their personalities, domain keys and the compatibility of the preferred supervisor style.
Furthermore, the professors will be able to add/delete projects and to sort the received options.