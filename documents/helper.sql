use BDMGDPRDatabase;

update Deadlines set IsAllocationMade =  0 where Id=3;
update Professors set ProjectsNumber = 20 where ProjectsNumber = 0;
update Options set Status = 'waiting' where DeadlineId = 3;
update CustomOptions set Status = 'waiting' where DeadlineId = 3;

(select StudentId, Status, Discriminator, Place, NumberForUser, DeadlineId
from Options)
union
(select StudentId, Status, Discriminator, Place, NumberForUser, DeadlineId
from CustomOptions
)
order by DeadlineId desc;

select * from Professors;