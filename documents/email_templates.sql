insert into EmailTemplates (Usage, Body) 
values(
'email_base', 
'<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Demo</title>
    <meta name="viewport" content="width=device-width" />
    <style type="text/css">
        @import url(''https://fonts.googleapis.com/css2?family=Catamaran&display=swap'');

        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: ''Catamaran'', sans-serif;
            background-color: gainsboro;
        }
        p{
            font-size: larger;
        }
        td{
            padding:0 30px;
        }
    </style>

</head>

<body style=" overflow-x: hidden; text-align: justify;">
    <div class="container"
        style="background-color:ghostwhite; margin-left: 10%; margin-right: 10%; margin-bottom: 30px; margin-top: 30px;">
        <div class="content" style="margin-left: 20px;margin-right: 20px;">
            <br>
            {email_body}
            <br>
        </div>
        <div class="footer" style="background-color: #EE6F2F;"></div>


    </div>
</body>

</html>');
insert into EmailTemplates (Usage, Subject, Body) 
values(
	'accepted',
	'Allocation process results',
	'<h2 style="text-align: center;">Congratulations!</h2>
            <br>
            <hr style="opacity: 0.2;"><br>
            <h3>Hooray! Your option no. {number} was <strong style="color: green">accepted</strong>.</h3>
            <br>
            <p>
                This email was sent by the <strong style="color:#EE6F2F;">SPA system</strong> in order to announce you
                that one of your options was marked as accepted and your allocation journey will meet its end here.
                Now, you can start planning the details with your professor and you can start working on the project.
                Please feel free to ask questions or report problems by sending a mail to <a
                    href="mailto:geo.klein@ethereal.email">Questions or problems</a>.<br />
                You can see the details for your project below.
            </p>
            <br>
            <table class="table" style="width: max-content;">
                <thead>
                </thead>
                <tbody>
                    <tr>
                        <th>
                            Option number
                        </th>
                        <td>
                            {option_number}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Type
                        </th>
                        <td>
                            {option_type}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Professor
                        </th>
                        <td>
                            {professor}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Project
                        </th>
                        <td>
                            {project}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Description
                        </th>
                        <td>
                            {description}
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr style="opacity: 0.2;">
            <br>
            <p>Keep up with the good work and good luck!</p>
            <h4>
                SPA Team :)
            </h4>'
);
insert into EmailTemplates (Usage, Subject, Body) 
values(
	'rejected',
	'Allocation process results',
	'<h2 style="text-align: center;">Hello from the SPA team!</h2>
<br>
<h3>We are sorry to inform you that your option no. {number} was <strong style="color: red">rejected</strong>.</h3>
<br>
<p>
    This email was sent by the <strong style="color:#EE6F2F;">SPA system</strong> in order to announce you
    that one of your options was marked as rejected, but don''t worry, the allocation process will not meet its end here.
    Our scope is to give you the best experience in this process filled with emotions and hope.
    Please feel free to ask questions or report problems by sending a mail to <a
        href="mailto:geo.klein@ethereal.email">Questions or problems</a>.<br />
    You can see the details for the rejected project below.
</p>
<br>
<table class="table" style="width: max-content;">
    <thead>
    </thead>
    <tbody>
        <tr>
            <th>
                Option number
            </th>
            <td>
                {option_number}
            </td>
        </tr>
        <tr>
            <th>
                Type
            </th>
            <td>
                {option_type}
            </td>
        </tr>
        <tr>
            <th>
                Professor
            </th>
            <td>
                {professor}
            </td>
        </tr>
        <tr>
            <th>
                Project
            </th>
            <td>
                {project}
            </td>
        </tr>
        <tr>
            <th>
                Description
            </th>
            <td>
                {description}
            </td>
        </tr>
    </tbody>
</table>
<hr style="opacity: 0.2;">
<br>
<p>Do not be sad, get ready for the next steps.</p>
<h4>
    SPA Team :)
</h4>'
);