create trigger after_delete_student
on [dbo].[Students]
after delete
as
declare @id nvarchar(13);
select @id = del.CNP from deleted del;
exec delete_custom_options @id;
exec delete_options @id;
