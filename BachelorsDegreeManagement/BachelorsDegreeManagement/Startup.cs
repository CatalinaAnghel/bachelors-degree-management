using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BachelorsDegreeManagement.DataAccess.Data;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.DataAccess.Repositories;
using BachelorsDegreeManagement.ApplicationLogic.HostedServices.Workers.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.HostedServices.Workers;
using BachelorsDegreeManagement.ApplicationLogic.HostedServices;
using NLog;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace BachelorsDegreeManagement
{
    public class Startup
    {
        readonly string allowSpecificOrigins = "_allowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            // logger
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // add the database context
            services.AddDbContext<BachelorsDegreeManagementDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            // add identity
            services.AddIdentity<Users, IdentityRole>()
                .AddEntityFrameworkStores<BachelorsDegreeManagementDbContext>();

            // configure the identity options
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = true;

                options.User.RequireUniqueEmail = true;
            });

            // configure the cookies
            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.SameSite = SameSiteMode.None;
                options.LoginPath = "/Identity/Account/Login";// login page
                options.AccessDeniedPath = "/Identity/Account/AccessDenied"; // the page for the access denied
                options.SlidingExpiration = true;// to reuse a cookie => slidig window
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            // prepare the repositories for Dependency Injection
            services.AddScoped<IStudentsRepository, StudentsRepository>();
            services.AddScoped<IProfessorsRepository, ProfessorsRepository>();
            services.AddScoped<IGroupsRepository, GroupsRepository>();
            services.AddScoped<IPersonalityTestResultsRepository, PersonalityTestResultsRepository>();
            services.AddScoped<IQuestionsRepository, QuestionsRepository>();
            services.AddScoped<ITagsRepository, TagsRepository>();
            services.AddScoped<IUserTagsRepository, UserTagsRepository>();
            services.AddScoped<IOptionsRepository, OptionsRepository>();
            services.AddScoped<ICustomOptionsRepository, CustomOptionsRepository>();
            services.AddScoped<IProjectsRepository, ProjectsRepository>();
            services.AddScoped<IDeadlinesRepository, DeadlinesRepository>();
            services.AddScoped<IProjectTagsRepository, ProjectTagsRepository>();
            services.AddScoped<IDepartmentsRepository, DepartmentsRepository>();
            services.AddScoped<ISystemVariablesRepository, SystemVariablesRepository>();
            services.AddScoped<IEmailTemplatesRepository, EmailTemplatesRepository>();
            services.AddScoped<ISpecializationsRepository, SpecializationsRepository>();

            // prepare the services for Dependency Injection
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IRegisterService, RegisterService>();
            services.AddScoped<IProfessorsService, ProfessorsService>();
            services.AddScoped<IStudentsService, StudentsService>();
            services.AddScoped<IUsersService, UserService>();
            services.AddScoped<IGroupsService, GroupsService>();
            services.AddScoped<IPersonalityTestResultsService, PersonalityTestResultsService>();
            services.AddScoped<IQuestionsService, QuestionsService>();
            services.AddScoped<ITagsService, TagsService>();
            services.AddScoped<IUserTagsService, UserTagsService>();
            services.AddScoped<IOptionsService, OptionsService>();
            services.AddScoped<ICustomOptionsService, CustomOptionsService>();
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<IOptionsFactoryService, OptionsFactoryService>();
            services.AddScoped<IDeadlinesService, DeadlinesService>();
            services.AddScoped<IDepartmentsService, DepartmentsService>();
            services.AddScoped<ISystemVariablesService, SystemVariablesService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IExportService, ExportService>();

            services.AddSingleton<ILoggerManager, LoggerManager>();

            // the hosted services
            services.AddSingleton<IDeadlinesWorker, DeadlinesWorker>();
            services.AddHostedService<DeadlineValidator>();

            // razor pages for Identity

            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/Error/Index");
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
