﻿$(document).ready(() => {
    var projectWeights = $(".project-weights");
    var projectTotal = $(".project-total");
    updateTotal(projectWeights, projectTotal);
    projectWeights.on("change", () => {
        updateTotal(projectWeights, projectTotal);
    });

    var professorWeights = $(".professor-weights");
    var professorTotal = $(".professor-total");
    updateTotal(professorWeights, professorTotal);
    professorWeights.on("change", () => {
        updateTotal(professorWeights, professorTotal);
    });
});

function updateTotal(weights, total) {
    var sum = 0.0;
    weights.each((index, element) => {
        sum += parseFloat($(element).val());
    });
    sum = sum.toFixed(2);
    total.val(sum);
    if (sum == 1.0 && total.hasClass("invalid-total")) {
        total.removeClass("invalid-total").addClass("valid-total");
    } else if(sum != 1.0){
        total.removeClass("valid-total").addClass("invalid-total");
    }
}