﻿$(document).ready(() => {
    if (window.location.pathname === "/Options/Sort") {
        $(".alert-success").hide();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        var cards = $(".cards");
        if (cards.length) {
            var maxNumberOfOptions = $("#maxNumberOfOptions").val();
            // the cards exists
            cards.sortable({
                update: function (event, ui) {
                    ($(this).children()).each(function (index) {
                        if ($(this).attr("data-position") != (index + 1)) {
                            // the element was moved
                            $(this).attr("data-position", (index + 1));
                            if (parseInt($(this).attr("data-position")) <= parseInt(maxNumberOfOptions)) {
                                $(this).removeClass("card-rejected-option").addClass("card-accepted-option");
                            } else {
                                $(this).removeClass("card-accepted-option").addClass("card-rejected-option");
                            }
                        }
                    });
                },
                axis: "y",
                cursor: "move",
                delay: 200
            });
        }
    }
});

/*
  Function used to send the options list
 */
(function ($) {
    var btn = $(".submit-options-button");
    if (btn.length) {
        btn.on("click", (event) => {
            var cards = $(".cards");
            var data = [];
            cards.children().each(function (index) {
                data.push({
                    id: $(this).attr("data-index"),
                    position: $(this).attr("data-position")
                });
            });
            $.ajax({
                url: "../Options/Result",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json;charset=utf-8",
                success: function () {
                    $("html, body").animate({ scrollTop: 0 }, "slow").after(() => {
                        $(window).off("scroll").scroll(() => {
                            if ($(window).scrollTop() == 0) {
                                $(".alert-success").fadeIn("slow").delay(3000).fadeOut("slow");
                                setTimeout(() => {
                                    window.location.reload();
                                }, 4000);
                            }
                        });
                        
                    });
                },
                error: function () {
                    console.log("Oh, snap... Something went wrong on our side :(");
                }
            });
        });
    }
}(jQuery));