﻿$(document).ready(function () {
    $('#professorsTable').DataTable({ "order": []});
    $('#tagsTable').DataTable();
    $('#userTagsTable').DataTable();
    $('#studentsTable').DataTable({ "order": [] });
    $('#tagsTable').DataTable();
    $('#projectsTable').DataTable({ "order": [] });
    $('#optionsTable').DataTable();
    $('#customOptionsTable').DataTable();
    $('#deadlinesTable').DataTable();
    $('#departmentsTable').DataTable();
});