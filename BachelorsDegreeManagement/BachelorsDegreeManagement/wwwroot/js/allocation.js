﻿$(document).ready(() => {
    var allocateBtn = $(".allocate-projects");
    if (allocateBtn.length) {
        allocateBtn.on("click", (event) => {
            event.preventDefault();
            event.stopPropagation();
            var response = confirm("Are you sure that you want to trigger the allocation process?");
            if (response) {
                step1();
            }
        });
    }
});

function step1() {
    $("#loader").show();
    $.ajax({
        url: '../Options/Step1',
        method: 'GET',
        success: function (data) {
            if (data) {
                step2();
            } else {
                $("#loader").fadeOut();
                alert("The allocation mechanism has been finished and a new iteration is not needed.");
            }
        },
        error: function (data) {
            console.log("Something went wrong...");
            $("#loader").fadeOut();
        }
    });
}

function step2() {
    $("#loader").show();
    $.ajax({
        url: '../Options/Step2',
        method: 'GET',
        success: function (data) {
            $("#loader").fadeOut();
            if (data != true) {
                alert("There are students that do not have a project. A new iteration should be made.");
            } else {
                window.location.reload();
            }
        },
        error: function (data) {
            console.log(data);
            $("#loader").fadeOut();
        }
    });
}