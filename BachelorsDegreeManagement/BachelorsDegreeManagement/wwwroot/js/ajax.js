﻿// function used in /Options/Create to get all the projects based on the type of the option and the professor
function getProjects() {
    var professor = { id: $("#professor-field").val() };
    var type = $("#type-field").val();
    console.log(professor);
    if (type == "Proposed" && professor.id != "Choose the professor") {
        $("#project-label").css("display", "block");
        $("#project-field").css("display", "block");
        $("#project-field").prop("required", true);
        $("#description-field").prop("required", false);
        $.ajax({
            url: "../Projects/GetProjects",
            type: "GET",
            data: professor,
            success: function (data) {
                $("#project-field").empty();
                console.log(data);
                if (data.length == 0) {
                    $("#project-field").append("<option>There are no proposed projects</option>");
                } else {
                    data.forEach(function (element) {
                        $("#project-field").append("<option>" + element.name + "</option>");
                    });
                }
                console.log(data);
            },
            error: function () {
                $("#project-field").empty();
               
                console.log("Something went wrong");
            }
        });
    } else {
        $("#project-field").prop("required", false);
        $("#project-field").empty();
        $("#project-field").append("<option selected>0</option>");
        $("#description-field").prop("required", true);
        $("#project-label").css("display", "none");
        $("#project-field").css("display", "none");
    }
}

// function used to search a person
/* a student can search a professor
 * a professor can search a student
 * the administrator can search professors and students*/
function search() {
    var name = { name: $("#search-field").val() };
    console.log(name);
    $.ajax({
        url: "../Home/Search",
        method: "GET",
        data: name,
        success: function (data) {
            console.log(data);
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}
