﻿$(document).ready(() => {
    $("#add-specialization").on("click", () => {
        add_specialization();
    });
    $("#remove-specialization").on("click", () => {
        remove_specialization();
    });
});

function add_specialization() {
    var specialization = $("#specialization-field").val();
    var specializations = $("#specializations-field").val();
    var groups = $("#groups-field").val(); console.log(groups);
    var specializations = specializations.split("; ");

    var found = false;
    for (iterator = 0; iterator < specializations.length && !found; iterator++) {
        if (specializations[iterator] == specialization + ", " + groups) {
            found = true;
        }
    }
    if (!found) {
        $("#specializations-field").val($("#specializations-field").val() + specialization + ", " + groups + "; ");
    }
}

function remove_specialization() {
    var specialization = $("#specialization-field").val();
    var specializations = $("#specializations-field").val();
    var groups = $("#groups-field").val();
    var rem_specializations = specializations.split("; ");
    $("#specializations-field").val('');
    for (iterator = 0; iterator < rem_specializations.length - 1; iterator++) {
        if (rem_specializations[iterator] != specialization + ", " + groups) {
            $("#specializations-field").val($("#specializations-field").val() + rem_specializations[iterator] + "; ");
        }
    }
}