﻿const importService = {
    setup: function () {
        this.bindImportEvents();
        this.bindExportEvents();
    },
    bindImportEvents: function () {
        var importModalTrigger = $("#import-modal-trigger");
        if (importModalTrigger.length) {
            importModalTrigger.on("click", () => {
                $(function () {
                    var modal = $("#students-import-dialog");
                    modal.dialog({
                        resizable: true,
                        modal: true,
                        width: '60%',
                        fluid: true
                    });
                    $('body').addClass('stop-scrolling');
                    $("#import-submit-button").click(function () {
                        modal.dialog("close");
                        $('body').removeClass('stop-scrolling');
                    });
                });
            });
        }

        var importModalTrigger = $("#professors-import-modal-trigger");
        if (importModalTrigger.length) {
            importModalTrigger.on("click", () => {
                $(function () {
                    var modal = $("#professors-import-dialog");
                    modal.dialog({
                        resizable: true,
                        modal: true,
                        width: '60%',
                        fluid: true
                    });
                    $('body').addClass('stop-scrolling');
                    $("#import-submit-button").click(function () {
                        modal.dialog("close");
                        $('body').removeClass('stop-scrolling');
                    });
                });
            });
        }
    },
    bindExportEvents: function () {
        var importModalTrigger = $("#export-students");
        if (importModalTrigger.length) {
            this.export(importModalTrigger, '../Students/Export');
        } else {
            importModalTrigger = $("#export-supervisors");
            if (importModalTrigger.length) {
                this.export(importModalTrigger, '../Professors/Export');
            }
        }
    },
    export: function (element, url) {
        if (element.length) {
            element.on("click", () => {
                $("#loader").show();
                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function (data) {
                        $("#loader").fadeOut();
                        window.open(data, '_blank');
                    },
                    error: function (data) {
                        console.log("Something went wrong...");
                        $("#loader").fadeOut();
                    }
                });
            });
        }
    }
}

$(document).ready(() => {
    importService.setup();
});