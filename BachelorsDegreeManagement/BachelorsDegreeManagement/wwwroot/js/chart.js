﻿var surgencyScore = 0;
var agreeablenessScore = 0;
var emotionalScore = 0;
var conscientScore = 0;
var imaginationScore = 0;
$.ajax({
    url: "/PersonalityTestResults/GetScores",
    type: "GET",
    success: function (data) {
        console.log(data);
        if (data != null) {
            surgencyScore = data.surgencyScore;
            agreeablenessScore = data.agreeablenessScore;
            emotionalScore = data.emotionalScore;
            conscientScore = data.conscientScore;
            imaginationScore = data.imaginationScore;
        }
    },
    error: function () {
        console.log("Something went wrong");
    }
});

// Load the Visualization API and the corechart package.
google.charts.load('current', { 'packages': ['corechart'] });

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

    // Create the data table.
    var chartData = new google.visualization.DataTable();
    chartData.addColumn('string', 'Category');
    chartData.addColumn('number', 'Score');
    chartData.addRows([
        ['Surgency', surgencyScore],
        ['Agreeableness', agreeablenessScore],
        ['Emotional', emotionalScore],
        ['Conscientiousness', conscientScore],
        ['Imagination', imaginationScore]
    ]);

    // Set chart options
    var options = {
        'title': 'Your results for the Big Five Personality Test',
        'width': 1220,
        'height': 500,
        colors: ['#ee6f2f'],
        tooltip: { isHtml: true }
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(chartData, options);
}