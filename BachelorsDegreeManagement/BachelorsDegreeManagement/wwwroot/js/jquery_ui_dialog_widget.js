﻿$(document).ready(() => {
    var info = $("#info");
    if (info.length) {
        info.on("click", () => {
            $(function () {
                $("#info-dialog").dialog({
                    resizable: true,
                    modal: true,
                    width: '60%',
                    fluid: true
                });
            });
        });
    }
});