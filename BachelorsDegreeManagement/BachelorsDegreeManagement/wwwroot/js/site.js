﻿$(document).ready(() => {
    $("#loader").show();
    var calendar = $(".calendar");
    if (calendar.length) {
        calendar.fullCalendar({
            header: {
                left: 'prev, next',
                center: 'title',
                right: 'today'
            },
            eventClick: (event) => {
                if (event != "There are no deadlines") {
                    $("#deadline-title").html("Deadline");
                    $("#deadline-description").html(event.description);
                }
                
                return false;
            },
            events: '/Deadlines/Get',
        });
    }
    closeSidebarMenu();
    add_back_button_effects();
});

$(window).on("load", () => {
    $("#loader").fadeOut();
});

function show_info(object) {
    object.children[0].style.height = "100px";
    object.children[0].style.marginLeft = "50px";
    object.children[2].style.display = "block";
}

function hide_info(object) {
    object.children[0].style.height = "200px";
    object.children[0].style.marginLeft = "0px";
    object.children[2].style.display = "none";
}

function openSidebarMenu() {
    $("#content").attr("class", "col-md-9");
    $("#mySidebar").css("display", "block");
    $(".close_sidebar_button").css("display", "block");
    $(".open_sidebar_button").css("display", "none");
}

function closeSidebarMenu() {
    $("#content").attr("class", "col-md-12");
    $("#mySidebar").css("display", "none");
    $(".close_sidebar_button").css("display", "none");
    $(".open_sidebar_button").css("display", "block");
}

function changeColor(color) {
    $(".navbar-brand").css("color", color);
}

function add_tag(event) {

    var tag = $("#tag-field").val();
    var tags = $("#tag-textarea").val();
    var tags = tags.split("; ");

    var found = false;
    for (iterator = 0; iterator < tags.length && !found; iterator++) {
        if (tags[iterator] == tag) {
            found = true;
        }
    }
    if (!found) {
        $("#tag-textarea").val($("#tag-textarea").val() + tag + "; ");
    }
}

function remove_tag() {

    var tag = $("#tag-field").val();
    var tags = $("#tag-textarea").val();
    var rem_tags = tags.split("; ");
    $("#tag-textarea").val('');
    for (iterator = 0; iterator < rem_tags.length - 1; iterator++) {
        if (rem_tags[iterator] != tag) {
            $("#tag-textarea").val($("#tag-textarea").val() + rem_tags[iterator] + "; ");
            console.log(rem_tags[iterator]);
        }
    }
}

function add_back_button_effects() {
    if ($("#back-button").length > 0) {
        $("#back-button a").hide();
        $("#back-button-bean").on("click", function () {
            $("#back-button a").toggle("slide", { direction: "right" });
        });
    }
}