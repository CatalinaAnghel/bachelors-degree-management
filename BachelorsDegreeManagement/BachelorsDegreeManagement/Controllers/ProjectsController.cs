﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace BachelorsDegreeManagement.Controllers
{
    public class ProjectsController : Controller
    {
        private readonly IProjectsService _projectsService;
        private readonly IUsersService _usersService;
        private readonly ITagsService _tagsService;
        private readonly ISystemVariablesService _variablesService;
        private readonly IProfessorsService _professorsService;
        private readonly UserManager<Users> _userManager;
        private readonly IPersonalityTestResultsService _testService;

        public ProjectsController(
            IProjectsService projectsService,
            IUsersService usersService,
            ITagsService tagsService,
            ISystemVariablesService variablesService,
            IProfessorsService professorsService,
            UserManager<Users> userManager,
            IPersonalityTestResultsService testResultsService)
        {
            _projectsService = projectsService;
            _usersService = usersService;
            _tagsService = tagsService;
            _variablesService = variablesService;
            _userManager = userManager;
            _professorsService = professorsService;
            _testService = testResultsService;
        }

        [Authorize(Roles = "Professor, Student")]
        // GET: Projects
        public async Task<IActionResult> Index()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            var role = (await _userManager.GetRolesAsync(user))[0];
            if (role.Equals("Professor"))
            {
                var projects = await _projectsService.GetProjects(user);
                var projectsViewModel = await _projectsService.GetProjectsViewModelList(projects, user, true);
                return View(projectsViewModel);
            }
            else
            {
                bool valid = await _testService.CheckPersonalityTest(user);
                if (valid)
                {
                    var projects = await _projectsService.GetAllProjects();
                    var projectsViewModel = await _projectsService.GetProjectsViewModelList(projects, user, false);

                    return View(projectsViewModel);
                }
                else
                {
                    return RedirectToAction("Create", "PersonalityTestResults");
                }
               
            }
        }

        [Authorize]
        public async Task<IActionResult> GetProjects(string id)
        {
            var user = await _usersService.GetUser(id);
            var projects = await _projectsService.GetProjectsWithMinimalInfo(user);
            return Json(projects);
        }

        [Authorize(Roles = "Professor, Student")]
        // GET: Projects/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var project = await _projectsService.GetProject(id);
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            var role = (await _userManager.GetRolesAsync(user))[0];
            bool isProfessor = role.Equals("Professor");
            var model = await _projectsService.GetProjectViewModel(project, user, isProfessor);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [Authorize(Roles = "Professor")]
        // GET: Projects/Create
        public async Task<IActionResult> Create()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            CreateProjectViewModel model = new CreateProjectViewModel();

            var tags = _tagsService.GetUsersTags(user);
            var complexities = _variablesService.GetVariableLikeName("complexityLevel");
            if (tags == null || tags.Count == 0)
            {
                // redirect to the functionality used to add a tag into the user's list
                return RedirectToAction("Create", "UserTags");
            }
            else
            {
                // the users have tags in their list, so they can add a project
                ViewData["Tags"] = new SelectList(tags, "Name", "Name");
                ViewData["Complexities"] = new SelectList(complexities, "Value", "Value");
                return View(model);
            } 
        }

        // POST: Projects/Create
        [Authorize(Roles = "Professor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateProjectViewModel model)
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            
            if (_projectsService.CheckDeadline())
            {
                if (ModelState.IsValid && !model.Complexity.Equals("Choose the project complexity"))
                {
                    if (await _professorsService.CheckAvailability(user))
                    {
                        _projectsService.AddProject(model, user);
                        ViewData["success"] = "The project has been successfully added";
                    }
                    else
                    {
                        ViewData["error"] = "You are not available anymore...";
                    }
                }
                else
                {
                    ViewData["error"] = "The data is invalid";
                }
            }
            else
            {
                ViewData["error"] = "Too late, the deadline has passed...";
            }

            var tags = _tagsService.GetUsersTags(user);
            ViewData["Tags"] = new SelectList(tags, "Name", "Name");
            var complexities = _variablesService.GetVariableLikeName("complexityLevel");
            ViewData["Complexities"] = new SelectList(complexities, "Value", "Value");
            return View(model);
        }

        [Authorize(Roles = "Professor")]
        // GET: Projects/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var project = await _projectsService.GetProject(id);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }


        [Authorize(Roles = "Professor")]
        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_projectsService.CheckDeadline())
            {
                var project = await _projectsService.GetProject(id);
                _projectsService.DeleteProject(project);
                return RedirectToAction(nameof(Index));
            }
            else {
                ViewData["error"] = "It is too late to perform this action";
                return View();
            }
        }

        [Authorize]
        private async Task<bool> ProjectsExists(int id)
        {
            return (await _projectsService.GetProject(id)) != null;
        }
    }
}
