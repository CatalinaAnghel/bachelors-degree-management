﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;

namespace BachelorsDegreeManagement.Controllers
{
    [Authorize(Roles="Administrator")]
    public class SystemVariablesController : Controller
    {
        private readonly ISystemVariablesService _variablesService;
        private readonly ILoggerManager _logger;

        public SystemVariablesController(ISystemVariablesService variablesService, ILoggerManager logger)
        {
            _variablesService = variablesService;
            _logger = logger;
        }

        // GET: SystemVariables/Edit
        public IActionResult Edit()
        {
            var model = _variablesService.GetWeights();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UpdateWeightsViewModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    bool updated = _variablesService.UpdateVariables(model);
                    if (updated)
                    {
                        ViewData["success"] = "The weights have been successfully updated";
                    }
                    else
                    {
                        ViewData["error"] = "The weights were not valid. Please try again";
                    }
                }
                catch (DbUpdateConcurrencyException e)
                {
                    _logger.LogError(e.Message);
                }
            }
            return View(model);
        }
    }
}
