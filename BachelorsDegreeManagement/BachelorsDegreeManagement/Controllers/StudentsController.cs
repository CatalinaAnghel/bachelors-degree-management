﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;

namespace BachelorsDegreeManagement.Controllers
{
    public class StudentsController : Controller
    {
        private readonly ILoggerManager _logger;
        private readonly IStudentsService _studentService;
        private readonly IGroupsService _groupsService;
        private readonly IDeadlinesService _deadlinesService;
        private readonly IProfessorsService _professorsService;
        private readonly IOptionsFactoryService _optionsFactoryService;
        private readonly IUsersService _usersService;
        private readonly IProjectsService _projectsService;
        private readonly IPersonalityTestResultsService _testService;
        private readonly IExportService _exportService;

        public StudentsController(ILoggerManager logger, IStudentsService studentsService, IGroupsService groupsService,
            IDeadlinesService deadlinesService, IProfessorsService professorsService, IOptionsFactoryService optionsFactoryService,
            IUsersService usersService, IProjectsService projectsService, IPersonalityTestResultsService testService,
            IExportService exportService)
        {
            _logger = logger;
            _studentService = studentsService;
            _groupsService = groupsService;
            _deadlinesService = deadlinesService;
            _professorsService = professorsService;
            _optionsFactoryService = optionsFactoryService;
            _usersService = usersService;
            _projectsService = projectsService;
            _testService = testService;
            _exportService = exportService;
        }

        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Dashboard()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            if (_deadlinesService.GetNextDeadline() != null)
            {
                ViewData["deadlines"] = 1;
                ViewData["options"] = (await _optionsFactoryService.FindSentCustomOptions(user)).Count() +
                (await _optionsFactoryService.FindSentOptions(user)).Count();
                ViewData["professors"] = (await _professorsService.GetProfessors()).Count();
                ViewData["projects"] = (await _projectsService.GetAllProjects()).Count();
            }

            return View();
        }

        // GET: Students
        [Authorize]
        public async Task<IActionResult> Index()
        {
            StudentsIndexViewModel model = new StudentsIndexViewModel
            {
                Students = await _studentService.GetStudents(),
                ImportViewModel = new StudentsImportViewModel()
            };
            return View(model);
        }

        // GET: Students/Details/5
        [Authorize]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _studentService.GetStudent(id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create()
        {
            ViewData["GroupId"] = new SelectList(await _groupsService.GetGroups(), "Id", "Name");

            return View();
        }

        // POST: Students/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create([Bind("GDPRCode,FirstName,LastName,GroupId,Grade")] Students student)
        {
            if (ModelState.IsValid)
            {
                var result = _studentService.CreateStudent(student);
                if (result)
                {
                    ViewData["success"] = "The student has been registered.";
                }
                else
                {
                    ViewData["error"] = "The student already exists";
                }
            }
            else
            {
                ViewData["error"] = "The data was not valid.";
            }

            ViewData["GroupId"] = new SelectList(await _groupsService.GetGroups(), "Id", "Name");
            return View(student);
        }

        // GET: Students/Edit/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var students = await _studentService.GetStudent(id);
            if (students == null)
            {
                return NotFound();
            }
            ViewData["GroupId"] = new SelectList(await _groupsService.GetGroups(), "Id", "Name");
            return View(students);
        }

        // POST: Students/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(string id, [Bind("GDPRCode,FirstName,LastName,GroupId,Grade")] Students students)
        {
            if (id != students.GDPRCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _studentService.UpdateStudent(students);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await StudentsExists(students.GDPRCode)))
                    {
                        _logger.LogInfo("The student " + students.GDPRCode +" does not exist");
                        return NotFound();
                    }
                    else
                    {
                        _logger.LogError("Student update error");
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupId"] = new SelectList(await _groupsService.GetGroups(), "Id", "Name");
            return View(students);
        }

        // GET: Students/Delete/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var students = await _studentService.GetStudent(id);
            if (students == null)
            {
                return NotFound();
            }

            return View(students);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var student = await _studentService.GetStudent(id);
            if(student != null && student.UserId == null)
            {
                // delete only the students that do not have an account
                _studentService.DeleteStudent(student);
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        private async Task<bool> StudentsExists(string id)
        {
            return await _studentService.GetStudent(id) != null;
        }

        [Authorize(Roles = "Professor")]
        // GET: Students/Suggestions
        [HttpGet]
        public async Task<IActionResult> Suggestions()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            bool valid = await _testService.CheckPersonalityTest(user);
            if (valid)
            {
                var suggestedStudents = await _studentService.FindSuggestedStudents(user);
                return View(suggestedStudents);
            }
            else
            {
                return RedirectToAction("Create", "PersonalityTestResults");
            }
            
        }

        [Authorize(Roles ="Administrator")]
        [HttpPost]
        public async Task<IActionResult> Import(StudentsImportViewModel model) 
        {
            if(model.Document != null&& model.Document.ContentType.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            {
                try
                {
                    await _studentService.Import(model);
                    return RedirectToAction(nameof(Index));
                }catch(Exception e)
                {
                    _logger.LogInfo("The excel file did not use the standard layout");
                }
                
            }
            else
            {
                _logger.LogWarn("The admin did not upload the file");
                ViewData["error"] = "You need to upload an Excel file";
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public async Task<IActionResult> Export()
        {
            _exportService.SetFilePath("\\documents\\export\\students.csv");
            var path = await _exportService.ExportData();

            return Json(path);
        }
    }
}