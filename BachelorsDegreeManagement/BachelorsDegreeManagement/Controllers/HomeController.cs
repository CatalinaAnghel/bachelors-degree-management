﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BachelorsDegreeManagement.Models;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Identity;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using Microsoft.AspNetCore.Authorization;

namespace BachelorsDegreeManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILoggerManager _logger;
        private readonly IUsersService _usersService;
        private readonly UserManager<Users> _userManager;
        private readonly IStudentsService _studentsService;
        private readonly IProfessorsService _professorsService;
        private readonly IDeadlinesService _deadlinesService;
        private readonly IOptionsFactoryService _optionsFactoryService;

        public HomeController(
            ILoggerManager logger,
            IUsersService usersService,
            UserManager<Users> userManager,
            IStudentsService studentsService,
            IProfessorsService professorsService,
            IDeadlinesService deadlinesService,
            IOptionsFactoryService optionsFactoryService)
        {
            _logger = logger;
            _usersService = usersService;
            _userManager = userManager;
            _studentsService = studentsService;
            _professorsService = professorsService;
            _deadlinesService = deadlinesService;
            _optionsFactoryService = optionsFactoryService;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);

            if (user != null)
            {
                return RedirectToAction("RedirectToDashboard", "Home");
            }
            else
            {
                return View();
            }
        }

        public IActionResult AboutUs()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> RedirectToDashboard()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            var role = await _userManager.GetRolesAsync(user);

            if (role[0].Equals("Administrator"))
            {
                return RedirectToAction("Dashboard", "Home");
            }
            else if (role[0].Equals("Professor"))
            {
                return RedirectToAction("Dashboard", "Professors");
            }
            else
            {
                return RedirectToAction("Dashboard", "Students");
            }
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Dashboard()
        {
            if(_deadlinesService.GetNextDeadline() != null)
            {
                ViewData["deadlines"] = 1;
            }
            
            ViewData["options"] = (await _optionsFactoryService.FindCustomOptions()).Count() + (await _optionsFactoryService.FindOptions()).Count();
            ViewData["students"] = (await _studentsService.GetStudents()).Count();
            ViewData["professors"] = (await _professorsService.GetProfessors()).Count();
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            _logger.LogError((Activity.Current?.Id ?? HttpContext.TraceIdentifier).ToString());
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Credits()
        {
            return View();
        }
    }
}
