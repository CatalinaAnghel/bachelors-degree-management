﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.IO;
using System.Text;

namespace BachelorsDegreeManagement.Controllers
{
    public class OptionsController : Controller
    {
        private readonly ILoggerManager _logger;
        private readonly IProjectsService _projectsService;
        private readonly IStudentsService _studentsService;
        private readonly IOptionsFactoryService _optionsFactoryService;
        private readonly IUsersService _usersService;
        private readonly IProfessorsService _professorsService;
        private readonly IDeadlinesService _deadlinesService;
        private readonly ISystemVariablesService _variablesService;
        private readonly UserManager<Users> _userManager;

        public OptionsController(
            ILoggerManager loggerManager,
            IProjectsService projectsService,
            IStudentsService studentsService,
            IOptionsFactoryService optionsFactoryService,
            IUsersService usersService,
            IProfessorsService professorsService,
            IDeadlinesService deadlinesService,
            UserManager<Users> userManager,
            ISystemVariablesService systemVariablesService)
        {
            _logger = loggerManager;
            _projectsService = projectsService;
            _studentsService = studentsService;
            _optionsFactoryService = optionsFactoryService;
            _usersService = usersService;
            _professorsService = professorsService;
            _deadlinesService = deadlinesService;
            _userManager = userManager;
            _variablesService = systemVariablesService;
        }

        [Authorize]
        // GET: Options
        public async Task<IActionResult> Index()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            var role = (await _userManager.GetRolesAsync(user))[0];
            ShowOptionsViewModel model = new ShowOptionsViewModel();
            switch (role)
            {
                case "Professor":
                    model.Options = await _optionsFactoryService.FindReceivedOptions(user);
                    model.CustomOptions = await _optionsFactoryService.FindReceivedCustomOptions(user);
                    break;
                case "Student":
                    model.CustomOptions = await _optionsFactoryService.FindSentCustomOptions(user);
                    model.Options = await _optionsFactoryService.FindSentOptions(user);
                    break;
                case "Administrator":
                    model.CustomOptions = await _optionsFactoryService.FindCustomOptions();
                    model.Options = await _optionsFactoryService.FindOptions();
                    break;
            }

            return View(model);
        }

        #region Allocation process
        // Allocation algorithm:
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Allocation()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Step1()
        {
            _logger.LogInfo("The allocation process starts - step 1");
            var result = _optionsFactoryService.Step1();

            if (result)
            {
                _optionsFactoryService.FinishAllocation();
                _logger.LogInfo("The allocation process has been finished");
            }
            return Json(result);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Step2()
        {
            _logger.LogInfo("The allocation process - step 2");

            var result = _optionsFactoryService.Step2();
            if (!result)
            {
                _logger.LogInfo("A new iteration is needed");
            }
            return Json(result);
        }
        #endregion

        #region Options details
        // GET: Options/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var option = await _optionsFactoryService.FindOption((int)id);
            if (option == null)
            {
                return NotFound();
            }

            return View(option);
        }

        // GET: Options/DetailsCustomOption/5
        [Authorize]
        public async Task<IActionResult> DetailsCustomOption(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var option = await _optionsFactoryService.FindCustomOption(id);
            if (option == null)
            {
                return NotFound();
            }

            return View(option);
        }
        #endregion

        #region Create option
        [Authorize(Roles = "Student")]
        // GET: Options/Create
        public async Task<IActionResult> Create()
        {
            var professors = await _professorsService.GetProfessors();
            ViewData["Professors"] = new SelectList(professors, "UserId", "FullName");
            CreateOptionViewModel model = new CreateOptionViewModel();
            return View(model);
        }

        // POST: Options/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Create(CreateOptionViewModel model)
        {
            if (ModelState.IsValid && !model.ProfessorId.Equals("Choose the professor"))
            {
                var user = await _usersService.GetCurrentUser(HttpContext.User);
                var result = await _optionsFactoryService.RegisterOption(user, model);
                if (result)
                {
                    ViewData["success"] = "The option was registered.";
                }
                else
                {
                    ViewData["error"] = "There is no deadline set or you sent too many options...";
                }
            }
            else
            {
                ViewData["error"] = "The data is not valid";
            }
            ViewData["Professors"] = new SelectList(await _professorsService.GetProfessors(), "UserId", "FullName");
            return View(model);
        }
        #endregion

        #region Sorting Options
        // GET: Options/Sort
        [HttpGet]
        [Authorize(Roles = "Professor")]
        public async Task<IActionResult> Sort(bool sent = false)
        {
            ViewData["success"] = "The options have been sorted.";
            
            var deadline = _deadlinesService.GetRecentDeadline();
            SortOptionsViewModel model = new SortOptionsViewModel
            {
                Options = new System.Collections.ArrayList()
            };


            if (deadline == null || (DateTime.Now > deadline.Date.AddDays(Int32.Parse(_variablesService.GetVariableByName("SortingNumberOfDays").Value)) && !sent))
            {
                var nextDeadline = _deadlinesService.GetNextDeadline();
                if (nextDeadline != null)
                {
                    ViewData["error"] = "Well, it is too early for that. Please check the deadlines and you will see when you can sort the options...";
                }
                else
                {
                    ViewData["error"] = "Too late, the deadline passed, please talk with the administrator to change the period of time...";
                }
            }
            else
            {
                var user = await _usersService.GetCurrentUser(HttpContext.User);
                var professor = await _professorsService.GetProfessorByUser(user);
                var options = _optionsFactoryService.GetOptionsForProfessor(professor);
                model.MaxNumberOfProjects = professor.ProjectsNumber;
                model.Options = options != null ? options : null;
            }

            return View(model);
        }

        // POST: Options/Result
        [Authorize(Roles = "Professor")]
        [HttpPost, ActionName("Result")]
        public async Task<IActionResult> Result()
        {
            using StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8);
            string postString = await reader.ReadToEndAsync();

            await _optionsFactoryService.SortOptions(postString);
            return RedirectToAction("Sort", "Options", new { sent = true });

        }
        #endregion

        #region Edit options
        #region Edit regular options
        // GET: Options/Edit/5
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // regular option
            var option = await _optionsFactoryService.FindOption(id);

            if (option == null)
            {
                return NotFound();
            }

            var deadline = _deadlinesService.GetNextDeadline();
            if(deadline != null)
            {
                ViewData["ProjectId"] = new SelectList(await _projectsService.GetAvailableProjects(), "Id", "Name", option.ProjectId);

            }
            else
            {
                ViewData["error"] = "You cannot edit this option anymore...";
            }
            return View(option);
        }

        //POST: Options/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProjectId,Description")] Options option)
        {
            if (id != option.Id)
            {
                return NotFound();
            }
            if (option.ProjectId != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var updated = await _optionsFactoryService.UpdateRegularOption(option);
                        if (!updated)
                        {
                            ViewData["ProjectId"] = new SelectList(await _projectsService.GetAvailableProjects(), "Id", "Name", option.ProjectId);
                            ViewData["error"] = "You cannot edit this option anymore...";
                            return View(option);
                        }
                        return RedirectToAction(nameof(Index));
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        // check if the option exists
                        if (!(await OptionExists(option.Id)))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                ViewData["ProjectId"] = new SelectList(await _projectsService.GetAvailableProjects(), "Id", "Name", option.ProjectId);
            }
            else
            {
                ViewData["ProjectId"] = new SelectList(await _projectsService.GetAvailableProjects(), "Id", "Name");
            }
            return View(option);
        }


        #endregion

        #region Edit custom options
        // GET: Options/Edit/5
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> EditCustom(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // regular option
            var option = await _optionsFactoryService.FindCustomOption(id);

            if (option == null)
            {
                return NotFound();
            }
            var deadline = _deadlinesService.GetNextDeadline();
            if(deadline != null)
            {
                ViewData["Professors"] = new SelectList(await _professorsService.GetProfessors(), "GDPRCode", "FullName");
            }
            else
            {
                ViewData["error"] = "You cannot edit this option anymore...";
            }
            
            return View(option);
        }

        //POST: Options/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> EditCustom(int id, [Bind("Id,Description,ProfessorId")] CustomOptions option)
        {
            if (id != option.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var updated = await _optionsFactoryService.UpdateCustomOption(option);
                    if (!updated)
                    {
                        ViewData["error"] = "You cannot edit this option anymore...";
                        ViewData["Professors"] = new SelectList(await _professorsService.GetProfessors(), "GDPRCode", "FullName");
                        return View(option);
                    }
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateConcurrencyException)
                {
                    // check if the option exists
                    if (!(await CustomOptionExists(option.Id)))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
               
            }
            ViewData["Professors"] = new SelectList(await _professorsService.GetProfessors(), "GDPRCode", "FullName");

            return View(option);
        }
        #endregion
        #endregion

        #region Delete options
        #region Delete regular options
        // GET: Options/Delete/5
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var option = await _optionsFactoryService.FindOption(id);
            if (option == null)
            {
                return NotFound();
            }

            return View(option);
        }

        // POST: Options/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var option = await _optionsFactoryService.FindOption(id);
            // check if the deadline has passed
            var deadline = await _deadlinesService.GetDeadline(option.DeadlineId);
            if (deadline.Status == "inactive" || DateTime.Now > deadline.Date)
            {
                ViewData["error"] = "You can not delete this option anymore...";
                return View(option);
            }
            else
            {
                await _optionsFactoryService.DeleteRegularOption(option);
                return RedirectToAction(nameof(Index));
            }

        }
        #endregion
        #region Delete custom options
        // GET: Options/Delete/5
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> DeleteCustom(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var option = await _optionsFactoryService.FindCustomOption(id);
            if (option == null)
            {
                return NotFound();
            }

            return View(option);
        }

        // POST: Options/Delete/5
        [HttpPost, ActionName("DeleteCustom")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> DeleteCustomConfirmed(int id)
        {
            var option = await _optionsFactoryService.FindCustomOption(id);
            // check if the deadline has passed
            var deadline = await _deadlinesService.GetDeadline(option.DeadlineId);
            if (deadline.Status == "inactive" || DateTime.Now > deadline.Date)
            {
                ViewData["error"] = "You can not delete this option anymore...";
                return View(option);
            }
            else
            {
                await _optionsFactoryService.DeleteCustomOption(option);
                return RedirectToAction(nameof(Index));
            }
        }
        #endregion
        #endregion

        #region Helpers
        [Authorize]
        public async Task<bool> OptionExists(int id)
        {
            return (await _optionsFactoryService.FindOption(id)) != null;
        }

        [Authorize]
        public async Task<bool> CustomOptionExists(int id)
        {
            return (await _optionsFactoryService.FindCustomOption(id)) != null;
        }
        #endregion
    }
}
