﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace BachelorsDegreeManagement.Controllers
{

    [Authorize(Roles = "Professor, Student")]
    public class PersonalityTestResultsController : Controller
    {
        private readonly IPersonalityTestResultsService _resultsService;
        private readonly IQuestionsService _questionsService;
        private readonly IUsersService _usersService;

        public PersonalityTestResultsController(
            IPersonalityTestResultsService resultsService,
            IQuestionsService questionsService,
            IUsersService usersService)
        {
            _resultsService = resultsService;
            _questionsService = questionsService;
            _usersService = usersService;
        }

        // GET: PersonalityTestResults
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            
            //ViewData["result"] = await _resultsService.GetResult(user);
            return View();
        }
        
        // GET: PersonalityTestResults/Create
        [Authorize(Roles="Student, Professor")]
        public async Task<IActionResult> Create()
        {
            ViewData["info"] = "In order to be able to see the suggestions, you need to take the personality test.";
            QuizViewModel model = new QuizViewModel();
            model.Questions = await _questionsService.GetQuestions();
            return View(model);
        }

        // POST: PersonalityTestResults/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student, Professor")]
        public async Task<IActionResult> Create(QuizViewModel model)
        {
            model.Questions = await _questionsService.GetQuestions();
            if (ModelState.IsValid)
            {
                var user = await _usersService.GetCurrentUser(HttpContext.User);
                _resultsService.ComputeResult(model, user);
                ViewData["success"] = "The test was successfully submitted :)";
                
                return RedirectToAction(nameof(Index));
            }
            ViewData["error"] = "The test's questions are mandatory...";
            model.Answers = null;
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> GetScores()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            var result = await _resultsService.GetResult(user);
            return Json(result);
        }
    }
}
