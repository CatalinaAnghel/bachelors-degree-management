﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Authorization;

namespace BachelorsDegreeManagement.Controllers
{
    public class DeadlinesController : Controller
    {
        private readonly IDeadlinesService _deadlinesService;
        private readonly ISystemVariablesService _variablesService;

        public DeadlinesController(IDeadlinesService deadlinesService, ISystemVariablesService systemVariablesService)
        {
            _deadlinesService = deadlinesService;
            _variablesService = systemVariablesService;
        }

        [Authorize]
        // GET: Deadlines
        public async Task<IActionResult> Index()
        {
            return View(await _deadlinesService.GetDeadlines());
        }

        [Authorize]
        public async Task<IActionResult> Get()
        {
            var activeDeadlines = await _deadlinesService.GetCurrentActiveDeadline();
            if(activeDeadlines != null)
            {
                var deadlines = new List<Deadlines>();
                deadlines.Add(activeDeadlines);
                var projectsDeadline = new Deadlines();
                projectsDeadline.Date = activeDeadlines.Date.AddDays(Int32.Parse(_variablesService.GetVariableByName("ProjectsInsertionNumberOfDays").Value) * -1);
                projectsDeadline.Description = "The limit date for the adding projects process.";
                deadlines.Add(projectsDeadline);
                var professorsDeadline = new Deadlines();
                professorsDeadline.Date = activeDeadlines.Date.AddDays(Int32.Parse(_variablesService.GetVariableByName("SortingNumberOfDays").Value));
                professorsDeadline.Description = "The professors must sort the option until this date.";
                deadlines.Add(professorsDeadline);

                return new JsonResult(deadlines);
            }
            else
            {
                var recentDeadline = _deadlinesService.GetRecentDeadline();
                if(recentDeadline != null)
                {
                    var deadlines = new List<Deadlines>();
                    deadlines.Add(recentDeadline);
                    var projectsDeadline = new Deadlines();
                    projectsDeadline.Date = recentDeadline.Date.AddDays(Int32.Parse(_variablesService.GetVariableByName("ProjectsInsertionNumberOfDays").Value) * -1);
                    projectsDeadline.Description = "The limit date for the adding projects process.";
                    deadlines.Add(projectsDeadline);
                    var professorsDeadline = new Deadlines();
                    professorsDeadline.Date = recentDeadline.Date.AddDays(Int32.Parse(_variablesService.GetVariableByName("SortingNumberOfDays").Value));
                    professorsDeadline.Description = "The professors must sort the option until this date.";
                    deadlines.Add(professorsDeadline);

                    return new JsonResult(deadlines);
                }
                
                return new JsonResult("There are no deadlines");
            }
            
        }

        [Authorize]
        // GET: Deadlines/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var deadline = await _deadlinesService.GetDeadline(id);
            if (deadline == null)
            {
                return NotFound();
            }

            return View(deadline);
        }

        [Authorize(Roles = "Administrator")]
        // GET: Deadlines/Create
        public IActionResult Create()
        {
            ViewData["info"] = "Only a deadline can be active at the time. " +
                "This deadline represents the last day when the students can send their options.";
            return View();
        }

        // POST: Deadlines/Create
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Description,Date")] Deadlines deadline)
        {
            if (ModelState.IsValid)
            {
                if(deadline.Date <= DateTime.Now)
                {
                    ViewData["error"] = "You can not set the deadline in the past...";
                }else
                if(deadline.Date.AddDays(-6) < DateTime.Now)
                {
                    ViewData["error"] = "Please reconsider your decission. You need to allocate time for the students (5 days) and for the professors";
                }else
                {
                    var result = await _deadlinesService.AddDeadline(deadline);
                    if (result)
                    {
                        ViewData["success"] = "The deadline has been created";
                    }
                    else
                    {
                        ViewData["error"] = "There is other deadline active";
                    }
                } 
            }
            else
            {
                ViewData["error"] = "The data is invalid";
            }

            ViewData["info"] = "Only a deadline can be active at the time. " +
                "This deadline represents the last day when the students can send their options.";

            return View(deadline);
        }

        [Authorize(Roles = "Administrator")]
        // GET: Deadlines/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var deadline = await _deadlinesService.GetDeadline(id);
            if (deadline == null)
            {
                return NotFound();
            }

            ViewData["info"] = "You can only edit active deadlines";
            return View(deadline);
        }

        // POST: Deadlines/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,Date")] Deadlines deadline)
        {
            if (id != deadline.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var isUpdated = await _deadlinesService.UpdateDeadline(deadline);
                    if (isUpdated)
                    {
                        ViewData["success"] = "The deadline has been successfully updated";
                    }
                    else
                    {
                        ViewData["error"] = "The deadline could not be updated...";
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await DeadlineExists(deadline.Id)))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            ViewData["info"] = "You can only edit active deadlines";
            return View(deadline);
        }

        // GET: Deadlines/Delete/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(int id)
        {
            var deadlines =await  _deadlinesService.GetDeadline(id);
            if (deadlines == null)
            {
                return NotFound();
            }
            ViewData["info"] = "Please note that the projects will be also deleted";
            return View(deadlines);
        }

        // POST: Deadlines/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deadline = await _deadlinesService.GetDeadline(id);
            bool status = await _deadlinesService.DeleteDeadline(deadline);
            if (status)
            {
                ViewData["success"] = "The deadline has been successfully removed.";
            }
            else
            {
                ViewData["error"] = "The deadline could not be removed...";
                return View(deadline);
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        private async Task<bool> DeadlineExists(int id)
        {
            return (await _deadlinesService.GetDeadline(id)) != null;
        }
    }
}
