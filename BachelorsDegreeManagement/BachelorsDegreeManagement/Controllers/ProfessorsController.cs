﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using Microsoft.AspNetCore.Authorization;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using BachelorsDegreeManagement.ApplicationLogic.Services;

namespace BachelorsDegreeManagement.Controllers
{
    public class ProfessorsController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IProfessorsService _professorsService;
        private readonly IDepartmentsService _departmentsService;
        private readonly IStudentsService _studentsService;
        private readonly IDeadlinesService _deadlinesService;
        private readonly IOptionsFactoryService _optionsFactoryService;
        private readonly IProjectsService _projectsService;
        private readonly IPersonalityTestResultsService _testService;
        private readonly ILoggerManager _logger;
        private readonly IExportService _exportService;

        public ProfessorsController(IUsersService usersService, IProjectsService projectsService,
            IProfessorsService professorsService, IDepartmentsService departmentsService,
            IStudentsService studentsService, IDeadlinesService deadlinesService, IOptionsFactoryService optionsFactoryService,
            IPersonalityTestResultsService testResultsService, ILoggerManager logger, IExportService exportService)
        {
            this._usersService = usersService;
            this._professorsService = professorsService;
            this._departmentsService = departmentsService;
            this._studentsService = studentsService;
            this._deadlinesService = deadlinesService;
            this._optionsFactoryService = optionsFactoryService;
            this._projectsService = projectsService;
            this._testService = testResultsService;
            this._logger = logger;
            this._exportService = exportService;
        }

        [Authorize(Roles = "Professor")]
        public async Task<IActionResult> Dashboard()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            if (_deadlinesService.GetNextDeadline() != null)
            {
                ViewData["deadlines"] = 1;
            }
            ViewData["options"] = (await _optionsFactoryService.FindReceivedCustomOptions(user)).Count() + 
                (await _optionsFactoryService.FindReceivedOptions(user)).Count();
            ViewData["students"] = (await _studentsService.GetStudents()).Count();
            ViewData["projects"] = (await _projectsService.GetProjectsWithMinimalInfo(user)).Count();
            return View();
        }

        [Authorize(Roles = "Student")]
        // GET: Professors/Suggestions
        [HttpGet]
        public async Task<IActionResult> Suggestions()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            bool valid = await _testService.CheckPersonalityTest(user);
            if (valid)
            {
                var suggestedProfessors = await _professorsService.FindSuggestedProfessors(user);
                return View(suggestedProfessors);
            }
            else
            {
                return RedirectToAction("Create", "PersonalityTestResults");
            }
            
        }

        // GET: Professors
        [Authorize]
        public async Task<IActionResult> Index()
        {
            ProfessorsIndexViewModel model = new ProfessorsIndexViewModel
            {
                Professors = User.IsInRole("Administrator") ? await _professorsService.GetRegisteredProfessors() : await _professorsService.GetProfessors(),
                ImportViewModel = new ProfessorsImportViewModel()
            };
            return View(model);
        }

        // GET: Professors/Details/5
        [Authorize]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var professor = await _professorsService.GetProfessor(id);
            if (professor == null)
            {
                return NotFound();
            }

            return View(professor);
        }

        // GET: Professors/Create
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create()
        {
            ViewData["DepartmentId"] = new SelectList(await _departmentsService.GetDepartments(), "Id", "Name");
            return View();
        }

        // POST: Professors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create([Bind("GDPRCode,FirstName,LastName,DepartmentId,ProjectsNumber")] Professors professor)
        {
            if (ModelState.IsValid)
            {
                var result = _professorsService.AddProfessor(professor);
                if (result)
                {
                    ViewData["success"] = "The professor has been registered.";
                }
                else
                {
                    ViewData["error"] = "The professor already exists";
                }
            }
            else
            {
                ViewData["error"] = "The data is not valid.";
            }
            ViewData["DepartmentId"] = new SelectList(await _departmentsService.GetDepartments(), "Id", "Name", professor.DepartmentId);
            return View(professor);
        }

        // GET: Professors/Edit/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var professor = await _professorsService.GetProfessor(id);
            if (professor == null)
            {
                return NotFound();
            }
            ViewData["DepartmentId"] = new SelectList(await _departmentsService.GetDepartments(), "Id", "Name", professor.DepartmentId);
            return View(professor);
        }

        // POST: Professors/Edit/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("GDPRCode,FirstName,LastName,DepartmentId,ProjectsNumber")] Professors professor)
        {   
            if (ModelState.IsValid)
            {
                try
                {
                    _professorsService.UpdateProfessor(professor);
                    ViewData["success"] = "The professor's details have been updated.";
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await ProfessorsExists(professor.GDPRCode)))
                    {
                        ViewData["error"] = "Something wrong happend.";
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            ViewData["DepartmentId"] = new SelectList(await _departmentsService.GetDepartments(), "Id", "Name", professor.DepartmentId);
            return View(professor);
        }

        // GET: Professors/Delete/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var professor = await _professorsService.GetProfessor(id);
            if (professor == null)
            {
                return NotFound();
            }

            return View(professor);
        }

        // POST: Professors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var professor = await _professorsService.GetProfessor(id);
            if( professor != null && professor.UserId == null)
            {
                await _professorsService.DeleteProfessor(professor);
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        private async Task<bool> ProfessorsExists(string id)
        {
            return (await _professorsService.GetProfessor(id)) != null;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<IActionResult> Import(ProfessorsImportViewModel model)
        {
            if (model.Document != null && model.Document.ContentType.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            {
                try
                {
                    await _professorsService.Import(model);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception e)
                {
                    _logger.LogInfo("The excel file did not use the standard layout");
                }
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public async Task<IActionResult> Export()
        {
            _exportService.SetFilePath("\\documents\\export\\professors.csv");
            var path = await _exportService.ExportData(false);

            return Json(path);
        }
    }
}
