﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace BachelorsDegreeManagement.Controllers
{
    public class TagsController : Controller
    {
        private readonly ITagsService _tagsService;
        private readonly IUsersService _usersService;
        private readonly IUserTagsService _userTagsService;
        private readonly UserManager<Users> _userManager;

        public TagsController(ITagsService tagsService,
            IUsersService usersService,
            IUserTagsService userTagsService,
            UserManager<Users> userManager)
        {
            _tagsService = tagsService;
            _usersService = usersService;
            _userTagsService = userTagsService;
            _userManager = userManager;
        }

        // GET: Tags
        [Authorize]
        public async Task<IActionResult> Index()
        {
            return View(await _tagsService.GetTags());
        }

        // GET: Tags/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tag = await _tagsService.GetTag(id);

            if (tag == null)
            {
                return NotFound();
            }

            return View(tag);
        }

        // GET: Tags/Create
        [Authorize(Roles = "Professor, Student")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tags/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Professor, Student")]
        public async Task<IActionResult> Create([Bind("Id,Name")] Tags tag)
        {
            if (ModelState.IsValid)
            {
                var user = await _usersService.GetCurrentUser(HttpContext.User);
                var response = _tagsService.AddTag(tag, user);
                if (response)
                {
                    ViewData["success"] = "The tag was successfully inserted";
                }
                else
                {
                    ViewData["error"] = "You already have this tag.";
                }
            }
            else
            {
                ViewData["error"] = "The data is not valid. Try again...";
            }
            return View(tag);
        }

        // GET: Tags/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tag = await _tagsService.GetTag(id);
            if (tag == null)
            {
                return NotFound();
            }
            return View(tag);
        }

        // POST: Tags/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name")] Tags tag)
        {
            if (id != tag.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _tagsService.UpdateTag(tag);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TagsExists(tag.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tag);
        }

        [HttpGet]
        [Authorize(Roles = "Professor, Student")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            var roles = await _userManager.GetRolesAsync(user);
            if (roles[0].Equals("Professor"))
            {
                ViewData["info"] = "If you check the checkbox, the tag will be deleted from the system (from the database)" +
                "and the other users that use it, won't be able to use it after this. " +
                "If you do not select it, the tag will only be deleted from your list of tags and the " +
                "other users that use it won't be affected by your action";
            }
            else
            {
                ViewData["info"] = "The tag will be deleted from your list of tags. You can add it back if you want to";
            }
            ViewData["Tags"] = new SelectList(_tagsService.GetUsersTags(user), "Id", "Name", id);
            
            DeleteTagViewModel model = new DeleteTagViewModel();
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles ="Professor, Student")]
        // GET: Tags/Delete/5
        public async Task<IActionResult> Delete([Bind("TagId, Selected")]DeleteTagViewModel model)
        {            
            var foundTag = await _tagsService.GetTag(model.TagId);
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            
            if (foundTag == null)
            {
                ViewData["error"] = "The tag does not exist.";
            }
            else
            {
                if (model.Selected)
                {
                    _tagsService.DeleteTag(foundTag);
                    ViewData["success"] = "The tag has been deleted.";
                }
                else
                {
                    var result = _userTagsService.DeleteTag(user, model);
                    if (result)
                    {
                        ViewData["success"] = "The tag has been deleted.";
                    }
                    else
                    {
                        ViewData["error"] = "The tag does not exist.";
                    }
                }
            }
            var roles = await _userManager.GetRolesAsync(user);
            if (roles[0].Equals("Professor"))
            {
                ViewData["info"] = "If you check the checkbox, the tag will be deleted from the system (from the database)" +
                "and the other users that use it, won't be able to use it after this. " +
                "If you do not select it, the tag will only be deleted from your list of tags and the " +
                "other users that use it won't be affected by your action";
            }
            else
            {
                ViewData["info"] = "The tag will be deleted from your list of tags. You can add it back if you want to";
            }
            ViewData["tags"] = new SelectList(_tagsService.GetUsersTags(user), "Id", "Name");
            return View(model);
        }

        [Authorize]
        private bool TagsExists(int id)
        {
            return _tagsService.GetTag(id) == null ? false : true;
        }
    }
}
