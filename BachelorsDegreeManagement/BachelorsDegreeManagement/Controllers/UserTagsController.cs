﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;

namespace BachelorsDegreeManagement.Controllers
{
    public class UserTagsController : Controller
    {
        private readonly ITagsService _tagsService;
        private readonly IUsersService _usersService;
        private readonly IUserTagsService _usersTagsService;

        public UserTagsController( ITagsService tagsService,
            IUsersService usersService, IUserTagsService userTagsService)
        {
            _tagsService = tagsService;
            _usersService = usersService;
            _usersTagsService = userTagsService;
        }

        // GET: UserTags/Index/UserId
        [Authorize]
        public async Task<IActionResult> Index(string id)
        {
            Users user = null;
            if(id == null)
            {
                user = await _usersService.GetCurrentUser(HttpContext.User);
            }
            else
            {
                user = await _usersService.GetUser(id);
            }
            
            var tags = _tagsService.GetUsersTags(user);
            var models = _tagsService.GetTagModels(tags);
            ViewData["user"] = user;
            return View(models);
        }

        // GET: UserTags/Create
        [Authorize(Roles="Professor, Student")]
        public async Task<IActionResult> Create()
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            ViewData["TagId"] = new SelectList(_tagsService.GetAvailableTags(user), "Id", "Name");
            return View();
        }

        // POST: UserTags/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Professor, Student")]
        public async Task<IActionResult> Create([Bind("TagId")] UserTags userTags)
        {
            var user = await _usersService.GetCurrentUser(HttpContext.User);
            if (ModelState.IsValid)
            {
                userTags.UserId = user.Id;
                _usersTagsService.AddUserTag(userTags);
                ViewData["success"] = "The tag was added into your list.";
            }
            else
            {
                ViewData["error"] = "The data is not valid";
            }
            ViewData["TagId"] = new SelectList(_tagsService.GetAvailableTags(user), "Id", "Name");
            return View(userTags);
        }
    }
}
