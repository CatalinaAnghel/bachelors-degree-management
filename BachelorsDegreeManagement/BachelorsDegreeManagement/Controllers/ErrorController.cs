﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BachelorsDegreeManagement.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILoggerManager _logger;

        public ErrorController(ILoggerManager loggerManager){
            _logger = loggerManager;
        }

        public ActionResult Index()
        {

            int statusCode = HttpContext.Response.StatusCode;
            string viewName = null;
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Oh, snap... It seems like this page doesn't exist";
                    viewName = "NotFound";
                    string referrer = Request.Headers["Referer"].ToString();
                    _logger.LogWarn("Not found: " + referrer);
                    break;
                case 500:
                    ViewBag.ErrorMessage = "Oops, it seems that the problem is on our side. We are currently working on fixing it. Please try again later...";
                    viewName = "InternalServerError";
                    _logger.LogError("Internal server error");
                    break;
                default:
                    ViewBag.ErrorMessage = "Oh, snap... It seems like this page doesn't exist";
                    viewName = "NotFound";
                    
                    break;
            }
            return View(viewName);
        }
    }
}
