﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;

namespace BachelorsDegreeManagement.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<Users> _signInManager;
        private readonly UserManager<Users> _userManager;
        private readonly ILoggerManager _logger;
        private readonly IStudentsService _studentsService;
        private readonly IProfessorsService _professorsService;
        private readonly IRegisterService _registerService;

        public RegisterModel(
            UserManager<Users> userManager,
            SignInManager<Users> signInManager,
            ILoggerManager logger,
            IStudentsService studentsService,
            IProfessorsService professorsService,
            IRegisterService registerService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _studentsService = studentsService;
            _professorsService = professorsService;
            _registerService = registerService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required]
            public string Role { get; set; }

            [Required]
            [StringLength(13, ErrorMessage = "The GDPR code must have 13 characters")]
            public string GDPRCode { get; set; }
            
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "Username")]
            public string Username { get; set; }

            [Required]
            [Display(Name = "Phone number")]
            [RegularExpression(@"[0-9]{10}", ErrorMessage = "The phone number must have 10 digits")]
            public string PhoneNumber { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            [RegularExpression(@"^(?=.*[\w])(?=.*[\W])[\w\W]{8,}$", ErrorMessage ="The password does not respect the standard form")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var result = await _registerService.Register(Input.GDPRCode, Input.Email, Input.Password, Input.Email, Input.Role, Input.PhoneNumber);

                if (result != null && result.Succeeded)
                {
                    _logger.LogInfo("User created a new account with password.");
                    return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                }
                if(result != null && result.Errors != null)
                {
                    
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The user could not be registered");
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
