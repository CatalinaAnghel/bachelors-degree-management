﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO.Pipelines;
using System.Linq;
using System.Threading.Tasks;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BachelorsDegreeManagement.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<Users> _userManager;
        private readonly ILoggerManager _logger;
        private readonly SignInManager<Users> _signInManager;
        private readonly IProfessorsService _professorsService;
        private readonly IStudentsService _studentsService;
        private readonly ISystemVariablesService _variablesService;
        private string _supervisorStylePrefix = "SupervisorStyle_";

        public IndexModel(
            UserManager<Users> userManager,
            ILoggerManager logger,
            SignInManager<Users> signInManager,
            IProfessorsService professorsService,
            IStudentsService studentsService,
            ISystemVariablesService variablesService)
        {
            _userManager = userManager;
            _logger = logger;
            _signInManager = signInManager;
            _professorsService = professorsService;
            _studentsService = studentsService;
            _variablesService = variablesService;
        }

        public string Email { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [StringLength(25, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 3)]
            [Display(Name = "First name")]
            public string FirstName { get; set; }

            [StringLength(25, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 3)]
            [Display(Name = "Last name")]
            public string LastName { get; set; }
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            [Display(Name = "Preferred complexity")]
            public string Complexity { get; set; }
            public SelectList Complexities { get; set; }

            [Display(Name = "Preferred supervisor style")]
            public string SupervisorStyle { get; set; }
            public SelectList SupervisorStyles { get; set; }
        }

        private async Task LoadAsync(Users user)
        {
            var email = await _userManager.GetEmailAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);

            Email = email;
            var supervisorStyles = _variablesService.GetVariableLikeName(this._supervisorStylePrefix);

            if (User.IsInRole("Student"))
            {
                var student = await _studentsService.GetStudentByUser(user);
                var complexities = _variablesService.GetComplexities();
                

                Input = new InputModel
                {
                    PhoneNumber = phoneNumber,
                    FirstName = student.FirstName,
                    LastName = student.LastName,
                    Complexities = new SelectList(complexities, "Name", "Value", student.Complexity),
                    SupervisorStyles = new SelectList(supervisorStyles, "Name", "Value",
                            _supervisorStylePrefix + student.SupervisorStyle.ToString())
                };
            }
            else if (User.IsInRole("Professor"))
            {
                var professor = await _professorsService.GetProfessorByUser(user);
                Input = new InputModel
                {
                    PhoneNumber = phoneNumber,
                    FirstName = professor.FirstName,
                    LastName = professor.LastName,
                    SupervisorStyles = new SelectList(supervisorStyles, "Name", "Value",
                            _supervisorStylePrefix + professor.SupervisorStyle.ToString())
                };
            }
            
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }


            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }
            var role = (await _userManager.GetRolesAsync(user))[0];
            try
            {
                if (role.Equals("Professor") || role.Equals("Student"))
                {
                    int supervisorStyle = Int32.Parse(Input.SupervisorStyle.Split("_")[1]);
                    switch (role)
                    {
                        case "Professor":
                            var professor = await _professorsService.GetProfessorByUser(user);
                            if (Input.FirstName != null && !Input.FirstName.Equals(professor.FirstName))
                            {
                                professor.FirstName = Input.FirstName;
                            }
                            if (Input.LastName != null && !Input.LastName.Equals(professor.LastName))
                            {
                                professor.LastName = Input.LastName;
                            }

                            if (supervisorStyle != professor.SupervisorStyle)
                            {
                                professor.SupervisorStyle = supervisorStyle;
                            }
                            _professorsService.UpdateProfessor(professor);
                            break;
                        case "Student":
                            var student = await _studentsService.GetStudentByUser(user);
                            if (Input.FirstName != null && !Input.FirstName.Equals(student.FirstName))
                            {
                                student.FirstName = Input.FirstName;
                            }
                            if (Input.LastName != null && !Input.LastName.Equals(student.LastName))
                            {
                                student.LastName = Input.LastName;
                            }
                            if (!Input.Complexity.Equals(student.Complexity))
                            {
                                student.Complexity = Input.Complexity;
                            }

                            if (supervisorStyle != student.SupervisorStyle)
                            {
                                student.SupervisorStyle = supervisorStyle;
                            }
                            _studentsService.UpdateStudent(student);
                            break;
                    }
                }
            }catch(Exception e)
            {
                _logger.LogError("Update profile: " + e.Message + "\nStack trace: \n" + e.StackTrace);
            }
            
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    StatusMessage = "Unexpected error when trying to set phone number.";
                    _logger.LogError("Unexpected error when trying to set phone number.");
                    return RedirectToPage();
                }
            }

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Your profile has been updated";
            _logger.LogInfo("The profile has been updated");
            return RedirectToPage();
        }
    }
}
