﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class TagsService : ITagsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TagsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool AddTag(Tags tag, Users user)
        {
            var foundTag = _unitOfWork.TagsRepository.FindByCondition(t => t.Name.Equals(tag.Name)).SingleOrDefault();
            if(foundTag == null)
            {
                _unitOfWork.TagsRepository.Create(tag);
                _unitOfWork.TagsRepository.Save();
                UserTags usertags = new UserTags
                {
                    TagId = tag.Id,
                    UserId = user.Id
                };
                _unitOfWork.UserTagsRepository.Create(usertags);
                _unitOfWork.Complete();
                return true;
            }
            else
            {
                var searchedTag = _unitOfWork.UserTagsRepository
                    .FindByCondition(t => t.UserId.Equals(user.Id) && t.TagId == foundTag.Id)
                    .SingleOrDefault();
                if(searchedTag == null)
                {
                    UserTags usertags = new UserTags
                    {
                        TagId = foundTag.Id,
                        UserId = user.Id
                    };
                    _unitOfWork.UserTagsRepository.Create(usertags);
                    _unitOfWork.Complete();
                    return true;
                }
                return false;
            }
        }

        public void UpdateTag(Tags tag)
        {
            _unitOfWork.TagsRepository.Update(tag);
            _unitOfWork.Complete();
        }

        public void DeleteTag(Tags tag)
        {
            _unitOfWork.TagsRepository.Delete(tag);
            _unitOfWork.Complete();
        }

        public async Task<Tags> GetTag(int? id)
        {
            return await _unitOfWork.TagsRepository.FindByCondition(tag => tag.Id == id).SingleOrDefaultAsync();
        }

        public async Task<List<Tags>> GetTags()
        {
            return await _unitOfWork.TagsRepository.FindAll().ToListAsync();
        }

        public List<Tags> GetUsersTags(Users user)
        {
            return _unitOfWork.TagsRepository.GetUsersTags(user);
        }

        public List<Tags> GetAvailableTags(Users user)
        {
            return _unitOfWork.TagsRepository.GetAvailableTags(user);
        }

        public List<TagsViewModel> GetTagModels(List<Tags> tags)
        {
            List<TagsViewModel> models = new List<TagsViewModel>();
            foreach(var tag in tags)
            {
                TagsViewModel model = new TagsViewModel
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    NoProfessors = _unitOfWork.UserTagsRepository.GetNumberOfProfessors(tag.Id),
                    NoProjects = _unitOfWork.ProjectTagsRepository.GetNumberOfProjects(tag.Id),
                    NoStudents = _unitOfWork.UserTagsRepository.GetNumberOfStudents(tag.Id)
                };

                models.Add(model);
            }

            return models;
        }
    }
}
