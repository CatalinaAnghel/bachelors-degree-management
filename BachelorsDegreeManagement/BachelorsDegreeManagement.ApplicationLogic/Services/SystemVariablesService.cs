﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class SystemVariablesService : ISystemVariablesService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerManager _logger;

        public SystemVariablesService(IUnitOfWork unitOfWork, ILoggerManager logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public List<SystemVariables> GetComplexities()
        {
            return _unitOfWork.SystemVariablesRepository.FindByCondition(s => s.Name.Contains("ComplexityLevel")).ToList();
        }

        public SystemVariables GetVariableByName(string name)
        {
            return _unitOfWork.SystemVariablesRepository.FindSystemVariableByCondition(v => v.Name == name);
        }

        /// <summary>
        /// This is a case insensitive search
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<SystemVariables> GetVariableLikeName(string name)
        {
            return _unitOfWork.SystemVariablesRepository.FindSystemVariablesByCondition(v => v.Name.ToUpper()
            .Contains(name.ToUpper()));
        }

        public UpdateWeightsViewModel GetWeights()
        {
            float personalityPercentage = float.Parse(GetVariableByName("PersonalityPercentage").Value);
            float tagsPercentage = float.Parse(GetVariableByName("TagsPercentage").Value);
            float supervisorStylePercentage = float.Parse(GetVariableByName("SupervisorStylePercentage").Value);
            float totalPercentage = personalityPercentage + tagsPercentage + supervisorStylePercentage;
            float projectPersonalityPercentage = float.Parse(GetVariableByName("ProjectPersonalityPercentage").Value);
            float projectTagsPercentage = float.Parse(GetVariableByName("ProjectTagsPercentage").Value);
            float projectComplexityPercentage = float.Parse(GetVariableByName("ProjectComplexityPercentage").Value);
            float projectSupervisorStylePercentage = float.Parse(GetVariableByName("ProjectSupervisorStylePercentage").Value);
            float projectTotalPercentage = projectPersonalityPercentage + projectComplexityPercentage +
                projectSupervisorStylePercentage + projectTagsPercentage;
            UpdateWeightsViewModel model = new UpdateWeightsViewModel
            {
                PersonalityPercentage = personalityPercentage,
                TagsPercentage = tagsPercentage,
                SupervisorStylePercentage = supervisorStylePercentage,
                TotalPercentage = totalPercentage,
                ProjectPersonalityPercentage = projectPersonalityPercentage,
                ProjectTagsPercentage = projectTagsPercentage,
                ProjectComplexityPercentage = projectComplexityPercentage,
                ProjectSupervisorStylePercentage = projectSupervisorStylePercentage,
                ProjectTotalPercentage = projectTotalPercentage
            };

            return model;
        }

        public bool UpdateVariables(UpdateWeightsViewModel model)
        {
            if (ValidateModel(model))
            {
                try
                {
                    // student / professor weights
                    var personalityWeight = GetVariableByName("PersonalityPercentage");
                    if (model.PersonalityPercentage != float.Parse(personalityWeight.Value))
                    {
                        personalityWeight.Value = model.PersonalityPercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(personalityWeight);
                        _unitOfWork.Complete();
                    }

                    var domainWeight = GetVariableByName("TagsPercentage");
                    if (model.TagsPercentage != float.Parse(domainWeight.Value))
                    {
                        domainWeight.Value = model.TagsPercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(domainWeight);
                        _unitOfWork.Complete();
                    }

                    var supervisorWeight = GetVariableByName("SupervisorStylePercentage");
                    if (model.SupervisorStylePercentage != float.Parse(supervisorWeight.Value))
                    {
                        supervisorWeight.Value = model.SupervisorStylePercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(supervisorWeight);
                        _unitOfWork.Complete();
                    }

                    // student / project weights
                    var projectPersonalityWeight = GetVariableByName("ProjectPersonalityPercentage");
                    if (model.ProjectPersonalityPercentage != float.Parse(projectPersonalityWeight.Value))
                    {
                        projectPersonalityWeight.Value = model.ProjectPersonalityPercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(projectPersonalityWeight);
                        _unitOfWork.Complete();
                    }

                    var projectDomainWeight = GetVariableByName("ProjectTagsPercentage");
                    if (model.ProjectTagsPercentage != float.Parse(projectDomainWeight.Value))
                    {
                        projectDomainWeight.Value = model.ProjectTagsPercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(projectDomainWeight);
                        _unitOfWork.Complete();
                    }

                    var projectSupervisorWeight = GetVariableByName("ProjectSupervisorStylePercentage");
                    if (model.ProjectSupervisorStylePercentage != float.Parse(projectSupervisorWeight.Value))
                    {
                        projectSupervisorWeight.Value = model.ProjectSupervisorStylePercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(projectSupervisorWeight);
                        _unitOfWork.Complete();
                    }

                    var projectComplexityWeight = GetVariableByName("ProjectComplexityPercentage");
                    if (model.ProjectComplexityPercentage != float.Parse(projectComplexityWeight.Value))
                    {
                        projectComplexityWeight.Value = model.ProjectComplexityPercentage.ToString();
                        _unitOfWork.SystemVariablesRepository.Update(projectComplexityWeight);
                        _unitOfWork.Complete();
                    }

                    return true;
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return false;
                }
            }
            else
            {
                _logger.LogWarn("Invalid model used to update the weights");
                return false;
            }

        }

        private bool ValidateModel(UpdateWeightsViewModel model)
        {
            float projectTotal = model.ProjectComplexityPercentage + model.ProjectPersonalityPercentage +
                model.ProjectSupervisorStylePercentage + model.ProjectTagsPercentage;
            float professorTotal = model.PersonalityPercentage + model.TagsPercentage + model.SupervisorStylePercentage;
            return projectTotal == 1 && professorTotal == 1;
        }
    }
}
