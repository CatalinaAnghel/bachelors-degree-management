﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class DeadlinesService : IDeadlinesService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DeadlinesService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> ValidateDeadlines()
        {
            var deadlines = await _unitOfWork.DeadlinesRepository.FindDeadlinesByCondition(d => d.Status.Equals("active"));
            int count = 0;
            foreach (var deadline in deadlines)
            {
                if (deadline.Date <= DateTime.Now)
                {
                    deadline.Status = "inactive";
                    _unitOfWork.DeadlinesRepository.Update(deadline);
                    _unitOfWork.Complete();
                    count++;
                }
            }
            return count;
        }

        public async Task<Deadlines> GetDeadline(int id)
        {
            return await _unitOfWork.DeadlinesRepository.FindDeadlineByCondition(d => d.Id == id);
        }

        public async Task<List<Deadlines>> GetDeadlines()
        {
            return await _unitOfWork.DeadlinesRepository.FindAllDeadlines();
        }

        public async Task<bool> AddDeadline(Deadlines deadline)
        {
            var activeDeadline = await _unitOfWork.DeadlinesRepository.FindDeadlineByCondition(d => d.Date >= DateTime.Now && d.Status.Equals("active"));
            if (activeDeadline != null)
            {
                return false;
            }
            else
            {
                deadline.IsAllocationMade = 0;
                deadline.Status = "active";
                deadline.Date = deadline.Date.AddHours(23);
                deadline.Date = deadline.Date.AddMinutes(59);
                deadline.Date = deadline.Date.AddSeconds(59);
                _unitOfWork.DeadlinesRepository.Create(deadline);
                _unitOfWork.Complete();

                return true;
            }
        }

        public async Task<bool> DeleteDeadline(Deadlines deadline)
        {
            if (deadline.Status.Equals("active"))
            {
                try
                {
                    var projects = await _unitOfWork.ProjectsRepository.FindByCondition(p => p.DeadlineId == deadline.Id).ToListAsync();
                    if (projects != null)
                    {
                        foreach (var project in projects)
                        {
                            _unitOfWork.ProjectsRepository.Delete(project);
                        }
                        _unitOfWork.Complete();
                    }

                    _unitOfWork.DeadlinesRepository.Delete(deadline);
                    _unitOfWork.Complete();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public async Task<bool> UpdateDeadline(Deadlines deadline, bool inactiveEnabled = false)
        {
            var foundDeadline = await _unitOfWork.DeadlinesRepository.FindDeadlineByCondition(d => d.Id == deadline.Id);
            if (foundDeadline.Status.Equals("active"))
            {
                if (deadline.Date != null && deadline.Date != foundDeadline.Date)
                {
                    deadline.Date = deadline.Date.AddHours(23);
                    deadline.Date = deadline.Date.AddMinutes(59);
                    deadline.Date = deadline.Date.AddSeconds(59);
                    foundDeadline.Date = deadline.Date;
                    if(foundDeadline.Date <= DateTime.Now)
                    {
                        foundDeadline.Status = "inactive";
                    }
                }
                if (deadline.Description != null && deadline.Description != foundDeadline.Description)
                {
                    foundDeadline.Description = deadline.Description;
                }

                _unitOfWork.DeadlinesRepository.Update(foundDeadline);
                _unitOfWork.Complete();
                return true;
            }
            else if (inactiveEnabled)
            {
                foundDeadline.IsAllocationMade = deadline.IsAllocationMade;
                _unitOfWork.DeadlinesRepository.Update(foundDeadline);
                _unitOfWork.Complete();
                return true;
            }
            return false;
        }

        public void UpdateDeadlineSync(Deadlines deadline, bool inactiveEnabled = false)
        {
            var foundDeadline = _unitOfWork.DeadlinesRepository.FindByCondition(d => d.Id == deadline.Id).SingleOrDefault();
            if (foundDeadline.Status.Equals("active"))
            {
                if (deadline.Date != null && deadline.Date != foundDeadline.Date)
                {
                    deadline.Date = deadline.Date.AddHours(23);
                    deadline.Date = deadline.Date.AddMinutes(59);
                    deadline.Date = deadline.Date.AddSeconds(59);
                    foundDeadline.Date = deadline.Date;
                }
                if (deadline.Description != null && deadline.Description != foundDeadline.Description)
                {
                    foundDeadline.Description = deadline.Description;
                }

                _unitOfWork.DeadlinesRepository.Update(foundDeadline);
                _unitOfWork.Complete();
            }
            else if (inactiveEnabled)
            {
                foundDeadline.IsAllocationMade = deadline.IsAllocationMade;
                _unitOfWork.DeadlinesRepository.Update(foundDeadline);
                _unitOfWork.Complete();
            }

        }

        public async Task<Deadlines> GetCurrentActiveDeadline()
        {
            return await _unitOfWork.DeadlinesRepository.FindDeadlineByCondition(d => d.Date >= DateTime.Now);
        }

        public Deadlines GetRecentDeadline()
        {
            return _unitOfWork.DeadlinesRepository.GetRecentDeadline();
        }

        public Deadlines GetNextDeadline()
        {
            return _unitOfWork.DeadlinesRepository.GetNextDeadline();
        }
    }
}
