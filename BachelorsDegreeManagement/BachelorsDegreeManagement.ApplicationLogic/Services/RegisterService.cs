﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly UserManager<Users> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUsersService _usersService;
        private readonly IStudentsService _studentsService;
        private readonly IProfessorsService _professorsService;

        public RegisterService(UserManager<Users> userManager,
            IUsersService usersService,
            RoleManager<IdentityRole> roleManager,
            IStudentsService studentsService,
            IProfessorsService professorsService)
        {
            _userManager = userManager;
            _usersService = usersService;
            _roleManager = roleManager;
            _studentsService = studentsService;
            _professorsService = professorsService;
        }

        public async Task<IdentityResult> Register(string GDPRCode, string username, string password, string email, string role, string phoneNumber)
        {
            var foundStudent = await _studentsService.GetStudent(GDPRCode);

            var foundProfessor = await _professorsService.GetProfessor(GDPRCode);

            if (foundStudent != null || foundProfessor != null)
            {
                bool hasAccount = false;
                IdentityResult result = null;
                if (foundStudent != null)
                {
                    hasAccount = foundStudent.UserId != null ? true : false;
                }
                else if(foundProfessor != null)
                {
                    hasAccount = foundProfessor.UserId != null ? true : false;
                }

                if (!hasAccount)
                {
                    Users user = null;
                    if (foundStudent != null && role.Equals("Student"))
                    {
                        user = new Users
                        {
                            UserName = username,
                            Email = email,
                            PhoneNumber = phoneNumber
                        };
                        result = await _userManager.CreateAsync(user, password);
                        foundStudent.User = user;
                        foundStudent.UserId = user.Id;
                        _studentsService.UpdateStudent(foundStudent);
                    }
                    else if(foundProfessor != null && role.Equals("Professor"))
                    {
                        user = new Users
                        {
                            UserName = username,
                            Email = email,
                            PhoneNumber = phoneNumber
                        };
                        result = await _userManager.CreateAsync(user, password);
                        foundProfessor.User = user;
                        foundProfessor.UserId = user.Id;
                        _professorsService.UpdateProfessor(foundProfessor);
                    }
                    if (result != null && result.Succeeded)
                    {
                        var foundRole = _roleManager.Roles.Where(r => r.Name == role).Single();
                        result = await _userManager.AddToRoleAsync(user, foundRole.Name);
                    }
                }

                return result;
            }
            return new IdentityResult();
        }
    }
}
