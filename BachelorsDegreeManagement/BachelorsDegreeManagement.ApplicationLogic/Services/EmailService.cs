﻿using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _config;
        public EmailService(IConfiguration config)
        {
            _config = config;
        }

        public MimeMessage Create(string receiver, string subject, string html)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_config.GetSection("EmailSettings").GetSection("Sender").Value);
            email.To.Add(MailboxAddress.Parse(receiver));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = html };

            return email;
        }

        public void Send(MimeMessage email)
        {
            // send email
            using var smtp = new SmtpClient();
            smtp.Connect(_config.GetSection("EmailSettings").GetSection("SmtpHost").Value,
                 Int32.Parse(_config.GetSection("EmailSettings").GetSection("SmtpPort").Value),
                 SecureSocketOptions.StartTls);
            smtp.Authenticate(_config.GetSection("EmailSettings").GetSection("SmtpUser").Value,
                 _config.GetSection("EmailSettings").GetSection("SmtpPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
