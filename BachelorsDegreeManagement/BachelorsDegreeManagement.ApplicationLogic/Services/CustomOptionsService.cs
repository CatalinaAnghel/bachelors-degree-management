﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class CustomOptionsService : ICustomOptionsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ICustomOptionsRepository OptionsRepository { get; set; }
        public IStudentsRepository StudentsRepository { get; set; }
        public IProfessorsRepository ProfessorsRepository { get; set; }

        public CustomOptionsService(IUnitOfWork unitOfWork,
            ICustomOptionsRepository optionsRepository,
            IStudentsRepository studentsRepository,
            IProfessorsRepository professorsRepository)
        {
            _unitOfWork = unitOfWork;
            OptionsRepository = optionsRepository;
            StudentsRepository = studentsRepository;
            ProfessorsRepository = professorsRepository;
        }

        public void AddCustomOption(CustomOptions option)
        {
            _unitOfWork.CustomOptionsRepository.Create(option);
            _unitOfWork.Complete();
        }
        
        public void UpdateCustomOption(CustomOptions option)
        {
            _unitOfWork.CustomOptionsRepository.Update(option);
            _unitOfWork.Complete();
        }

        public void DeleteCustomOption(CustomOptions option)
        {
            _unitOfWork.CustomOptionsRepository.Delete(option);
            _unitOfWork.Complete();
        }

        public async Task<CustomOptions> GetCustomOption(int id)
        {
            return await _unitOfWork.CustomOptionsRepository.FindCustomOptionByCondition(option => option.Id == id);
        }

        public async Task<List<CustomOptions>> GetReceivedOptions(Users user)
        {
            var professor = await ProfessorsRepository.FindByCondition(s => s.UserId.Equals(user.Id)).SingleOrDefaultAsync();

            return await _unitOfWork.CustomOptionsRepository.FindCustomOptionsByCondition(o => o.ProfessorId.Equals(professor.GDPRCode));
        }

        public async Task<List<CustomOptions>> GetSentOptions(Users user)
        {
            var student = await StudentsRepository.FindByCondition(s => s.UserId.Equals(user.Id)).SingleOrDefaultAsync();

            return await _unitOfWork.CustomOptionsRepository.FindCustomOptionsByCondition(o => o.StudentId.Equals(student.GDPRCode));
        }
        public async Task<List<CustomOptions>> GetWaitingOptions(Users user)
        {
            var student = await StudentsRepository.FindByCondition(s => s.UserId.Equals(user.Id)).SingleOrDefaultAsync();

            return await _unitOfWork.CustomOptionsRepository
                .FindByCondition(o => o.StudentId.Equals(student.GDPRCode) && o.Status.Equals("waiting")).ToListAsync();
        }
        public List<CustomOptions> GetWaitingOptionsSync(Users user)
        {
            var student = StudentsRepository.FindByCondition(s => s.UserId.Equals(user.Id)).SingleOrDefault();

            return _unitOfWork.CustomOptionsRepository
                .FindByCondition(o => o.StudentId.Equals(student.GDPRCode) && o.Status.Equals("waiting")).ToList();
        }

        public async Task<List<CustomOptions>> GetCustomOptions()
        {
            return await _unitOfWork.CustomOptionsRepository.FindAllCustomOptions();
        }

        public async Task<List<CustomOptions>> GetAllWaitingOptions()
        {
            return await _unitOfWork.CustomOptionsRepository.FindCustomOptionsByCondition(o => o.Status == "waiting");
        }

        public async Task<CustomOptions> FindOptionByNumber(Students student, int numberForStudent)
        {
            return await _unitOfWork.CustomOptionsRepository.FindCustomOptionByCondition(o => o.NumberForUser == numberForStudent && o.Student == student);
        }

        public CustomOptions FindOptionByNumberSync(Students student, int numberForStudent)
        {
            return _unitOfWork.CustomOptionsRepository.FindByCondition(o => o.NumberForUser == numberForStudent && o.Student == student)
                .Include(o => o.Professor).FirstOrDefault();
        }

        public bool CheckStatus(int id, string status)
        {
            return _unitOfWork.CustomOptionsRepository.FindByCondition(o => o.Id == id && o.Status.Equals(status))
                .SingleOrDefault() != null;
        }

    }
}
