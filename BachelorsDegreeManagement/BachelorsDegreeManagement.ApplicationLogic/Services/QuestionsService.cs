﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class QuestionsService: IQuestionsService
    {
        private readonly IUnitOfWork _unitOfWork; 
        public QuestionsService(IQuestionsRepository questionsRepository, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Questions>> GetQuestions()
        {
            return await _unitOfWork.QuestionsRepository.FindAllQuestions();
        }

    }
}
