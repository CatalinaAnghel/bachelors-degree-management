﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class ProfessorsService : SupervisorStyleService, IProfessorsService
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ProfessorsService(
            IUnitOfWork unitOfWork,
            IWebHostEnvironment hostingEnvironment):base(unitOfWork)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<Professors> GetProfessor(string GDPRCode)
        {
            return await _unitOfWork.ProfessorsRepository
                .FindByCondition(student => student.GDPRCode.Equals(GDPRCode))
                .Include(p => p.Department)
                .Include(p => p.Projects)
                .Include(p => p.User)
                .SingleOrDefaultAsync();
        }

        public async Task<Professors> GetProfessorByUser(Users user)
        {
            return await _unitOfWork.ProfessorsRepository.FindByCondition(p => p.UserId.Equals(user.Id))
                .Include(p => p.User).Include(p => p.Department).Include(p => p.Projects)
                .SingleOrDefaultAsync();
        }

        public async Task<List<Professors>> GetProfessors()
        {
            return await _unitOfWork.ProfessorsRepository.FindByCondition(p => p.UserId != null && p.ProjectsNumber > 0).Include(p => p.Department)
                .Include(p => p.Projects).Include(p => p.User).ToListAsync();
        }
        public List<Professors> GetProfessorsSync()
        {
            return _unitOfWork.ProfessorsRepository.FindByCondition(p => p.UserId != null && p.ProjectsNumber > 0).Include(p => p.Department)
                .Include(p => p.Projects).Include(p => p.User).ToList();
        }

        public async Task<List<Professors>> GetRegisteredProfessors()
        {
            return await _unitOfWork.ProfessorsRepository.FindAll().Include(p => p.Department)
                .Include(p => p.Projects).Include(p => p.User).ToListAsync();
        }

        public void UpdateProfessor(Professors professor)
        {
            var foundProffessor = _unitOfWork.ProfessorsRepository.FindByCondition(p => p.GDPRCode.Equals(professor.GDPRCode)).Single();
            if (professor.FirstName != null && professor.FirstName != foundProffessor.FirstName)
            {
                foundProffessor.FirstName = professor.FirstName;
            }
            if (professor.LastName != null && professor.LastName != foundProffessor.LastName)
            {
                foundProffessor.LastName = professor.LastName;
            }
            if (professor.DepartmentId != 0 && professor.DepartmentId != foundProffessor.DepartmentId)
            {
                foundProffessor.DepartmentId = professor.DepartmentId;
            }
            if ( professor.ProjectsNumber != foundProffessor.ProjectsNumber)
            {
                foundProffessor.ProjectsNumber = professor.ProjectsNumber;
            }
            if (foundProffessor.UserId == null && professor.UserId != null)
            {
                foundProffessor.UserId = professor.UserId;
            }
            _unitOfWork.ProfessorsRepository.Update(foundProffessor);
            _unitOfWork.Complete();
        }

        public async Task<List<Professors>> FindProfessorsByName(string name)
        {
            return await _unitOfWork.ProfessorsRepository.FindByCondition(p => (p.FirstName + p.LastName).Contains(name)
                                        || (p.LastName + p.FirstName).Contains(name)).Include(p => p.User)
                                        .Include(p => p.Department).Include(p => p.Projects)
                                        .Distinct().ToListAsync();
        }

        /**
         * The result will be: results[Professor] = [personalityScore, tagsScore, totalScore]
         */
        public async Task<List<GetSuggestionsViewModel>> FindSuggestedProfessors(Users user)
        {
            // Get the student's information
            var student = await _unitOfWork.StudentsRepository
                .FindByCondition(s => s.UserId.Equals(user.Id))
                .Include(s => s.User)
                .ThenInclude(u => u.UserTags)
                .ThenInclude(ut => ut.Tag)
                .SingleOrDefaultAsync();
            var studentTestResult = await _unitOfWork.PersonalityTestResultsRepository
                .FindByCondition(t => t.UserId.Equals(user.Id))
                .SingleOrDefaultAsync();

            // Get the professors that have an account
            var professors = await _unitOfWork.ProfessorsRepository
                .FindByCondition(p => p.UserId != null && p.ProjectsNumber > 0)
                .Include(p => p.Department)
                .Include(p => p.Projects)
                .Include(p => p.User)
                .ThenInclude(u => u.UserTags)
                .ThenInclude(ut => ut.Tag)
                .ToListAsync();

            // compute the results:
            List<GetSuggestionsViewModel> finalResults = new List<GetSuggestionsViewModel>();
            foreach (var professor in professors)
            {
                // compute the personality score
                var professorTestResult = await _unitOfWork.PersonalityTestResultsRepository
                    .FindByCondition(t => t.UserId.Equals(professor.UserId))
                    .SingleOrDefaultAsync();
                var personalityScore = 0.0;
                if (professorTestResult != null && studentTestResult != null)
                {
                    double personalityDiff = (Math.Abs(studentTestResult.AgreeablenessScore - professorTestResult.AgreeablenessScore) +
                        Math.Abs(studentTestResult.ConscientScore - professorTestResult.ConscientScore) +
                        Math.Abs(studentTestResult.EmotionalScore - professorTestResult.EmotionalScore) +
                        Math.Abs(studentTestResult.ImaginationScore - professorTestResult.ImaginationScore) +
                        Math.Abs(studentTestResult.SurgencyScore - professorTestResult.SurgencyScore)) / 200;
                    personalityScore = (1 - personalityDiff) * 100;
                }
                else
                {
                    double personalityDiff = (Math.Abs(studentTestResult.AgreeablenessScore - 30) +
                        Math.Abs(studentTestResult.ConscientScore - 30) +
                        Math.Abs(studentTestResult.EmotionalScore - 30) +
                        Math.Abs(studentTestResult.ImaginationScore - 30) +
                        Math.Abs(studentTestResult.SurgencyScore - 30)) / 200;
                    personalityScore = (1 - personalityDiff) * 100;
                }

                // compute the tags score:
                int count = 0;
                int studentTagsNumber = student.User.UserTags.Count > 0? student.User.UserTags.Count: 1;
                foreach (var tag in professor.User.UserTags)
                {
                    foreach (var studentTag in student.User.UserTags)
                    {
                        if (studentTag.TagId == tag.TagId)
                        {
                            count++;
                        }
                    }
                }
                double tagsScore = count / (studentTagsNumber * 1.0) * 100;// 70% of the common tags number
                double personalityPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("PersonalityPercentage")).Value);
                double tagsPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("TagsPercentage")).Value);
                double supervisorStyleScore = ComputeSupervisorStyleScore(student, professor);
                double supervisorStylePercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("SupervisorStylePercentage")).Value);
                GetSuggestionsViewModel model = new GetSuggestionsViewModel
                {
                    Professor = professor,
                    PersonalityScore = Math.Round(personalityScore, 2),
                    TagsScore = Math.Round(tagsScore, 2),
                    SupervisorStyleScore = Math.Round(supervisorStyleScore, 2),
                    CompatibilityScore = Math.Round(personalityScore * personalityPercentage + tagsScore * tagsPercentage +
                    supervisorStyleScore * supervisorStylePercentage, 2)
                };
                finalResults.Add(model);
            }
            var sortedResults = finalResults.OrderByDescending(model => model.CompatibilityScore).ToList();

            return sortedResults;
        }

        public bool AddProfessor(Professors professor)
        {
            if (!_unitOfWork.ProfessorsRepository.CheckIfProfessorExists(professor.GDPRCode))
            {
                professor.UserId = null;
                _unitOfWork.ProfessorsRepository.Create(professor);
                _unitOfWork.Complete();
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public async Task DeleteProfessor(Professors professor)
        {
            _unitOfWork.ProfessorsRepository.Delete(professor);
            await _unitOfWork.CompleteAsync();
        }

        public async Task Import(ProfessorsImportViewModel professorsImportViewModel)
        {
            if (professorsImportViewModel.Document != null)
            {
                string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "documents");
                var fileName = Guid.NewGuid().ToString() + "_" + professorsImportViewModel.Document.FileName;
                string filePath = Path.Combine(uploadsFolder, fileName);
                var filestream = new FileStream(filePath, FileMode.Create);
                professorsImportViewModel.Document.CopyTo(filestream);
                filestream.Close();
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        List<Professors> professors = new List<Professors>();

                        var departments = _unitOfWork.DepartmentsRepository.FindAll();
                        while (reader.Read())
                        {
                            var foundProfessor = await _unitOfWork.ProfessorsRepository.FindProfessorByCondition(p => p.GDPRCode.Equals(reader.GetValue(0).ToString()));
                            if (foundProfessor == null)
                            {
                                var professor = new Professors
                                {
                                    GDPRCode = reader.GetValue(0).ToString(),
                                    FirstName = reader.GetValue(1).ToString(),
                                    LastName = reader.GetValue(2).ToString(),
                                    DepartmentId = departments.FirstOrDefault(d => d.Name.Equals(reader.GetValue(3).ToString())).Id,
                                    ProjectsNumber = Int32.Parse(reader.GetValue(4).ToString())
                                };
                                _unitOfWork.ProfessorsRepository.Create(professor);
                            }
                        }
                        await _unitOfWork.CompleteAsync();
                    }
                }
            }
        }

        public async Task<bool> CheckAvailability(Users user)
        {
            var professor = await _unitOfWork.ProfessorsRepository.FindProfessorByCondition(p => p.UserId.Equals(user.Id));
            return (professor != null && professor.ProjectsNumber > 0);
        }
    }
}
