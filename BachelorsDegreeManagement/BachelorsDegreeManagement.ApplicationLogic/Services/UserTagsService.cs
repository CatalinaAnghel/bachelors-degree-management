﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class UserTagsService : IUserTagsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserTagsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddUserTag(UserTags userTag)
        {
            _unitOfWork.UserTagsRepository.Create(userTag);
            _unitOfWork.Complete();
        }

        public async Task<UserTags> GetUserTag(int id)
        {
            return await _unitOfWork.UserTagsRepository.FindByCondition(ut => ut.Id == id).SingleOrDefaultAsync();
        }

        public bool DeleteTag(Users user, DeleteTagViewModel model)
        {
            var userTag = _unitOfWork.UserTagsRepository
                .FindByCondition(ut => ut.UserId.Equals(user.Id) && ut.TagId == model.TagId)
                .SingleOrDefault();
            if(userTag != null)
            {
                _unitOfWork.UserTagsRepository.Delete(userTag);
                _unitOfWork.Complete();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
