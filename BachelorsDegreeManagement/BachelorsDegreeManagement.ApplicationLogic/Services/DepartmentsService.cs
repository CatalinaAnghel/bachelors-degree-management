﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class DepartmentsService: IDepartmentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DepartmentsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Departments>> GetDepartments()
        {
            return await _unitOfWork.DepartmentsRepository.FindAllDepartments();
        }

        public async Task AddDepartment(CreateDepartmentViewModel model)
        {

            Departments department = new Departments
            {
                Name = model.Name
            };
            _unitOfWork.DepartmentsRepository.Create(department);
            _unitOfWork.Complete();
            var specializations = model.Specializations.TrimEnd(';', ' ').Split("; ");
            foreach (var specialization in specializations)
            {
                var data = specialization.Split(", ");
                var acronym = "";
                var words = data[0].Split(' ').ToList();
                foreach(var word in words)
                {
                    acronym += word[0];
                }
                var foundSpecialization = _unitOfWork.SpecializationsRepository
                    .FindByCondition(s => s.Name.Equals(data[0]) && s.Acronym.Equals(acronym))
                    .SingleOrDefault();
                if (foundSpecialization == null)
                {
                    Specializations s = new Specializations
                    {
                        Name = data[0],
                        DepartmentId = department.Id,
                        Acronym = acronym
                    };
                    _unitOfWork.SpecializationsRepository.Create(s);
                    _unitOfWork.Complete();
                    for (int iterator = 1; iterator <= Int32.Parse(data[1]); iterator++)
                    {
                        var groupName = acronym + "3." + iterator;

                        Groups g = new Groups
                        {
                            Name = groupName,
                            SpecializationId = s.Id,
                            StudyYear = 3
                        };
                        _unitOfWork.GroupsRepository.Create(g);
                        _unitOfWork.Complete();
                    }
                }
            }
        }

        public async Task UpdateDepartment(int id, Departments department)
        {
            var foundDepartment = await _unitOfWork.DepartmentsRepository.FindByCondition(d => d.Id == id).SingleOrDefaultAsync();
            if (!foundDepartment.Name.Equals(department.Name))
            {
                foundDepartment.Name = department.Name;
                _unitOfWork.DepartmentsRepository.Update(foundDepartment);
                await _unitOfWork.CompleteAsync();
            }
        }

        public async Task DeleteDepartment(Departments department)
        {
            _unitOfWork.DepartmentsRepository.Delete(department);
            await _unitOfWork.CompleteAsync();
        }

        public async Task<Departments> GetDepartment(int id)
        {
            return await _unitOfWork.DepartmentsRepository.FindDepartmentByCondition(d => d.Id == id);
        }

        public async Task<List<DepartmentsViewModel>> GetDepartmentsViewModel()
        {
            List<DepartmentsViewModel> models = new List<DepartmentsViewModel>();
            var departments = await _unitOfWork.DepartmentsRepository.FindAllDepartments();
            foreach(var department in departments)
            {
                models.Add(
                    new DepartmentsViewModel
                    {
                        Id = department.Id,
                        Name = department.Name,
                        NoProfessors = _unitOfWork.DepartmentsRepository.GetNumberOfProfessors(department.Id),
                        NoStudents = _unitOfWork.DepartmentsRepository.GetNumberOfStudents(department.Id),
                        NoGroups = _unitOfWork.DepartmentsRepository.GetNumberOfGroups(department.Id)
                    });
            }

            return models;
        }
    }
}
