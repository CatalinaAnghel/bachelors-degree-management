﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class ProjectsService : SupervisorStyleService, IProjectsService
    {
        public ProjectsService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public void AddProject(CreateProjectViewModel model, Users user)
        {
            var deadline = _unitOfWork.DeadlinesRepository.GetNextDeadline();
            Projects project = new Projects
            {
                Description = model.Description,
                ProfessorId = (_unitOfWork.ProfessorsRepository.FindByCondition(p => p.UserId.Equals(user.Id)).SingleOrDefault()).GDPRCode,
                Name = model.Name,
                Complexity = model.Complexity,
                DeadlineId = deadline.Id
            };
            _unitOfWork.ProjectsRepository.Create(project);
            _unitOfWork.Complete();
            var tags = model.Tags.TrimEnd(';', ' ').Split("; ");
            foreach (var tag in tags)
            {
                var foundTag = _unitOfWork.TagsRepository.FindByCondition(t => t.Name.Equals(tag)).SingleOrDefault();
                ProjectTags pt = new ProjectTags
                {
                    ProjectId = project.Id,
                    TagId = foundTag.Id
                };
                _unitOfWork.ProjectTagsRepository.Create(pt);
                _unitOfWork.Complete();
            }
        }

        public void DeleteProject(Projects project)
        {
            var deadline = _unitOfWork.DeadlinesRepository.GetNextDeadline();
            if (deadline != null && project.DeadlineId == deadline.Id)
            {
                _unitOfWork.ProjectsRepository.Delete(project);
                _unitOfWork.Complete();
            }

        }

        public async Task<Projects> GetProject(int id)
        {
            return await _unitOfWork.ProjectsRepository.FindByCondition(p => p.Id == id)
                .Include(p => p.Professor)
                .ThenInclude(prof => prof.User)
                .Include(p => p.ProjectTags)
                .ThenInclude(t => t.Tag)
                .Include(p => p.Deadline)
                .SingleOrDefaultAsync();
        }
        /// <summary>
        /// This method is called in order to retrieve the projects that have been added for the next deadline
        /// </summary>
        /// <returns></returns>
        public async Task<List<Projects>> GetAllProjects()
        {
            var deadline = _unitOfWork.DeadlinesRepository.GetNextDeadline();
            if (deadline == null)
            {
                // if there is no active deadline, then the projects will be selected from the last deadline 
                deadline = _unitOfWork.DeadlinesRepository.GetRecentDeadline();
            }
            return await _unitOfWork.ProjectsRepository.FindByCondition(p => p.DeadlineId == deadline.Id).Include(p => p.Professor)
                .ThenInclude(prof => prof.User).Include(p => p.ProjectTags).ThenInclude(t => t.Tag).ToListAsync();
        }


        public async Task<List<Projects>> GetAvailableProjects()
        {
            var deadline = _unitOfWork.DeadlinesRepository.GetNextDeadline();
            var takenProjects = _unitOfWork.ProjectsRepository.FindTakenProjects();
            if (takenProjects != null && takenProjects.Count > 0)
            {
                return await _unitOfWork.ProjectsRepository
                .FindByCondition(project => !takenProjects.Contains(project.Id) && project.DeadlineId == deadline.Id)
                .Include(p => p.Professor)
                .ThenInclude(prof => prof.User)
                .Include(p => p.ProjectTags)
                .ThenInclude(t => t.Tag)
                .ToListAsync();
            }
            else if (deadline != null)
            {
                return await _unitOfWork.ProjectsRepository
                .FindByCondition(project => project.DeadlineId == deadline.Id)
                .Include(p => p.Professor)
                .ThenInclude(prof => prof.User)
                .Include(p => p.ProjectTags)
                .ThenInclude(t => t.Tag)
                .ToListAsync();
            }
            else
            {
                return null;
            }

        }

        public async Task<List<Projects>> GetProjects(Users user)
        {
            return await _unitOfWork.ProjectsRepository.FindByCondition(p => p.Professor.UserId.Equals(user.Id)).
                Include(p => p.Professor).Include(p => p.ProjectTags).ThenInclude(t => t.Tag).Include(p => p.Deadline).ToListAsync();
        }
        public async Task<List<Projects>> GetProjectsWithMinimalInfo(Users user)
        {
            var takenProjects = _unitOfWork.ProjectsRepository.FindTakenProjects();
            var deadline = _unitOfWork.DeadlinesRepository.GetNextDeadline();
            if (deadline != null)
            {
                return await _unitOfWork.ProjectsRepository
                .FindByCondition(p => p.Professor.UserId.Equals(user.Id) && takenProjects != null && !takenProjects.Contains(p.Id) && p.DeadlineId == deadline.Id).ToListAsync();

            }
            else
            {
                return new List<Projects>();
            }
        }

        public void UpdateProject(Projects project)
        {
            _unitOfWork.ProjectsRepository.Update(project);
            _unitOfWork.Complete();
        }

        public bool CheckDeadline()
        {
            var nextDeadline = _unitOfWork.DeadlinesRepository.GetNextDeadline();
            var timePeriodForAddingProjects = _unitOfWork.SystemVariablesRepository.FindSystemVariableByCondition(v => v.Name.Equals("ProjectsInsertionNumberOfDays"));
            var today = DateTime.Now;
            if (nextDeadline != null && DateTime.Compare(today, nextDeadline.Date.AddDays(-1 * Int32.Parse(timePeriodForAddingProjects.Value))) <= 0 &&
                DateTime.Compare(today, nextDeadline.Date) <= 0)
            {
                return true;
            }
            return false;
        }

        public async Task<List<ProjectsViewModel>> GetProjectsViewModelList(List<Projects> projects, Users user, bool isProfessor = false)
        {
            List<ProjectsViewModel> models = new List<ProjectsViewModel>();
            foreach (var project in projects)
            {
                double compatibilityScore = 0;

                if (!isProfessor)
                {
                    Students student = await _unitOfWork.StudentsRepository.FindByCondition(s => s.UserId.Equals(user.Id)).FirstOrDefaultAsync();
                    compatibilityScore = await ComputeCompatibilityScore(project, student);
                }
                string cellClass = "green_cell";
                if (compatibilityScore < 70 && compatibilityScore >= 50)
                {
                    cellClass = "yellow_cell";
                }
                else if (compatibilityScore < 50)
                {
                    cellClass = "red_cell";
                }

                ProjectsViewModel model = new ProjectsViewModel
                {
                    Project = project,
                    CompatibilityScore = Math.Round(compatibilityScore, 2),
                    Class = cellClass
                };
                models.Add(model);
            }
            return models.OrderByDescending(m => m.CompatibilityScore).ToList();
        }

        public async Task<ProjectViewModel> GetProjectViewModel(Projects project, Users user, bool isProfessor = false)
        {
            ProjectViewModel model = null;
            if (!isProfessor)
            {
                Students student = await _unitOfWork.StudentsRepository.FindByCondition(s => s.UserId.Equals(user.Id)).FirstOrDefaultAsync();

                // compute the scores
                double supervisorScore = ComputeSupervisorStyleScore(student, project.Professor);
                double domainScore = ComputeTagsScore(project, student);
                double personalityScore = await ComputePersonalityScore(project, student);
                double complexityScore = ComputeComplexityScore(project, student);

                // get percentages
                double personalityPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                .FindSystemVariableByCondition(v => v.Name.Equals("ProjectPersonalityPercentage")).Value);
                double tagsPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("ProjectTagsPercentage")).Value);
                double complexityPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("ProjectComplexityPercentage")).Value);
                double supervisorStylePercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("SupervisorStylePercentage")).Value);

                // compute the overall compatibility
                double compatibilityScore = personalityScore * personalityPercentage + domainScore * tagsPercentage +
                    complexityScore * complexityPercentage + supervisorScore * supervisorStylePercentage;

                // put the data into the viewmodel
                model = new ProjectViewModel
                {
                    Project = project,
                    OverallCompatibility = Math.Round(compatibilityScore, 2),
                    SupervisorStyleCompatibility = Math.Round(supervisorScore, 2),
                    PersonalityCompatibility = Math.Round(personalityScore, 2),
                    ComplexityCompatibility = Math.Round(complexityScore, 2)
                };
            }
            else
            {
                model = new ProjectViewModel
                {
                    Project = project,
                    OverallCompatibility = 0,
                    SupervisorStyleCompatibility = 0,
                    PersonalityCompatibility = 0,
                    DomainCompatibility = 0,
                    ComplexityCompatibility = 0
                };
            }

            return model;
        }


        private async Task<double> ComputePersonalityScore(Projects project, Students student)
        {
            var professorTestResult = await _unitOfWork.PersonalityTestResultsRepository
                .FindByCondition(t => t.UserId.Equals(project.Professor.UserId))
                .SingleOrDefaultAsync();
            // compute the euclidian distance
            var studentTestResult = await _unitOfWork.PersonalityTestResultsRepository
                .FindByCondition(t => t.UserId.Equals(student.UserId))
                .SingleOrDefaultAsync();
            double personalityScore = 0.0;
            if (professorTestResult != null && studentTestResult != null)
            {
                double personalityDiff = (Math.Abs(studentTestResult.AgreeablenessScore - professorTestResult.AgreeablenessScore) +
                        Math.Abs(studentTestResult.ConscientScore - professorTestResult.ConscientScore) +
                        Math.Abs(studentTestResult.EmotionalScore - professorTestResult.EmotionalScore) +
                        Math.Abs(studentTestResult.ImaginationScore - professorTestResult.ImaginationScore) +
                        Math.Abs(studentTestResult.SurgencyScore - professorTestResult.SurgencyScore)) / 200;
                personalityScore = (1 - personalityDiff) * 100;
            }
            else
            {
                double personalityDiff = (Math.Abs(studentTestResult.AgreeablenessScore - 30) +
                        Math.Abs(studentTestResult.ConscientScore - 30) +
                        Math.Abs(studentTestResult.EmotionalScore - 30) +
                        Math.Abs(studentTestResult.ImaginationScore - 30) +
                        Math.Abs(studentTestResult.SurgencyScore - 30)) / 200;
                personalityScore = (1 - personalityDiff) * 100;
            }

            return personalityScore;
        }

        private double ComputeTagsScore(Projects project, Students student)
        {
            // compute the tags score:
            int count = 0;
            var userTags = _unitOfWork.UserTagsRepository.FindByCondition(ut => ut.UserId.Equals(student.UserId)).ToList();
            var studentTagsNumber = (userTags != null && userTags.Count() > 0) ? userTags.Count() : 1;
            foreach (var tag in project.ProjectTags)
            {
                foreach (var studentTag in userTags)
                {
                    if (studentTag.TagId == tag.TagId)
                    {
                        count++;
                    }
                }
            }
            double tagsScore = count / (studentTagsNumber * 1.0) * 100;
            return tagsScore;
        }

        private double ComputeComplexityScore(Projects project, Students student)
        {
            double score = 0.0;
            if (student.Complexity != null)
            {
                var complexityLevel = Int32.Parse(student.Complexity.Split("Level")[1]);
                var projectComplexityLevel = Int32.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Value.Equals(project.Complexity)).Name
                    .Split("Level")[1]);
                var levelDifference = Math.Abs(complexityLevel - projectComplexityLevel);
                switch (levelDifference)
                {
                    case 0:
                        // the same complexity
                        score = double.Parse(_unitOfWork.SystemVariablesRepository
                            .FindSystemVariableByCondition(s => s.Name.Equals("ComplexityScore0")).Value);
                        break;
                    case 1:
                        score = double.Parse(_unitOfWork.SystemVariablesRepository
                            .FindSystemVariableByCondition(s => s.Name.Equals("ComplexityScore1")).Value);
                        break;
                    case 2:
                        score = double.Parse(_unitOfWork.SystemVariablesRepository
                            .FindSystemVariableByCondition(s => s.Name.Equals("ComplexityScore2")).Value);
                        break;
                }
            }
            return score * 100;
        }

        private async Task<double> ComputeCompatibilityScore(Projects project, Students student)
        {
            double personalityScore = await ComputePersonalityScore(project, student);
            double tagsScore = ComputeTagsScore(project, student);
            double complexityScore = ComputeComplexityScore(project, student);
            double supervisorStyleScore = ComputeSupervisorStyleScore(student, project.Professor);


            double personalityPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                .FindSystemVariableByCondition(v => v.Name.Equals("ProjectPersonalityPercentage")).Value);
            double tagsPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                .FindSystemVariableByCondition(v => v.Name.Equals("ProjectTagsPercentage")).Value);
            double complexityPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                .FindSystemVariableByCondition(v => v.Name.Equals("ProjectComplexityPercentage")).Value);
            double supervisorStylePercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                .FindSystemVariableByCondition(v => v.Name.Equals("SupervisorStylePercentage")).Value);

            return personalityScore * personalityPercentage + tagsScore * tagsPercentage +
                complexityScore * complexityPercentage + supervisorStyleScore * supervisorStylePercentage;
        }
    }
}
