﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class LoginService : ILoginService
    {
        private readonly SignInManager<Users> _signInManager;

        public LoginService(SignInManager<Users> signInManager)
        {
            _signInManager = signInManager;
        }

        public async Task<SignInResult> Login(string email, string password, bool rememberMe)
        {
            return await _signInManager.PasswordSignInAsync(email, password, rememberMe, lockoutOnFailure: false);
        }
    }
}
