﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class StudentsService : SupervisorStyleService, IStudentsService
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public StudentsService(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment): base(unitOfWork)
        {
            _hostingEnvironment = hostEnvironment;
        }

        public async Task<Students> GetStudent(string GDPRCode)
        {
            return await _unitOfWork.StudentsRepository.FindByCondition(student => student.GDPRCode.Equals(GDPRCode))
                .Include(student => student.User)
                .Include(student => student.Group)
                .SingleOrDefaultAsync();
        }

        public async Task<Students> GetStudentByUser(Users user)
        {
            return await _unitOfWork.StudentsRepository.FindByCondition(p => p.UserId.Equals(user.Id)).SingleOrDefaultAsync();
        }

        public async Task<List<Students>> GetStudents()
        {
            return await _unitOfWork.StudentsRepository.FindAll().Include(s => s.Group)
                .ThenInclude(g => g.Specialization)
                .Include(s => s.User)
                .ToListAsync();
        }

        public List<Students> GetOrderedStudents()
        {
            var students = _unitOfWork.StudentsRepository.FindAvailableStudents();
            return students;
        }

        public bool CreateStudent(Students student)
        {
            if (!_unitOfWork.StudentsRepository.CheckIfStudentExists(student.GDPRCode))
            {
                student.UserId = null;
                _unitOfWork.StudentsRepository.Create(student);
                _unitOfWork.Complete();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateStudent(Students student)
        {
            var foundStudent = _unitOfWork.StudentsRepository
                .FindByCondition(s => s.GDPRCode.Equals(student.GDPRCode)).Single();
            if (student.FirstName != null && student.FirstName != foundStudent.FirstName)
            {
                foundStudent.FirstName = student.FirstName;
            }
            if (student.LastName != null && student.LastName != foundStudent.LastName)
            {
                foundStudent.LastName = student.LastName;
            }
            if (student.Grade != 0 && student.Grade != foundStudent.Grade)
            {
                foundStudent.Grade = student.Grade;
            }
            if (student.GroupId > 0 && student.GroupId != foundStudent.GroupId)
            {
                foundStudent.GroupId = student.GroupId;
            }
            if (foundStudent.UserId == null && student.UserId != null)
            {
                foundStudent.UserId = student.UserId;
            }
            if (foundStudent.Complexity != student.Complexity && student.Complexity != null)
            {
                foundStudent.Complexity = student.Complexity;
            }
            try
            {
                _unitOfWork.StudentsRepository.Update(foundStudent);
                _unitOfWork.Complete();
                return true;
            }catch(Exception e)
            {
                throw;
            }
        }
            

        public void DeleteStudent(Students student)
        {
            _unitOfWork.StudentsRepository.Delete(student);
            _unitOfWork.Complete();
        }

        /**
         * The result will be: results[Student] = [personalityScore, tagsScore, totalScore]
         */
        public async Task<List<GetStudentsSuggestionsViewModel>> FindSuggestedStudents(Users user)
        {
            // Get the student's information
            var professor = await _unitOfWork.ProfessorsRepository
                .FindByCondition(p => p.UserId.Equals(user.Id))
                .Include(p => p.Department)
                .Include(p => p.Projects)
                .Include(p => p.User)
                .ThenInclude(u => u.UserTags)
                .ThenInclude(ut => ut.Tag)
                .SingleOrDefaultAsync();
            var professorTestResult = await _unitOfWork.PersonalityTestResultsRepository
                .FindByCondition(t => t.UserId.Equals(user.Id))
                .SingleOrDefaultAsync();

            // Get the professors that have an account
            var students = await _unitOfWork.StudentsRepository
                .FindByCondition(s => s.UserId != null)
                .Include(s => s.User)
                .ThenInclude(u => u.UserTags)
                .ThenInclude(ut => ut.Tag)
                .ToListAsync();

            // compute the results:
            List<GetStudentsSuggestionsViewModel> finalResults = new List<GetStudentsSuggestionsViewModel>();
            foreach (var student in students)
            {
                // compute the personality score
                var studentTestResult = await _unitOfWork.PersonalityTestResultsRepository
                    .FindByCondition(t => t.UserId.Equals(student.UserId))
                    .SingleOrDefaultAsync();
                var personalityScore = 0.0;
                if (professorTestResult != null && studentTestResult != null)
                {
                    double personalityDiff = (Math.Abs(studentTestResult.AgreeablenessScore - professorTestResult.AgreeablenessScore) +
                        Math.Abs(studentTestResult.ConscientScore - professorTestResult.ConscientScore) +
                        Math.Abs(studentTestResult.EmotionalScore - professorTestResult.EmotionalScore) +
                        Math.Abs(studentTestResult.ImaginationScore - professorTestResult.ImaginationScore) +
                        Math.Abs(studentTestResult.SurgencyScore - professorTestResult.SurgencyScore)) / 200;
                    personalityScore = (1 - personalityDiff) * 100;
                }
                else
                {
                    double personalityDiff = (Math.Abs(30 - professorTestResult.AgreeablenessScore) +
                       Math.Abs(30 - professorTestResult.ConscientScore) +
                       Math.Abs(30 - professorTestResult.EmotionalScore) +
                       Math.Abs(30 - professorTestResult.ImaginationScore) +
                       Math.Abs(30 - professorTestResult.SurgencyScore)) / 200;
                    personalityScore = (1 - personalityDiff) * 100;
                }

                // compute the tags score:
                int count = 0;
                int studentTagsNumber = student.User.UserTags.Count > 0? student.User.UserTags.Count: 1;
                foreach (var tag in professor.User.UserTags)
                {
                    foreach (var studentTag in student.User.UserTags)
                    {
                        if (studentTag.TagId == tag.TagId)
                        {
                            count++;
                        }
                    }
                }
                double tagsScore = count / (studentTagsNumber * 1.0) * 100;// 70% of the common tags number
                double personalityPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("PersonalityPercentage")).Value);
                double tagsPercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("TagsPercentage")).Value);
                double supervisorStyleScore = ComputeSupervisorStyleScore(student, professor);
                double supervisorStylePercentage = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("SupervisorStylePercentage")).Value);

                GetStudentsSuggestionsViewModel model = new GetStudentsSuggestionsViewModel
                {
                    Student = student,
                    PersonalityScore = Math.Round(personalityScore, 2),
                    TagsScore = Math.Round(tagsScore, 2),
                    SupervisorStyleScore = Math.Round(supervisorStyleScore, 2),
                    CompatibilityScore = Math.Round(personalityScore * personalityPercentage + tagsScore * tagsPercentage +
                        supervisorStyleScore * supervisorStylePercentage, 2)
                };
                finalResults.Add(model);
            }
            finalResults = finalResults.OrderByDescending(model => model.CompatibilityScore).ToList();

            return finalResults;
        }

        public async Task Import(StudentsImportViewModel studentsImportViewModel)
        {
            try
            {
                string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "documents");
                var fileName = Guid.NewGuid().ToString() + "_" + studentsImportViewModel.Document.FileName;
                string filePath = Path.Combine(uploadsFolder, fileName);
                var filestream = new FileStream(filePath, FileMode.Create);
                studentsImportViewModel.Document.CopyTo(filestream);
                filestream.Close();
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        List<Students> students = new List<Students>();

                        var groups = _unitOfWork.GroupsRepository.FindAll();
                        while (reader.Read())
                        {
                            var foundStudent = await _unitOfWork.StudentsRepository
                                .FindStudentByCondition(s => s.GDPRCode.Equals(reader.GetValue(0)
                                .ToString()));
                            if (foundStudent == null)
                            {
                                var student = new Students
                                {
                                    GDPRCode = reader.GetValue(0).ToString(),
                                    FirstName = reader.GetValue(1).ToString(),
                                    LastName = reader.GetValue(2).ToString(),
                                    GroupId = groups.FirstOrDefault(g => g.Name.Equals(reader.GetValue(3).ToString())).Id,
                                    Grade = float.Parse(reader.GetValue(4).ToString(), CultureInfo.InvariantCulture.NumberFormat)
                                };
                                _unitOfWork.StudentsRepository.Create(student);
                            }
                        }
                        await _unitOfWork.CompleteAsync();
                    }
                }
            }catch(Exception e)
            {
                throw e;
            }
        }
    }
}
