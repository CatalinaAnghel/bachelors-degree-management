﻿
using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using CsvHelper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class ExportService : IExportService
    {
        private string _filePath;
        private readonly ISystemVariablesService systemVariablesService;
        private readonly IOptionsFactoryService optionsFactoryService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IUnitOfWork unitOfWork;

        public ExportService(ISystemVariablesService systemVariablesService,
            IOptionsFactoryService optionsFactoryService, IWebHostEnvironment webHostEnvironment,
            IUnitOfWork unitOfWork)
        {
            this._filePath = "";
            this.systemVariablesService = systemVariablesService;
            this.optionsFactoryService = optionsFactoryService;
            this._hostingEnvironment = webHostEnvironment;
            this.unitOfWork = unitOfWork;
        }

        public void SetFilePath(string path)
        {
            this._filePath = path;
        }

        public async Task<string> ExportData(bool studentData = true)
        {
            if (studentData)
            {
                var students = await this.unitOfWork.StudentsRepository.GetStudentsInfo();
                List<ExportStudentsViewModel> exportStudents = new List<ExportStudentsViewModel>();
                if(students.Count() > 0)
                {
                    foreach (var student in students)
                    {
                        AcceptedProposalViewModel acceptedProposal = await this.optionsFactoryService.getAcceptedProposal(student.UserId);

                        if (acceptedProposal != null) { 
                            var supervisoryStyle = this.systemVariablesService
                            .GetVariableByName("SupervisorStyle_" + student.SupervisorStyle);
                            ExportStudentsViewModel exportStudent = new ExportStudentsViewModel
                            {
                                Code = student.GDPRCode,
                                FirstName = student.FirstName,
                                LastName = student.LastName,
                                Email = student.User.Email,
                                PhoneNumber = student.User.PhoneNumber,
                                Specialization = student.Group.Specialization.Name,
                                SupervisoryPlan = supervisoryStyle != null ? supervisoryStyle.Value: "Laissez-faire",
                                ProjectDescription = acceptedProposal.Description,
                                ProjectTitle = acceptedProposal.Title,
                                SupervisorCode = acceptedProposal.SupervisorCode
                            };

                            exportStudents.Add(exportStudent);
                        }
                    }
                }
                using (var writer = new StreamWriter(this._hostingEnvironment.WebRootPath + this._filePath))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(exportStudents);
                }
            }
            else
            {
                var professors = await this.unitOfWork.ProfessorsRepository
                    .FindByCondition(p => p.UserId != null)
                    .Include(p => p.Department)
                    .Include(p => p.User).ToListAsync();

                List<ExportSupervisorViewModel> exportSupervisors = new List<ExportSupervisorViewModel>();

                if (professors.Count() > 0)
                {
                    foreach (var professor in professors)
                    {
                        var supervisoryStyle = this.systemVariablesService
                            .GetVariableByName("SupervisorStyle_" + professor.SupervisorStyle);
                        ExportSupervisorViewModel exportProfessor = new ExportSupervisorViewModel
                        {
                                Code = professor.GDPRCode,
                                FirstName = professor.FirstName,
                                LastName = professor.LastName,
                                Email = professor.User.Email,
                                PhoneNumber = professor.User.PhoneNumber,
                                SupervisoryPlan = supervisoryStyle != null ? supervisoryStyle.Value : "Laissez-faire",
                                Department = professor.Department.Name
                                
                            };

                            exportSupervisors.Add(exportProfessor);
                    }
                }
                using (var writer = new StreamWriter(this._hostingEnvironment.WebRootPath + this._filePath))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(exportSupervisors);
                }
            }

            return this._filePath.Replace("\\", "/");
        }
    }
}
