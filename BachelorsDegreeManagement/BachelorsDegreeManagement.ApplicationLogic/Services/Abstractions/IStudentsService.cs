﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IStudentsService
    {
        public Task<Students> GetStudent(string GDPRCode);
        public Task<List<Students>> GetStudents();
        public Task<Students> GetStudentByUser(Users user);
        public List<Students> GetOrderedStudents();
        public bool CreateStudent(Students student);
        public bool UpdateStudent(Students student);
        public void DeleteStudent(Students student);
        public Task<List<GetStudentsSuggestionsViewModel>> FindSuggestedStudents(Users user);
        public Task Import(StudentsImportViewModel studentsImportViewModel);
    }
}
