﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface ITagsService
    {
        public Task<Tags> GetTag(int? id);
        public Task<List<Tags>> GetTags();
        public List<Tags> GetUsersTags(Users user);
        public List<Tags> GetAvailableTags(Users user);
        public bool AddTag(Tags tag, Users user);
        public void UpdateTag(Tags tag);
        public void DeleteTag(Tags tag);
        public List<TagsViewModel> GetTagModels(List<Tags> userTags);
    }
}
