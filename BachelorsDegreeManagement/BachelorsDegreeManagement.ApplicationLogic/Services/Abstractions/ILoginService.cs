﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface ILoginService
    {
        public Task<SignInResult> Login(string email, string password, bool rememberMe);
    }
}
