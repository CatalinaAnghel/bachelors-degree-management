﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IPersonalityTestResultsService
    {
        public void AddTestResult(PersonalityTestResults result);
        public void UpdateTestResult(PersonalityTestResults result);
        public Task<PersonalityTestResults> GetResult(Users user);
        public Task<List<PersonalityTestResults>> GetResults();
        public void ComputeResult(QuizViewModel model, Users user);
        public Task<bool> CheckPersonalityTest(Users user);
    }
}
