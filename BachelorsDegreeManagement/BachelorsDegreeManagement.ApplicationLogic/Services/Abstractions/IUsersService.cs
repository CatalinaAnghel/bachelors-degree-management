﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IUsersService
    {
        public Task<Users> GetCurrentUser(ClaimsPrincipal claims);
        public Task<Users> GetUser(string id);
        public Task DeleteUser(Users user);
    }
}
