﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IEmailService
    {
        public MimeMessage Create(string receiver, string subject, string html);
        public void Send(MimeMessage email);
    }
}
