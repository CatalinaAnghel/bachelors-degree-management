﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IUserTagsService
    {
        public void AddUserTag(UserTags userTag);
        public Task<UserTags> GetUserTag(int id);
        public bool DeleteTag(Users user, DeleteTagViewModel model);
    }
}
