﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IDeadlinesService
    {
        public Task<bool> AddDeadline(Deadlines deadline);
        public Task<bool> DeleteDeadline(Deadlines deadline);
        public Task<bool> UpdateDeadline(Deadlines deadline, bool inactiveEnabled = false);
        public void UpdateDeadlineSync(Deadlines deadline, bool inactiveEnabled = false);
        public Task<Deadlines> GetDeadline(int id);
        public Task<List<Deadlines>> GetDeadlines();
        public Task<int> ValidateDeadlines();
        public Task<Deadlines> GetCurrentActiveDeadline();
        public Deadlines GetRecentDeadline();
        public Deadlines GetNextDeadline();

    }
}
