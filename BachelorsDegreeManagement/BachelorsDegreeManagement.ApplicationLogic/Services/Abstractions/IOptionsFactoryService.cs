﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IOptionsFactoryService
    {
        public Task<bool> RegisterOption(Users user, CreateOptionViewModel model);
        public Task<List<CustomOptions>> FindSentCustomOptions(Users user);
        public Task<List<CustomOptions>> FindReceivedCustomOptions(Users user);
        public Task<List<Options>> FindSentOptions(Users user);
        public Task<List<Options>> FindReceivedOptions(Users user);
        public Task<List<Options>> FindOptions();
        public Task<List<CustomOptions>> FindCustomOptions();
        public Task<Options> FindOption(int id);
        public Task<CustomOptions> FindCustomOption(int id);

        /// <summary>
        /// This is the function used to get the options for the professor.
        /// The options will be in an array list and the list's elements can be CustomOptions objects or Options 
        /// objects.The result is sorted depending on the grade of the option's student
        /// </summary>
        /// <param name="professor"> The professor that asks to see the options</param>
        /// <returns>sorted array list with the options/custom options</returns>
        public ArrayList GetOptionsForProfessor(Professors professor);

        /// <summary>
        /// This is a method used to get the data from a serialized JSON and it is used to sort the options.
        /// </summary>
        /// <param name="postString">The serialized JSON</param>
        /// <returns></returns>
        public Task SortOptions(string postString);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Step1();
        public bool Step2();

        public Task<bool> UpdateRegularOption(Options option);
        public Task<bool> UpdateCustomOption(CustomOptions option);
        public Task DeleteRegularOption(Options option);
        public Task DeleteCustomOption(CustomOptions option);
        public void FinishAllocation();
        public Task<AcceptedProposalViewModel> getAcceptedProposal(string userId);
    }
}
