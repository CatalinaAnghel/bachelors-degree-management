﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface ICustomOptionsService
    {
        public void AddCustomOption(CustomOptions option);
        public void UpdateCustomOption(CustomOptions option);
        public void DeleteCustomOption(CustomOptions option);
        public Task<CustomOptions> GetCustomOption(int id);
        public Task<List<CustomOptions>> GetSentOptions(Users user);
        public Task<List<CustomOptions>> GetReceivedOptions(Users user);
        public Task<List<CustomOptions>> GetCustomOptions();
        public Task<List<CustomOptions>> GetWaitingOptions(Users user);
        public List<CustomOptions> GetWaitingOptionsSync(Users user);
        public Task<List<CustomOptions>> GetAllWaitingOptions();
        public Task<CustomOptions> FindOptionByNumber(Students student, int numberForStudent);
        public CustomOptions FindOptionByNumberSync(Students student, int numberForStudent);
        public bool CheckStatus(int id, string status);
    }
}
