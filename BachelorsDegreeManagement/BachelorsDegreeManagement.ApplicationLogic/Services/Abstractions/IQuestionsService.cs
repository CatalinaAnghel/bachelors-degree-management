﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IQuestionsService
    {
        public Task<List<Questions>> GetQuestions();

    }
}
