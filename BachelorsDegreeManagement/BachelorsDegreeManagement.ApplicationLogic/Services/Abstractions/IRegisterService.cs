﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IRegisterService
    {
        public Task<IdentityResult> Register(string GDPRCode, string username, string password, string email, string role, string phoneNumber);
    }
}
