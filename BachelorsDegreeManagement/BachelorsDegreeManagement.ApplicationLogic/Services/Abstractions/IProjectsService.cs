﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IProjectsService
    {
        public void AddProject(CreateProjectViewModel model, Users user);
        public void UpdateProject(Projects project);
        public void DeleteProject(Projects project);
        public Task<Projects> GetProject(int id);
        public Task<List<Projects>> GetAllProjects();
        public Task<List<Projects>> GetProjects(Users user);
        public Task<List<Projects>> GetProjectsWithMinimalInfo(Users user);
        public Task<List<Projects>> GetAvailableProjects();
        public bool CheckDeadline();
        public Task<List<ProjectsViewModel>> GetProjectsViewModelList(List<Projects> projects, Users user, bool isProfessor);
        public Task<ProjectViewModel> GetProjectViewModel(Projects project, Users user, bool isProfessor);
    }
}
