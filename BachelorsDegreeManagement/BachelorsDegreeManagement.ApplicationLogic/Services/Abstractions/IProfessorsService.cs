﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IProfessorsService
    {
        public bool AddProfessor(Professors professor);
        public Task DeleteProfessor(Professors professor);
        public Task<Professors> GetProfessor(string GDPRCode);
        public Task<Professors> GetProfessorByUser(Users user);
        public Task<List<Professors>> GetProfessors();
        public List<Professors> GetProfessorsSync();
        public Task<List<Professors>> GetRegisteredProfessors();
        public void UpdateProfessor(Professors professor);
        public Task<List<Professors>> FindProfessorsByName(string name);
        public Task<List<GetSuggestionsViewModel>> FindSuggestedProfessors(Users user);
        public Task Import(ProfessorsImportViewModel studentsImportViewModel);
        public Task<bool> CheckAvailability(Users user);
    }
}
