﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface ISystemVariablesService
    {
        public SystemVariables GetVariableByName(string name);
        public List<SystemVariables> GetVariableLikeName(string name);
        public List<SystemVariables> GetComplexities();
        public UpdateWeightsViewModel GetWeights();
        public bool UpdateVariables(UpdateWeightsViewModel model);
    }
}
