﻿using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IExportService
    {
        public Task<string> ExportData(bool studentData = true);
        public void SetFilePath(string path);
    }
}
