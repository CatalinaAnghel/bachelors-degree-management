﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IDepartmentsService
    {
        public Task<List<Departments>> GetDepartments();
        public Task<Departments> GetDepartment(int id);
        public Task AddDepartment(CreateDepartmentViewModel department);
        public Task UpdateDepartment(int id, Departments department);
        public Task DeleteDepartment(Departments department);
        public Task<List<DepartmentsViewModel>> GetDepartmentsViewModel();
    }
}
