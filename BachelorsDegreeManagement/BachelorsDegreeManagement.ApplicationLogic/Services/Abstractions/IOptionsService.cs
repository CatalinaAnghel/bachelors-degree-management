﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions
{
    public interface IOptionsService
    {
        public void AddOption(Options option);
        public void UpdateOption(Options option);
        public void DeleteOption(Options option);
        public Task<Options> GetOption(int id);
        public Task<List<Options>> GetSentOptions(Users user);
        public Task<List<Options>> GetReceivedOptions(Users user);
        public Task<List<Options>> GetOptions();
        public Task<List<Options>> GetWaitingOptions(Users user);
        public List<Options> GetWaitingOptionsSync(Users user);
        public Task<Options> FindOptionByNumber(Students student, int numberForStudent);
        public Task<List<Options>> GetAllWaitingOptions();
        public Options FindOptionByNumberSync(Students student, int numberForStudent);
        public bool CheckStatus(int id, string status);
    }
}
