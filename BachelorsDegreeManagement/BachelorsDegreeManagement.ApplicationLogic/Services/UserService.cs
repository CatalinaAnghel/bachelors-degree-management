﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class UserService: IUsersService
    {
        protected readonly UserManager<Users> _userManager;
        protected readonly SignInManager<Users> _signInManager;

        public UserService(UserManager<Users> userManager, SignInManager<Users> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task DeleteUser(Users user)
        {
            await _userManager.DeleteAsync(user);
        }

        public async Task<Users> GetCurrentUser(ClaimsPrincipal claims)
        {
            return await _userManager.GetUserAsync(claims);
        }

        public async Task<Users> GetUser(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }
    }
}
