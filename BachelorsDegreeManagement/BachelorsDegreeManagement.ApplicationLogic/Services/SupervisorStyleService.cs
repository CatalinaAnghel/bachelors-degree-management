﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class SupervisorStyleService
    {
        protected readonly IUnitOfWork _unitOfWork;
        public SupervisorStyleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public double ComputeSupervisorStyleScore(Students student, Professors professor)
        {
            int supportStudent = student.SupervisorStyle != null ? (int)student.SupervisorStyle / 2 : 0;
            int structureStudent = student.SupervisorStyle != null ? (int)student.SupervisorStyle % 2 : 0;
            int supportProfessor = professor.SupervisorStyle != null ? (int)professor.SupervisorStyle / 2 : 0;
            int structureProfessor = professor.SupervisorStyle != null ? (int)professor.SupervisorStyle % 2 : 0;
            double score = 0.0f;
            int type = Math.Abs(supportProfessor - supportStudent) + Math.Abs(structureProfessor - structureStudent);
            score = double.Parse(_unitOfWork.SystemVariablesRepository
                    .FindSystemVariableByCondition(v => v.Name.Equals("SupervisorStyleScore" + type.ToString())).Value) * 100;

            return score;
        }
    }
}
