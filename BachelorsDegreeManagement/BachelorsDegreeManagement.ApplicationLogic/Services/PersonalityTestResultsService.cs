﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class PersonalityTestResultsService: IPersonalityTestResultsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public IPersonalityTestResultsRepository ResultsRepository { get; }
        public PersonalityTestResultsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddTestResult(PersonalityTestResults result)
        {
            _unitOfWork.PersonalityTestResultsRepository.Create(result);
            _unitOfWork.Complete();
        }

        public void UpdateTestResult(PersonalityTestResults result)
        {
            _unitOfWork.PersonalityTestResultsRepository.Update(result);
            _unitOfWork.Complete();
        }

        public async Task<PersonalityTestResults> GetResult(Users user)
        {
            var result = await _unitOfWork.PersonalityTestResultsRepository
                .FindByCondition(result => result.UserId == user.Id)
                .SingleOrDefaultAsync();
            if(result != null)
            {
                result.User = null;
            }
            return result;
        }

        public async Task<List<PersonalityTestResults>> GetResults()
        {
            return await _unitOfWork.PersonalityTestResultsRepository.FindAll().ToListAsync();
        }

        public void ComputeResult(QuizViewModel model, Users user)
        {
            PersonalityTestResults result = new PersonalityTestResults();
            // the user submitted the quiz
            for (int iterator = 0; iterator < 50; iterator++)
            {
                switch (model.Questions[iterator].CategoryId)
                {
                    case 1:
                        if (model.Questions[iterator].Keyed.Equals("+"))
                        {
                            // it is positive keyed
                            result.SurgencyScore += model.Answers[iterator];
                        }
                        else
                        {
                            // negative keyed
                            result.SurgencyScore += (6 - model.Answers[iterator]);
                        }
                        break;
                    case 2:
                        if (model.Questions[iterator].Keyed.Equals("+"))
                        {
                            // it is positive keyed
                            result.AgreeablenessScore += model.Answers[iterator];
                        }
                        else
                        {
                            // negative keyed
                            result.AgreeablenessScore += (6 - model.Answers[iterator]);
                        }
                        break;
                    case 3:
                        if (model.Questions[iterator].Keyed.Equals("+"))
                        {
                            // it is positive keyed
                            result.EmotionalScore += model.Answers[iterator];
                        }
                        else
                        {
                            // negative keyed
                            result.EmotionalScore += (6 - model.Answers[iterator]);
                        }
                        break;
                    case 4:
                        if (model.Questions[iterator].Keyed.Equals("+"))
                        {
                            // it is positive keyed
                            result.ConscientScore += model.Answers[iterator];
                        }
                        else
                        {
                            // negative keyed
                            result.ConscientScore += (6 - model.Answers[iterator]);
                        }
                        break;
                    case 5:
                        if (model.Questions[iterator].Keyed.Equals("+"))
                        {
                            // it is positive keyed
                            result.ImaginationScore += model.Answers[iterator];
                        }
                        else
                        {
                            // negative keyed
                            result.ImaginationScore += (6 - model.Answers[iterator]);
                        }
                        break;
                }
            }
            result.User = user;
            var foundTest = _unitOfWork.PersonalityTestResultsRepository.FindByCondition(result => result.UserId == user.Id).SingleOrDefault();
            if(foundTest != null)
            {
                foundTest.SurgencyScore = result.SurgencyScore;
                foundTest.ImaginationScore = result.ImaginationScore;
                foundTest.AgreeablenessScore = result.AgreeablenessScore;
                foundTest.ConscientScore = result.ConscientScore;
                foundTest.EmotionalScore = result.EmotionalScore;
                _unitOfWork.PersonalityTestResultsRepository.Update(foundTest);
                _unitOfWork.Complete();
            }
            else {
                foundTest = new PersonalityTestResults();
                foundTest.SurgencyScore = result.SurgencyScore;
                foundTest.ImaginationScore = result.ImaginationScore;
                foundTest.AgreeablenessScore = result.AgreeablenessScore;
                foundTest.ConscientScore = result.ConscientScore;
                foundTest.EmotionalScore = result.EmotionalScore;
                foundTest.UserId = user.Id;
                _unitOfWork.PersonalityTestResultsRepository.Create(foundTest);
                _unitOfWork.Complete();
            }
            
        }

        public async Task<bool> CheckPersonalityTest(Users user)
        {
            var result = await _unitOfWork.PersonalityTestResultsRepository
                .FindByCondition(result => result.UserId == user.Id)
                .SingleOrDefaultAsync();

            return result != null;
        }
    }
}
