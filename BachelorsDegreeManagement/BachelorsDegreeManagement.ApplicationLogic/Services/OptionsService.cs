﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class OptionsService : IOptionsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OptionsService( IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void AddOption(Options option)
        {

            _unitOfWork.OptionsRepository.Create(option);
            _unitOfWork.Complete();
        }

        public void DeleteOption(Options option)
        {
            _unitOfWork.OptionsRepository.Delete(option);
            _unitOfWork.Complete();
        }

        public async Task<Options> GetOption(int id)
        {
            return await _unitOfWork.OptionsRepository.FindOptionByCondition(option => option.Id == id);
        }

        public async Task<List<Options>> GetReceivedOptions(Users user)
        {
            var professor = await _unitOfWork.ProfessorsRepository.FindProfessorByCondition(p => p.UserId.Equals(user.Id));
            var projects = await _unitOfWork.ProjectsRepository.FindProjectsByCondition(p => p.ProfessorId.Equals(professor.GDPRCode));
            return await _unitOfWork.OptionsRepository
                .FindOptionsByCondition(o => projects.Contains(o.Project));
        }

        public async Task<List<Options>> GetSentOptions(Users user)
        {
            return await _unitOfWork.OptionsRepository.FindOptionsByCondition(o => o.Student.UserId.Equals(user.Id));
        }

        public async Task<List<Options>> GetWaitingOptions(Users user)
        {
            return await _unitOfWork.OptionsRepository.FindByCondition(o => o.Student.UserId.Equals(user.Id) && o.Status.Equals("waiting")).ToListAsync();
        }

        public List<Options> GetWaitingOptionsSync(Users user)
        {
            return _unitOfWork.OptionsRepository.FindByCondition(o => o.Student.UserId.Equals(user.Id) && o.Status.Equals("waiting")).ToList();
        }

        public void UpdateOption(Options option)
        {
            _unitOfWork.OptionsRepository.Update(option);
            _unitOfWork.Complete();
        }

        public async Task<List<Options>> GetOptions()
        {
            return await _unitOfWork.OptionsRepository.FindAllOptions();
        }

        public async Task<Options> FindOptionByNumber(Students student, int numberForStudent)
        {
            return await _unitOfWork.OptionsRepository.FindOptionByCondition(o => o.NumberForUser == numberForStudent && o.Student == student);
        }

        public Options FindOptionByNumberSync(Students student, int numberForStudent)
        {
            return _unitOfWork.OptionsRepository.FindByCondition(o => o.NumberForUser == numberForStudent && o.Student == student)
                .Include(o => o.Project).ThenInclude(p => p.Professor).SingleOrDefault();
        }

        public async Task<List<Options>> GetAllWaitingOptions()
        {
            return await _unitOfWork.OptionsRepository
                .FindOptionsByCondition(o => o.Status == "waiting");
        }

        public bool CheckStatus(int id, string status)
        {
            return _unitOfWork.OptionsRepository.FindByCondition(o => o.Id == id && o.Status.Equals(status))
                .SingleOrDefault() != null;
        }
    }
}
