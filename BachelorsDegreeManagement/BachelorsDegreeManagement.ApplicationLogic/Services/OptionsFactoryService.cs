﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.ViewModels;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Internal.Account.Manage;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class OptionsFactoryService : IOptionsFactoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerManager _logger;
        public readonly IOptionsService _optionsService;
        public readonly ICustomOptionsService _customOptionsService;
        public readonly IStudentsService _studentsService;
        public readonly IProfessorsService _professorsService;
        public readonly IUsersService _usersService;
        public readonly IDeadlinesService _deadlinesService;
        public readonly ISystemVariablesService _variablesService;
        private readonly IEmailService _emailService;


        public OptionsFactoryService(IOptionsService optionsService, ICustomOptionsService customOptionsService,
            IStudentsService studentsService, IProfessorsService professorsService, IUsersService usersService,
            IDeadlinesService deadlinesService,
            ISystemVariablesService systemVariablesService, IEmailService emailService, IUnitOfWork unitOfWork, ILoggerManager loggerManager)
        {
            _optionsService = optionsService;
            _customOptionsService = customOptionsService;
            _studentsService = studentsService;
            _professorsService = professorsService;
            _usersService = usersService;
            _deadlinesService = deadlinesService;
            _variablesService = systemVariablesService;
            _emailService = emailService;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> RegisterOption(Users user, CreateOptionViewModel model)
        {
            var deadline = await _deadlinesService.GetCurrentActiveDeadline();
            var timePeriodForAddingProjects = _unitOfWork.SystemVariablesRepository.FindSystemVariableByCondition(v => v.Name.Equals("ProjectsInsertionNumberOfDays"));
            var today = DateTime.Now;
            if (deadline == null || DateTime.Compare(today, deadline.Date.AddDays(-1 * Int32.Parse(timePeriodForAddingProjects.Value))) <= 0)
            {
                return false;
            }
            else
            {
                bool isAvailable = _unitOfWork.StudentsRepository.CheckIfAvailable(user);
                int numberOfOptions = await GetOptionNumber(user) + 1;
                int maxNumberOfOptions = Int32.Parse(_variablesService.GetVariableByName("NumberOfOptions").Value);
                if (numberOfOptions <= maxNumberOfOptions && isAvailable)
                {
                    var professorUser = await _usersService.GetUser(model.ProfessorId);
                    var professorId = (await _professorsService.GetProfessorByUser(professorUser)).GDPRCode;
                    switch (model.Type)
                    {
                        case "Custom":
                            var studentId = (await _studentsService.GetStudentByUser(user)).GDPRCode;

                            CustomOptions customOption = new CustomOptions
                            {
                                Status = "waiting",
                                ProfessorId = professorId,
                                StudentId = studentId,
                                Description = model.Description,
                                NumberForUser = numberOfOptions,
                                DeadlineId = deadline.Id,
                                Discriminator = "custom"
                            };
                            _customOptionsService.AddCustomOption(customOption);
                            break;
                        case "Proposed":
                            var project = await _unitOfWork.ProjectsRepository.FindProjectByCondition(p => p.Name.Equals(model.Project) && p.ProfessorId == professorId);
                            if(project != null)
                            {
                                Options option = new Options
                                {
                                    Status = "waiting",
                                    ProjectId = project.Id,
                                    StudentId = (await _studentsService.GetStudentByUser(user)).GDPRCode,
                                    Description = model.Description,
                                    NumberForUser = numberOfOptions,
                                    DeadlineId = deadline.Id,
                                    Discriminator = "proposed"
                                };
                                _optionsService.AddOption(option);
                            }
                            else
                            {
                                return false;
                            }
                            
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        public async Task<int> GetOptionNumber(Users user)
        {
            List<Options> options = await _optionsService.GetWaitingOptions(user);
            List<CustomOptions> customOptions = await _customOptionsService.GetWaitingOptions(user);
            return options.Count + customOptions.Count;
        }

        public async Task<List<CustomOptions>> FindSentCustomOptions(Users user)
        {
            return await _customOptionsService.GetSentOptions(user);
        }

        public async Task<List<CustomOptions>> FindReceivedCustomOptions(Users user)
        {
            return await _customOptionsService.GetReceivedOptions(user);
        }

        public async Task<List<Options>> FindSentOptions(Users user)
        {
            return await _optionsService.GetSentOptions(user);
        }

        public async Task<List<Options>> FindReceivedOptions(Users user)
        {
            return await _optionsService.GetReceivedOptions(user);
        }

        public async Task<List<Options>> FindOptions()
        {
            return await _optionsService.GetOptions();
        }

        public async Task<List<CustomOptions>> FindCustomOptions()
        {
            return await _customOptionsService.GetCustomOptions();
        }

        public async Task<Options> FindOption(int id)
        {
            return await _optionsService.GetOption(id);
        }

        public async Task<CustomOptions> FindCustomOption(int id)
        {
            return await _customOptionsService.GetCustomOption(id);
        }

        public ArrayList GetOptionsForProfessor(Professors professor)
        {
            // get the last deadline
            var deadline = _unitOfWork.DeadlinesRepository.GetRecentDeadline();
            if (deadline != null)
            {
                // create an array list because its elements will be Options or CustomOption  s
                ArrayList result = new ArrayList();

                // get the options for the last deadline
                var options = _unitOfWork.OptionsRepository
                    .FindByCondition(o => o.Project.ProfessorId == professor.GDPRCode && o.DeadlineId == deadline.Id && o.Status == "waiting")
                    .Include(o => o.Student)
                    .ThenInclude(s => s.User)
                    .OrderBy(o => o.Student.Grade)
                    .AsNoTracking()
                    .ToList();

                // get the custom options for the last deadline
                var customOptions = _unitOfWork.CustomOptionsRepository
                    .FindByCondition(c => c.ProfessorId == professor.GDPRCode && c.DeadlineId == deadline.Id && c.Status == "waiting")
                    .Include(o => o.Student)
                    .ThenInclude(s => s.User)
                    .OrderBy(c => c.Student.Grade)
                    .AsNoTracking()
                    .ToList();

                // add the options and the custom options in the list
                if (options != null)
                {
                    result.AddRange(options);
                }
                if (customOptions != null)
                {
                    result.AddRange(customOptions);
                }


                if (result != null && result.Count > 0 && (result[0] as OptionsBase).Place == 0)
                {
                    // sort the list depending on the student's grade
                    for (int iterator = 0; iterator < result.Count - 1; iterator++)
                    {
                        for (int iterator2 = iterator + 1; iterator2 < result.Count; iterator2++)
                        {
                            if ((result[iterator] as OptionsBase).Student.Grade < (result[iterator2] as OptionsBase).Student.Grade)
                            {
                                var temporary = result[iterator];
                                result[iterator] = result[iterator2];
                                result[iterator2] = temporary;
                            }
                        }
                    }
                }
                else
                {
                    // sort based on the place
                    for (int iterator = 0; iterator < result.Count - 1; iterator++)
                    {
                        for (int iterator2 = iterator + 1; iterator2 <= result.Count - 1; iterator2++)
                        {
                            if ((result[iterator] as OptionsBase).Place > (result[iterator2] as OptionsBase).Place)
                            {
                                var temporary = result[iterator];
                                result[iterator] = result[iterator2];
                                result[iterator2] = temporary;
                            }
                        }
                    }
                }

                return result;
            }

            return null;
        }

        public ArrayList GetRemainingOptionsForProfessor(Professors professor)
        {
            // get the last deadline
            var deadline = _unitOfWork.DeadlinesRepository.GetRecentDeadline();
            if (deadline != null)
            {
                // create an array list because its elements will be Options or CustomOptions
                ArrayList result = new ArrayList();

                // get the options for the last deadline
                var options = _unitOfWork.OptionsRepository
                    .FindByCondition(o => o.Project.ProfessorId == professor.GDPRCode && o.DeadlineId == deadline.Id &&
                        o.Status.Equals("waiting"))
                    .Include(o => o.Student)
                    .ToList();

                // get the custom options for the last deadline
                var customOptions = _unitOfWork.CustomOptionsRepository
                    .FindByCondition(c => c.ProfessorId == professor.GDPRCode && c.DeadlineId == deadline.Id &&
                        c.Status.Equals("waiting"))
                    .Include(o => o.Student)
                    .ToList();

                // add the options and the custom options in the list
                if (options != null)
                {
                    result.AddRange(options);
                }
                if (customOptions != null)
                {
                    result.AddRange(customOptions);
                }


                return result;
            }

            return null;
        }

        public async Task SortOptions(string postString)
        {
            var result = JsonConvert.DeserializeObject<List<OptionViewModel>>(postString);
            foreach (var element in result)
            {
                var info = element.Id.Split('_');
                // search the option and assign the position
                if (info[1].Equals("custom"))
                {
                    var option = await _customOptionsService.GetCustomOption(Int32.Parse(info[0]));
                    option.Place = element.Position;
                    option.Deadline = null;
                    option.Professor = null;
                    option.Student = null;
                    _unitOfWork.CustomOptionsRepository.Update(option);

                }
                else
                {
                    var option = await _optionsService.GetOption(Int32.Parse(info[0]));
                    option.Place = element.Position;
                    option.Deadline = null;
                    option.Project = null;
                    option.Student = null;
                    _unitOfWork.OptionsRepository.Update(option);
                }
                _unitOfWork.Complete();
            }

        }

        public ArrayList OrderOptions(ArrayList options)
        {
            for (int iterator = 0; iterator < options.Count - 1; iterator++)
            {
                for (int iterator2 = iterator + 1; iterator2 < options.Count; iterator2++)
                {
                    if ((options[iterator] as OptionsBase).Place > (options[iterator2] as OptionsBase).Place)
                    {
                        var temp = options[iterator];
                        options[iterator] = options[iterator2];
                        options[iterator2] = temp;
                    }
                }
            }

            ArrayList result = new ArrayList();
            foreach (var option in options)
            {
                List<string> optionDetails = new List<string>();
                optionDetails.Add((option as OptionsBase).Id.ToString());
                optionDetails.Add((option as OptionsBase).NumberForUser.ToString());
                optionDetails.Add((option as OptionsBase).Place.ToString());
                optionDetails.Add((option as OptionsBase).Discriminator);
                result.Add(optionDetails);
            }
            return result;
        }

        public ArrayList GetOptionsForStudent(Students student)
        {
            // get the last deadline
            var deadline = _unitOfWork.DeadlinesRepository.GetRecentDeadline();
            if (deadline != null)
            {
                // create an array list because its elements will be Options or CustomOptions
                ArrayList result = new ArrayList();

                // get the options for the last deadline
                var options = _unitOfWork.OptionsRepository
                    .FindByCondition(o => o.StudentId == student.GDPRCode && o.DeadlineId == deadline.Id && o.Status == "waiting")
                    .ToList();

                // get the custom options for the last deadline
                var customOptions = _unitOfWork.CustomOptionsRepository
                    .FindByCondition(c => c.StudentId == student.GDPRCode && c.DeadlineId == deadline.Id && c.Status == "waiting").ToList();

                // add the options and the custom options in the list
                if (options != null)
                {
                    result.AddRange(options);
                }
                if (customOptions != null)
                {
                    result.AddRange(customOptions);
                }

                return result;
            }

            return null;
        }

        public bool Step1()
        {
            // get the most recent deadline (the deadline that is inactive, but is the most recent one)
            var deadline = _deadlinesService.GetRecentDeadline();
            if (deadline != null && deadline.IsAllocationMade == 0)
            {
                var professors = _professorsService.GetProfessorsSync();
                bool nextStep = false;
                foreach (var professor in professors)
                {
                    ArrayList options = OrderOptions(GetOptionsForProfessor(professor));
                    _unitOfWork.Detach();
                    int remainingSpots = professor.ProjectsNumber;
                    foreach (List<string> option in options)
                    {
                        if (MarkAsAccepted(option, professor, remainingSpots))
                        {
                            UpdateProfessor(professor.GDPRCode);
                        }
                    }
                    var remainingOptions = GetOptionsForProfessor(professor);
                    if (remainingOptions.Count > 0 && !nextStep)
                    {
                        nextStep = true;
                    }
                }

                return nextStep;
            }
            return false;
        }

        public bool MarkAsAccepted(List<string> option, Professors professor, int remainingSpots)
        {
            bool accepted = false;
            if (Int32.Parse(option.ElementAt(1)) == 1 && Int32.Parse(option.ElementAt(2)) <= remainingSpots)
            {
                switch (option.ElementAt(3))
                {
                    case "custom":
                        var foundCustomOption = _unitOfWork.CustomOptionsRepository
                            .FindByCondition(co => co.Id == Int32.Parse(option.ElementAt(0)))
                            .Include(co => co.Student)
                            .SingleOrDefault();
                        foundCustomOption.Status = "accepted";
                        _customOptionsService.UpdateCustomOption(foundCustomOption);
                        accepted = true;
                        var student1 = foundCustomOption.Student;
                        _unitOfWork.Detach();
                        var otherOptions1 = GetOptionsForStudent(student1);
                        MarkRedundantOptions(otherOptions1, "inactive", student1);
                        NotifyUser(foundCustomOption as OptionsBase, "accepted");
                        break;
                    case "proposed":
                        var foundOption = _unitOfWork.OptionsRepository
                            .FindByCondition(o => o.Id == Int32.Parse(option.ElementAt(0)))
                            .Include(o => o.Student)
                            .SingleOrDefault();
                        foundOption.Status = "accepted";
                        _optionsService.UpdateOption(foundOption);
                        accepted = true;
                        _unitOfWork.Detach();
                        var student2 = foundOption.Student;
                        var otherOptions2 = GetOptionsForStudent(student2);
                        MarkRedundantOptions(otherOptions2, "inactive", student2);
                        NotifyUser(foundOption as OptionsBase, "accepted");
                        break;
                    default:
                        break;
                }
            }
            return accepted;
        }

        public bool Step2()
        {
            var students = _studentsService.GetOrderedStudents();
            foreach (var student in students)
            {
                List<Dictionary<Dictionary<string, int>, int>> priorityList = CreatePriorityList(student);
                if (priorityList.Count > 0)
                {
                    var professorId = AllocateProjectBasedOnPriority(priorityList, student);
                    UpdateRemainingOptions(student);
                    UpdateProfessor(professorId);
                }
            }
            FinishAllocation();

            bool isAllocationDone = CheckIfAllocationProcessIsDone(students);

            return isAllocationDone;
        }

        private bool CheckIfAllocationProcessIsDone(List<Students> students)
        {
            foreach (var student in students)
            {
                var customOption = _unitOfWork.CustomOptionsRepository.FindByCondition(o => o.StudentId.Equals(student.GDPRCode) && o.Status.Equals("accepted")).SingleOrDefault();
                var option = _unitOfWork.OptionsRepository.FindByCondition(o => o.StudentId.Equals(student.GDPRCode) && o.Status.Equals("accepted")).SingleOrDefault();
                if (customOption == null &&
                    option == null)
                {
                    return false;
                }
            }

            return true;
        }

        public List<Dictionary<Dictionary<string, int>, int>> CreatePriorityList(Students student)
        {
            List<Dictionary<Dictionary<string, int>, int>> priorityList = new List<Dictionary<Dictionary<string, int>, int>>();
            
            foreach (var option in student.Options)
            {
                if (_optionsService.CheckStatus(option.Id, "waiting"))
                {
                    Dictionary<Dictionary<string, int>, int> score = new Dictionary<Dictionary<string, int>, int>();
                    Dictionary<string, int> key = new Dictionary<string, int>();
                    key[option.Discriminator] = option.NumberForUser;
                    score[key] = ComputePriorityScore(option);
                    priorityList.Add(score);
                }
            }
            foreach (var customOption in student.CustomOptions)
            {
                if (_customOptionsService.CheckStatus(customOption.Id, "waiting"))
                {
                    Dictionary<Dictionary<string, int>, int> score = new Dictionary<Dictionary<string, int>, int>();
                    Dictionary<string, int> key = new Dictionary<string, int>();
                    key[customOption.Discriminator] = customOption.NumberForUser;
                    score[key] = ComputePriorityScore(null, customOption);
                    priorityList.Add(score);
                }
            }
            // sort the priority scores
            priorityList.OrderBy(l => l.Values);
            return priorityList;
        }

        public string AllocateProjectBasedOnPriority(List<Dictionary<Dictionary<string, int>, int>> priorityList, Students student)
        {
            String professorId = "";
            // validate the option with the minimum score:

            var firstOptionInfo = priorityList.First().Keys.First();

            switch (firstOptionInfo.Keys.First())
            {
                case "proposed":
                    // it is a proposed project
                    int number = firstOptionInfo.Values.First();
                    var foundOption = _optionsService.FindOptionByNumberSync(student, number);
                    if (ValidateOption(foundOption))
                    {
                        foundOption.Status = "accepted";
                        professorId = foundOption.Project.ProfessorId;
                        _unitOfWork.OptionsRepository.Update(foundOption);
                        _unitOfWork.Complete();
                        NotifyUser(foundOption as OptionsBase, "accepted");
                    }

                    break;
                case "custom":
                    // it is a custom project
                    var foundCustomOption = _customOptionsService.FindOptionByNumberSync(student, firstOptionInfo.Values.First());
                    if (ValidateOption(null, foundCustomOption))
                    {
                        foundCustomOption.Status = "accepted";
                        professorId = foundCustomOption.ProfessorId;
                        _unitOfWork.CustomOptionsRepository.Update(foundCustomOption);
                        _unitOfWork.Complete();
                        NotifyUser(foundCustomOption as OptionsBase, "accepted");
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }

            return professorId;
        }

        public void UpdateRemainingOptions(Students student)
        {
            ArrayList options = new ArrayList();
            var waitingOptions = _optionsService.GetWaitingOptionsSync(student.User);
            var waitingCustomOptions = _customOptionsService.GetWaitingOptionsSync(student.User);
            if(waitingOptions != null)
            {
                options.AddRange(waitingOptions);
            } 
            if (waitingCustomOptions != null)
            {
                options.AddRange(waitingCustomOptions);
            }
            
            MarkRedundantOptions(options, "inactive", student);
        }

        public void FinishAllocation()
        {
            // mark the deadline
            var deadline = _deadlinesService.GetRecentDeadline();
            deadline.IsAllocationMade = 1;
            _deadlinesService.UpdateDeadlineSync(deadline, true);
        }

        public int ComputePriorityScore(Options option = null, CustomOptions customOption = null)
        {
            if (option != null)
            {
                var professor = option.Project.Professor;
                return option.Place - professor.ProjectsNumber + option.NumberForUser;

            }
            else if (customOption != null)
            {
                var professor = customOption.Professor;
                return customOption.Place - professor.ProjectsNumber + customOption.NumberForUser;
            }

            return 1000;
        }

        public void MarkRedundantOptions(ArrayList options, string status, Students student = null)
        {
            foreach (var option in options)
            {
                if ((option as OptionsBase).Status.Equals("waiting") && (
                    (student != null && (option as OptionsBase).StudentId.Equals(student.GDPRCode)) ||
                    student == null))
                {
                    (option as OptionsBase).Status = status;
                    switch ((option as OptionsBase).Discriminator)
                    {
                        case "custom":
                            _customOptionsService.UpdateCustomOption(option as CustomOptions);
                            break;
                        case "proposed":
                            _optionsService.UpdateOption(option as Options);
                            break;
                        default:
                            break;
                    }
                    if (status.Equals("rejected"))
                    {
                        NotifyUser(option as OptionsBase, status);
                    }
                }
            }
        }

        public void UpdateProfessor(string professorId)
        {
            var professor = _unitOfWork.ProfessorsRepository.FindByCondition(p => p.GDPRCode.Equals(professorId)).SingleOrDefault();
            if (professor != null)
            {
                professor.ProjectsNumber--;
                _professorsService.UpdateProfessor(professor);
                if (professor.ProjectsNumber == 0)
                {
                    // the professor does not have available spots
                    var suplimentarOptions = GetRemainingOptionsForProfessor(professor);
                    MarkRedundantOptions(suplimentarOptions, "rejected");
                }
            }
        }

        public bool ValidateOption(Options option = null, CustomOptions customOption = null)
        {
            if (option != null && option.Status.Equals("waiting"))
            {
                return option.Project.Professor.ProjectsNumber > 0;
            }
            else if (customOption != null && customOption.Status.Equals("waiting"))
            {
                return customOption.Professor.ProjectsNumber > 0;
            }
            else
            {
                return false;
            }
        }

        public void NotifyUser(OptionsBase option, string status)
        {
            var template = _unitOfWork.EmailTemplatesRepository.FindByCondition(t => t.Usage.Equals(status)).SingleOrDefault();
            var body = template.Body;
            var emailBase = (_unitOfWork.EmailTemplatesRepository.FindEmailTemplateByCondition(e => e.Usage.Equals("email_base"))).Body;

            body = emailBase.Replace("{email_body}", body);
            var professorName = "";
            if (option.Discriminator.Equals("proposed"))
            {
                // this is a proposed option
                var optionDetails = _unitOfWork.OptionsRepository
                    .FindByCondition(o => o.Id == option.Id)
                    .Include(o => o.Project)
                    .ThenInclude(p => p.Professor).SingleOrDefault();
                professorName = optionDetails.Project.Professor.FullName;
                body = body.Replace("{project}", optionDetails.Project.Name);
            }
            else
            {
                // this is for the custom options
                var optionDetails = _unitOfWork.CustomOptionsRepository
                    .FindByCondition(o => o.Id == option.Id)
                    .Include(o => o.Professor).SingleOrDefault();
                professorName = optionDetails.Professor.FullName;
                body = body.Replace("{project}", "-");
            }
            body = body.Replace("{number}", option.NumberForUser.ToString())
                .Replace("{option_number}", option.NumberForUser.ToString())
                .Replace("{option_type}", option.Discriminator)
                .Replace("{description}", option.Description)
                .Replace("{professor}", professorName);


            var subject = template.Subject;

            var receiver = _unitOfWork.StudentsRepository.GetEmail((option as OptionsBase).StudentId);


            var email = _emailService.Create(receiver, subject, body);
            _emailService.Send(email);
        }

        public async Task<bool> UpdateRegularOption(Options option)
        {
            bool updated = false;
            var foundOption = await _unitOfWork.OptionsRepository.FindOptionByCondition(o => o.Id == option.Id);
            // check if the deadline has passed
            var deadline = await _unitOfWork.DeadlinesRepository.FindDeadlineByCondition(d => d.Id == foundOption.DeadlineId);
            if ((deadline.Status.Equals("active") || DateTime.Now < deadline.Date) && foundOption != null)
            {
                if (foundOption.ProjectId != option.ProjectId && option.ProjectId != null)
                {
                    foundOption.ProjectId = option.ProjectId;
                    foundOption.Project = _unitOfWork.ProjectsRepository.FindByCondition(p => p.Id == option.ProjectId).SingleOrDefault();
                }
                if (foundOption.Description != option.Description && option.Description != null)
                {
                    foundOption.Description = option.Description;
                }
                try
                {
                    _unitOfWork.OptionsRepository.Update(foundOption);
                    _unitOfWork.Complete();
                }catch(Exception e)
                {
                    _logger.LogError(e.Message);   
                }
                
                updated = true;
            }
            return updated;
        }

        public async Task<bool> UpdateCustomOption(CustomOptions option)
        {
            bool updated = false;
            var foundOption = await _unitOfWork.CustomOptionsRepository.FindCustomOptionByCondition(o => o.Id == option.Id);
            // check if the deadline has passed
            var deadline = await _unitOfWork.DeadlinesRepository.FindDeadlineByCondition(d => d.Id == foundOption.DeadlineId);
            if ((deadline.Status.Equals("active") || DateTime.Now < deadline.Date) && foundOption != null)
            {
                if (foundOption.Description != option.Description && option.Description != null)
                {
                    foundOption.Description = option.Description;
                }
                if (!foundOption.ProfessorId.Equals(option.ProfessorId) && option.ProfessorId != null)
                {
                    var professor = _unitOfWork.ProfessorsRepository.FindByCondition(p => p.GDPRCode.Equals(option.ProfessorId)).SingleOrDefault();
                    foundOption.ProfessorId = option.ProfessorId;
                    foundOption.Professor = professor;
                }
                _unitOfWork.CustomOptionsRepository.Update(foundOption);
                _unitOfWork.Complete();
                updated = true;
            }
            return updated;
        }

        public async Task DeleteRegularOption(Options option)
        {
            _unitOfWork.OptionsRepository.Delete(option);
            await UpdateOtherOptions(option.NumberForUser, option.Student.UserId);
            await _unitOfWork.CompleteAsync();
        }

        public async Task DeleteCustomOption(CustomOptions option)
        {
            _unitOfWork.CustomOptionsRepository.Delete(option);
            await UpdateOtherOptions(option.NumberForUser, option.Student.UserId);
            await _unitOfWork.CompleteAsync();
        }

        public async Task UpdateOtherOptions(int optionNumber, string userId)
        {
            // firstly, update the regular options:
            var options = await _unitOfWork.OptionsRepository
                .FindByCondition(o => o.NumberForUser > optionNumber && o.Student.UserId.Equals(userId) && o.Status.Equals("waiting"))
                .ToListAsync();

            foreach (var option in options)
            {
                option.NumberForUser--;
                _unitOfWork.OptionsRepository.Update(option);
                await _unitOfWork.CompleteAsync();
            }

            // secondly, update the custom options:
            var customOptions = await _unitOfWork.CustomOptionsRepository
                .FindByCondition(o => o.NumberForUser > optionNumber && o.Student.UserId.Equals(userId) && o.Status.Equals("waiting"))
                .ToListAsync();

            foreach (var option in customOptions)
            {
                option.NumberForUser--;
                _unitOfWork.CustomOptionsRepository.Update(option);
                await _unitOfWork.CompleteAsync();
            }
        }

        public async Task<AcceptedProposalViewModel> getAcceptedProposal(string userId)
        {
            AcceptedProposalViewModel acceptedProposal = null;
            var option = await _unitOfWork.OptionsRepository
                .FindByCondition(o => o.Student.UserId.Equals(userId) && o.Status.Equals("accepted"))
                .Include(o => o.Project)
                .ThenInclude(p => p.Professor)
                .SingleOrDefaultAsync();

            if(option != null)
            {
                acceptedProposal = new AcceptedProposalViewModel
                {
                    Description = option.Project.Description,
                    Title = option.Project.Name,
                    SupervisorCode = option.Project.Professor.GDPRCode
                };
            }
            else
            {
                var customOption = await _unitOfWork.CustomOptionsRepository
                .FindByCondition(co => co.Student.UserId.Equals(userId) && co.Status.Equals("accepted"))
                .Include(co => co.Professor)
                .SingleOrDefaultAsync();
                if(customOption != null)
                {
                    acceptedProposal = new AcceptedProposalViewModel
                    {
                        Description = customOption.Description,
                        SupervisorCode = customOption.Professor.GDPRCode
                    };
                }
            }

            return acceptedProposal;
        }
    }
}
