﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Services
{
    public class GroupsService: IGroupsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public GroupsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Groups>> GetGroups()
        {
            return await _unitOfWork.GroupsRepository.FindAllGroups();
        }

        public async Task<Groups> GetGroup(int id)
        {
            return await _unitOfWork.GroupsRepository.FindGroupByCondition(group => group.Id == id);
        }
    }
}
