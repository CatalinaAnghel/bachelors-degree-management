﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.HostedServices.Workers.Abstractions
{
    public interface IDeadlinesWorker
    {
        public Task Work(CancellationToken cancellationToken);
    }
}
