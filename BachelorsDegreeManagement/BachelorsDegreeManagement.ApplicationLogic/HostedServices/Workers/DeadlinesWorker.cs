﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.HostedServices.Workers.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.HostedServices.Workers
{
    public class DeadlinesWorker : IDeadlinesWorker
    {
        private readonly ILoggerManager _logger;

        public DeadlinesWorker(ILoggerManager logger, IServiceProvider services)
        {
            this._logger = logger;
            Services = services;
        }

        public IServiceProvider Services { get; }

        public async Task Work(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.LogInfo("The worker is validating the deadlines");
                using (var scope = Services.CreateScope())
                {
                    var _deadlinesService =
                        scope.ServiceProvider
                            .GetRequiredService<IDeadlinesService>();
                    var numberOfUpdatedDeadlines = await _deadlinesService.ValidateDeadlines();
                    if(numberOfUpdatedDeadlines > 0)
                    {
                        _logger.LogInfo($"The worker validated {numberOfUpdatedDeadlines} deadline(s)");
                    }
                    else
                    {
                        _logger.LogInfo("All the deadlines are valid");

                    }
                    var _variablesService = scope.ServiceProvider.GetRequiredService<ISystemVariablesService>();
                    _logger.LogWarn("The worker will try to validate the deadlines after 24 hours");
                    await Task.Delay(Int32.Parse(_variablesService.GetVariableByName("ValidationNumberOfHours").Value) * 3600 * 1000);
                }
            }
        }
    }
}
