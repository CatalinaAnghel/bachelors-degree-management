﻿using BachelorsDegreeManagement.ApplicationLogic.HostedServices.Workers.Abstractions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.HostedServices
{
    public class DeadlineValidator : BackgroundService
    {
        private readonly IDeadlinesWorker _worker;

        public DeadlineValidator(IDeadlinesWorker worker)
        {
            this._worker = worker;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _worker.Work(stoppingToken);
        }
    }
}
