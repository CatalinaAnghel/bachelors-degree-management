﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class GetStudentsSuggestionsViewModel
    {
        [Display(Name = "Student")]
        public Students Student { get; set; }
        [Display(Name = "Personality compatibility(%)")]
        public double PersonalityScore { get; set; }
        [Display(Name = "Domain compatibility(%)")]
        public double TagsScore { get; set; }
        [Display(Name = "Supervisor style compatibility(%)")]
        public double SupervisorStyleScore { get; set; }
        [Display(Name = "Overall compatibility(%)")]
        public double CompatibilityScore { get; set; }
    }
}
