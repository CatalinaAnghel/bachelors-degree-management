﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ProfessorsIndexViewModel
    {
        public List<Professors> Professors { get; set; }
        public ProfessorsImportViewModel ImportViewModel { get; set; }
    }
}
