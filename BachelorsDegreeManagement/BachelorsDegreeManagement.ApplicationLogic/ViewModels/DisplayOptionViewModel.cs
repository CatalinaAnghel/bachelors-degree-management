﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class DisplayOptionViewModel
    {
        public Students Student { get; set; }
        public string Description { get; set; }
        public string Discriminator { get; set; }
        public Projects Project { get; set; }
    }
}
