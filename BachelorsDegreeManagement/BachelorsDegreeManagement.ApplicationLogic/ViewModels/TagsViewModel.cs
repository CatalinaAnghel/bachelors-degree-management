﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class TagsViewModel
    {
        public int Id { set; get; }
        [DisplayName("Tag")]
        public string Name { get; set; }
        [DisplayName("Professors")]
        public int NoProfessors { get; set;}
        [DisplayName("Students")]
        public int NoStudents { get; set; }
        [DisplayName("Projects")]
        public int NoProjects { get; set; }
    }
}
