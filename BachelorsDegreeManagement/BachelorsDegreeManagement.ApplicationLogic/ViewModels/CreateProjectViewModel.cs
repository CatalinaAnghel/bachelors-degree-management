﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class CreateProjectViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        [Required]
        public string Complexity { get; set; }
        [Required]
        public int DeadlineId { get; set; }
        [Required]
        [StringLength(256, MinimumLength = 50)]
        public string Description { get; set; }
        [Required(ErrorMessage ="You need to add at least one domain keyword")]
        public string Tags { get; set; }
    }
}
