﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ProjectViewModel
    {
        public Projects Project { get; set; }
        [DisplayName("Domain compatibility(%)")]
        public double DomainCompatibility { get; set; }
        [DisplayName("Personality compatibility(%)")]
        public double PersonalityCompatibility { get; set; }
        [DisplayName("Complexity compatibility(%)")]
        public double ComplexityCompatibility { get; set; }
        [DisplayName("Supervisor style compatibility(%)")]
        public double SupervisorStyleCompatibility { get; set; }
        [DisplayName("Overall compatibility(%)")]
        public double OverallCompatibility { get; set; }
    }
}
