﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ExportSupervisorViewModel
    {
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Department { get; set; }
        public string SupervisoryPlan { get; set; }
        public string Email { get; set; }
    }
}
