﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class DepartmentsViewModel
    {
        public int Id { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Professors")]
        public int NoProfessors { get; set; }
        [DisplayName("Students")]
        public int NoStudents { get; set; }
        [DisplayName("Groups")]
        public int NoGroups { get; set; }
    }
}
