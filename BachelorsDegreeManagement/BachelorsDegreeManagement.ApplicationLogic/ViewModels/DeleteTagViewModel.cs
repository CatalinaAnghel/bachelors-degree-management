﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class DeleteTagViewModel
    {
        [Required]
        public int TagId { get; set; }
        public bool Selected { get; set; }
    }
}
