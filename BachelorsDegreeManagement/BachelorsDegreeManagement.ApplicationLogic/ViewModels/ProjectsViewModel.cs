﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ProjectsViewModel
    {
        public Projects Project { get; set; }
        [DisplayName("Overall Compatibility (%)")]
        public double CompatibilityScore { get; set; }
        public string Class { get; set; }
    }
}
