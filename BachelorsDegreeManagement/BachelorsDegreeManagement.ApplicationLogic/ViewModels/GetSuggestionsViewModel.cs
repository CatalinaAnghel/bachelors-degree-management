﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class GetSuggestionsViewModel
    {
        [Display(Name = "Professor")]
        public Professors Professor { get; set; }
        [Display(Name = "Personality compatibility(%)")]
        public double PersonalityScore { get; set; }
        [Display(Name = "Domain compatibility(%)")]
        public double TagsScore { get; set; }
        [Display(Name = "Supervisor Style compatibility(%)")]
        public double SupervisorStyleScore { get; set; }
        [Display(Name = "Overall Compatibility(%)")]
        public double CompatibilityScore { get; set; }
    }
}
