﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class CreateOptionViewModel
    {
        [Required]
        public string Type { get; set; }
        [Required]
        [DisplayName("Professor")]
        public string ProfessorId{ get; set; }
        public string Project { get; set; }
        [StringLength(200, ErrorMessage = "The description must have beween 20 and 200 characters.", MinimumLength = 20)]
        public string Description { get; set; }
        
    }
}
