﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class OptionViewModel
    {
        public string Id { get; set; }
        public int Position { get; set; }
    }
}
