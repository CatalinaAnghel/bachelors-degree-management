﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class CreateDepartmentViewModel
    {
        [Required]
        [DisplayName("Department's name")]
        public string Name { get; set; }
        [Required]
        public string Specializations { get; set; }
    }
}
