﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ExportStudentsViewModel
    {
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Specialization { get; set; }
        public string PhoneNumber { get; set; }
        public string ProjectTitle { get; set; }
        public string ProjectDescription { get; set; }
        public string SupervisorCode { get; set; }
        public string SupervisoryPlan { get; set; }
    }
}
