﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class QuizViewModel
    {
        public List<Questions> Questions { get; set; }
        [Required]
        [MinLength(50, ErrorMessage ="All the questions are mandatory")]
        public List<int> Answers { get; set; }
    }
}
