﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class StudentsImportViewModel
    {
        [Display(Name = "Upload the Excel file with the students information")]
        [Required]
        [FileExtensions(Extensions ="xls,xlsx", ErrorMessage ="You need to use an Excel file")]
        public IFormFile Document { get; set; }
    }
}
