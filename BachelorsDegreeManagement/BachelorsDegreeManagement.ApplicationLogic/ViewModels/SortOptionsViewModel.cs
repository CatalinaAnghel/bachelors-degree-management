﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class SortOptionsViewModel
    {
        public ArrayList Options { get; set; }
        public int MaxNumberOfProjects { get; set; }
    }
}
