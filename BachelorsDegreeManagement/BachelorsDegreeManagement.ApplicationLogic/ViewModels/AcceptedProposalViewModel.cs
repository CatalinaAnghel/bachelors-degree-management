﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class AcceptedProposalViewModel
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public string SupervisorCode { get; set; }
    }
}
