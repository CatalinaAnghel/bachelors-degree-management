﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class StudentsIndexViewModel
    {
        public List<Students> Students { get; set; }
        public StudentsImportViewModel ImportViewModel { get; set; }
    }
}
