﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ProfessorsImportViewModel
    {
        [Display(Name = "Upload the Excel file with the professors information")]
        [Required]
        public IFormFile Document { get; set; }
    }
}
