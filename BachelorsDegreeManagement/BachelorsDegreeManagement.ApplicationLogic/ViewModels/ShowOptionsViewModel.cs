﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class ShowOptionsViewModel
    {
        public List<CustomOptions> CustomOptions { get; set; }
        public List<Options> Options { get; set; }
    }
}
