﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.ViewModels
{
    public class UpdateWeightsViewModel
    {
        #region Student / Professor Compatibility Weights
        [Required]
        [DisplayName("Personality weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float PersonalityPercentage { get; set; }
        [Required]
        [DisplayName("Domain weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float TagsPercentage { get; set; }
        [Required]
        [DisplayName("Supervisor style weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float SupervisorStylePercentage { get; set; }
        [DisplayName("Student / professor total")]
        public float TotalPercentage { get; set; }
        #endregion

        #region Student / Project Compatibility Weights
        [Required]
        [DisplayName("Personality weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float ProjectPersonalityPercentage { get; set; }
        [Required]
        [DisplayName("Domain weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float ProjectTagsPercentage { get; set; }
        [Required]
        [DisplayName("Complexity weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float ProjectComplexityPercentage { get; set; }
        [Required]
        [DisplayName("Supervisor style weight")]
        [Range(0, 1, ErrorMessage = "The weight must be between {1} and {2}")]
        public float ProjectSupervisorStylePercentage { get; set; }
        [DisplayName("Student / project total")]
        public float ProjectTotalPercentage { get; set; }
        #endregion
    }
}
