﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Professors
    {
        [Key]
        [Required]
        [StringLength(13, ErrorMessage = "The GDPR code must have 13 characters")]
        public string GDPRCode { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 3)]
        [DisplayName("First name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 3)]
        [DisplayName("Last name")]
        public string LastName { get; set; }
        [ForeignKey("Users")]
        [DisplayName("User id")]
        public string UserId { get; set; }
        [ForeignKey("Departments")]
        [DisplayName("Department")]
        public int DepartmentId { get; set; }
        [Required]
        [Range(1, 50, ErrorMessage = "The number of projects must be between {1} and {2}")]
        [DisplayName("Number of projects")]
        public int ProjectsNumber { get; set; }
        [DisplayName("Preferred supervisor style")]
        public int? SupervisorStyle { get; set; }
        [NotMapped]
        [DisplayName("Full name")]
        public string FullName {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        // navigation properties
        public Users User { get; set; }
        public Departments Department { get; set; }
        public ICollection<Projects> Projects { get; set; }
        public ICollection<CustomOptions> CustomOptions { get; set; }

    }
}
