﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class OptionsBase
    {
        [Key]
        public int Id { get; set; }
        public int Place { get; set; }
        [ForeignKey("Students")]
        [DisplayName("Student")]
        public string StudentId { get; set; }
        [DisplayName("Option's number")]
        public int NumberForUser { get; set; }
        [StringLength(300, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 50)]
        public string Description { get; set; }
        public string Status { get; set; }
        [ForeignKey("Deadlines")]
        [DisplayName("Deadline")]
        public int DeadlineId { get; set; }
        [DisplayName("Type")]
        public string Discriminator { get; set; }

        // navigation properties
        public Students Student { get; set; }
        public Deadlines Deadline { get; set; }
    }
}
