﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Categories
    {
        [Key]
        [Range(1, 5)]
        public int Id { get; set; }
        [Required]
        [DisplayName("Category")]
        public string Name { get; set; }

        // navigation properties
        public ICollection<Questions> Questions { get; set; }
    }
}
