﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class CustomOptions: OptionsBase
    {
        [ForeignKey("Professors")]
        [DisplayName("Professor")]
        public string ProfessorId { get; set; }
     

        // navigation properties
        public Professors Professor { get; set; }
    }
}
