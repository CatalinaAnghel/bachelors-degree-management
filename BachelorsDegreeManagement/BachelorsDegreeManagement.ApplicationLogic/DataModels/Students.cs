﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Students
    {
        [Key]
        [Required]
        [StringLength(13, ErrorMessage = "The GDPR code must have 13 characters")]
        public string GDPRCode { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 3)]
        [DisplayName("First name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 3)]
        [DisplayName("Last name")]
        public string LastName { get; set; }
        [ForeignKey("Users")]
        [DisplayName("User id")]
        public string UserId { get; set; }
        [ForeignKey("Groups")]
        [DisplayName("Group")]
        public int GroupId { get; set; }
        [Required]
        [Range(1, 10, ErrorMessage ="The grade must be between {2} and {1}")]
        [DisplayName("Grade")]
        public float Grade { get; set; }
        [DisplayName("Preferred supervisor style")]
        public int? SupervisorStyle { get; set; }
        [DisplayName("Preferred complexity")]
        public string Complexity { get; set; }

        [NotMapped]
        [DisplayName("Full name")]
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        // navigation properties
        public Users User { get; set; }
        public Groups Group { get; set; }
        public ICollection<Options> Options { get; set; }
        public ICollection<CustomOptions> CustomOptions { get; set; }
    }
}
