﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Options: OptionsBase
    {      
        [ForeignKey("Projects")]
        [Display(Name = "Project")]
        public int? ProjectId { get; set; }
       

        // navigation properties
        public Projects Project { get; set; }
    }
}
