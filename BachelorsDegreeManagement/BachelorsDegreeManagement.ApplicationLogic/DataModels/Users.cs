﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Users: IdentityUser
    {
        // navigation properties
        public PersonalityTestResults Result { get; set; }

        public ICollection<UserTags> UserTags { get; set; }
    }
}
