﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class UserTags
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Users")]
        [DisplayName("User")]
        public string UserId { get; set; }
        [ForeignKey("Tags")]
        [DisplayName("Keyword")]
        public int TagId { get; set; }

        // navigation properties
        public Users User { get; set; }
        public Tags Tag { get; set; }
    }
}
