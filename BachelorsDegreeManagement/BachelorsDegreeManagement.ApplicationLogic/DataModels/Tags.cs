﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Tags
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50, ErrorMessage ="The tag's length must be between {2} and {1}", MinimumLength=1)]
        public string Name { get; set; }
        
        //navigation properties
        public ICollection<UserTags> UserTags { get; set; }
        public ICollection<ProjectTags> ProjectTags { get; set; }
    }
}
