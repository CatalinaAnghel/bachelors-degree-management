﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Questions
    {
        [Key]
        public int Id { get; set; }
        public string Keyed { get; set; }
        [StringLength(255, ErrorMessage = "{0} must be between {2} and {1}", MinimumLength = 5)]
        public string Content { get; set; }
        [ForeignKey("Categories")]
        [DisplayName("Category")]
        public int CategoryId { get; set; }

        // navigation properties
        public Categories Category { get; set; }
    }
}
