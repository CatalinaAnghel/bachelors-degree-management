﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Projects
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [DisplayName("Project title")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Complexity level")]
        public string Complexity { get; set; }
        [Required]
        [StringLength(256, MinimumLength = 50)]
        public string Description { get; set; }
        [ForeignKey("Professors")]
        [DisplayName("Professor")]
        public string ProfessorId { get; set; }
        [Required]
        [ForeignKey("Deadlines")]
        [DisplayName("Deadline")]
        public int DeadlineId { get; set; }
        [NotMapped]
        [DisplayName("Keywords")]
        public string Tags
        {
            get
            { 
                var tags = "";
                if (ProjectTags != null && ProjectTags.Count > 0)
                {
                    for (int iterator = 0; iterator < ProjectTags.Count; iterator++)
                    {
                        tags += ProjectTags.ElementAt(iterator).Tag.Name;
                        if (iterator < ProjectTags.Count - 1)
                        {
                            tags += ", ";
                        }

                    }
                }
                return tags;
            }
        }

        // navigation properties
        public Professors Professor { get; set; }
        public ICollection<ProjectTags> ProjectTags { get; set; }
        public ICollection<Options> Options { get; set; }
        [JsonIgnore]
        public Deadlines Deadline { get; set; }
    }
}
