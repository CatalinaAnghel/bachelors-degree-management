﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Deadlines
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Due day")]
        [DataType(DataType.Date)]
        public  DateTime Date { get; set; }
        [StringLength(16, MinimumLength = 5)]
        public string Status { get; set; }
        public int IsAllocationMade { get; set; }

        // Navigation properties:
        public ICollection<Options> Options { get; set; }
        public ICollection<Projects> Projects { get; set; }

        public ICollection<CustomOptions> CustomOptions { get; set; }

    }
}
