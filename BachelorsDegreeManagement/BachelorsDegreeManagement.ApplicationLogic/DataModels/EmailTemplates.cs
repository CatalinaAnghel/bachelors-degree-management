﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class EmailTemplates
    {
        [Key]
        public int Id { get; set; }
        public string Usage { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
