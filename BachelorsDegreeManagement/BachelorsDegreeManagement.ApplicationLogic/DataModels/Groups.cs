﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Groups
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Specializations")]
        public int SpecializationId { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 3)]
        public string Name { get; set; }
        [Range(1, 4)]
        public int StudyYear { get; set; }

        //navigation properties
        public Specializations Specialization { get; set; }
    }
}
