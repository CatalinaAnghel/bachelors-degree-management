﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class PersonalityTestResults
    {
        public int Id { get; set; }
        [Required]
        [ForeignKey("Users")]
        public string UserId { get; set; }
        [Range(1, 100, ErrorMessage = "The score must be in the interval (1, 100)")]
        public float SurgencyScore { get; set; }
        [Range(1, 100, ErrorMessage = "The score must be in the interval (1, 100)")]
        public float AgreeablenessScore { get; set; }
        [Range(1, 100, ErrorMessage = "The score must be in the interval (1, 100)")]
        public float EmotionalScore { get; set; }
        [Range(1, 100, ErrorMessage = "The score must be in the interval (1, 100)")]
        public float ConscientScore { get; set; }
        [Range(1, 100, ErrorMessage = "The score must be in the interval (1, 100)")]
        public float ImaginationScore { get; set; }

        // navigation properties
        public Users User { get; set; }
    }
}
