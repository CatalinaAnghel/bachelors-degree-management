﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Departments
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // nevigation properties
        public ICollection<Professors> Professors { get; set; }
        public ICollection<Specializations> Specializations { get; set; }
    }
}
