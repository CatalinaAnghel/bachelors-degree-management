﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class ProjectTags
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Projects")]
        [DisplayName("Project")]
        public int ProjectId { get; set; }
        [ForeignKey("Tags")]
        [DisplayName("Tag")]
        public int TagId { get; set; }

        // navigation properties
        public Projects Project { get; set; }
        public Tags Tag { get; set; }
    }
}
