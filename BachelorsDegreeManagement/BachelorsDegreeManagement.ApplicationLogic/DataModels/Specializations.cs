﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.DataModels
{
    public class Specializations
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }
        public string Acronym { get; set; }
        [Required]
        public int DepartmentId { get; set; }
        // navigation properties
        public Departments Department { get; set; }
        public ICollection<Groups> Groups { get; set; }
    }
}
