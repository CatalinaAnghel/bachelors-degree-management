﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface ITagsRepository: IRepositoryBase<Tags>
    {
        public List<Tags> GetUsersTags(Users user);
        public List<Tags> GetAvailableTags(Users user);
 
    }
}
