﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IDepartmentsRepository: IRepositoryBase<Departments>
    {
        public Task<List<Departments>> FindAllDepartments();
        public Task<Departments> FindDepartmentByCondition(Expression<Func<Departments, bool>> expression);
        public Task<List<Departments>> FindDepartmentsByCondition(Expression<Func<Departments, bool>> expression);
        public int GetNumberOfStudents(int departmentId);
        public int GetNumberOfProfessors(int departmentId);
        public int GetNumberOfGroups(int departmentId);
    }
}
