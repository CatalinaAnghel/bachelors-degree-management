﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IEmailTemplatesRepository : IRepositoryBase<EmailTemplates>
    {
        public List<EmailTemplates> FindAllEmailTemplates();
        public EmailTemplates FindEmailTemplateByCondition(Expression<Func<EmailTemplates, bool>> expression);
        public List<EmailTemplates> FindEmailTemplatesByCondition(Expression<Func<EmailTemplates, bool>> expression);
    }
}
