﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IProjectsRepository: IRepositoryBase<Projects>
    {
        public Task<List<Projects>> FindAllProjects();
        public List<int> FindTakenProjects();
        public Task<Projects> FindProjectByCondition(Expression<Func<Projects, bool>> expression);
        public Task<List<Projects>> FindProjectsByCondition(Expression<Func<Projects, bool>> expression);
    }
}
