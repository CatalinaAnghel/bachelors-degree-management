﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IQuestionsRepository: IRepositoryBase<Questions>
    {
        public Task<List<Questions>> FindAllQuestions();
        public Task<Questions> FindQuestionByCondition(Expression<Func<Questions, bool>> expression);
        public Task<List<Questions>> FindQuestionsByCondition(Expression<Func<Questions, bool>> expression);
    }
}
