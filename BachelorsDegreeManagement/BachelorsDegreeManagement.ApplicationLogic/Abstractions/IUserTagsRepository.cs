﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IUserTagsRepository: IRepositoryBase<UserTags>
    {
        public int GetNumberOfStudents(int tagId);
        public int GetNumberOfProfessors(int tagId);
    }
}