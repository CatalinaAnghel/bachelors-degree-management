﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IProjectTagsRepository: IRepositoryBase<ProjectTags>
    {
        public Task<List<ProjectTags>> FindAllProjectTags();
        public Task<ProjectTags> FindProjectTagByCondition(Expression<Func<ProjectTags, bool>> expression);
        public Task<List<ProjectTags>> FindProjectTagsByCondition(Expression<Func<ProjectTags, bool>> expression);
        public int GetNumberOfProjects(int tagId);
    }
}
