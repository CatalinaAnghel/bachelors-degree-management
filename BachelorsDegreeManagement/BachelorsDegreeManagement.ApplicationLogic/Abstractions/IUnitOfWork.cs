﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IUnitOfWork
    {
        ICustomOptionsRepository CustomOptionsRepository { get; }
        IDeadlinesRepository DeadlinesRepository { get; }
        IDepartmentsRepository DepartmentsRepository { get; }
        IEmailTemplatesRepository EmailTemplatesRepository { get; }
        IGroupsRepository GroupsRepository { get; }
        IOptionsRepository OptionsRepository { get; }
        IPersonalityTestResultsRepository PersonalityTestResultsRepository { get; }
        IProfessorsRepository ProfessorsRepository { get; }
        IProjectsRepository ProjectsRepository { get; }
        IProjectTagsRepository ProjectTagsRepository { get; }
        IQuestionsRepository QuestionsRepository { get; }
        IStudentsRepository StudentsRepository { get; }
        ISystemVariablesRepository SystemVariablesRepository { get; }
        ITagsRepository TagsRepository { get; }
        IUserTagsRepository UserTagsRepository { get; }
        ISpecializationsRepository SpecializationsRepository { get; }

        Task<int> CompleteAsync();
        int Complete();
        public void Dispose();
        public void Detach();
    }
}
