﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface ICustomOptionsRepository: IRepositoryBase<CustomOptions>
    {
        public Task<List<CustomOptions>> FindAllCustomOptions();
        public Task<CustomOptions> FindCustomOptionByCondition(Expression<Func<CustomOptions, bool>> expression);
        public Task<List<CustomOptions>> FindCustomOptionsByCondition(Expression<Func<CustomOptions, bool>> expression);
        public CustomOptions FindCustomOptionByConditionSync(Expression<Func<CustomOptions, bool>> expression);
        public List<CustomOptions> FindCustomOptionsByConditionSync(Expression<Func<CustomOptions, bool>> expression);
    }
}
