﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface ISpecializationsRepository:IRepositoryBase<Specializations>
    {
    }
}
