﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IGroupsRepository: IRepositoryBase<Groups>
    {
        public Task<List<Groups>> FindAllGroups();
        public Task<Groups> FindGroupByCondition(Expression<Func<Groups, bool>> expression);
        public Task<List<Groups>> FindGroupsByCondition(Expression<Func<Groups, bool>> expression);
    }
}
