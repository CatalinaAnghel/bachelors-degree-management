﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IStudentsRepository: IRepositoryBase<Students>
    {
        public List<Students> FindAvailableStudents();
        public Task<List<Students>> FindAllStudents();
        public Task<Students> FindStudentByCondition(Expression<Func<Students, bool>> expression);
        public Task<List<Students>> FindStudentsByCondition(Expression<Func<Students, bool>> expression);
        public string GetEmail(string studentId);
        public bool CheckIfAvailable(Users user);
        public bool CheckIfStudentExists(string GDPRCode);
        public Task<List<Students>> GetStudentsInfo();

    }
}
