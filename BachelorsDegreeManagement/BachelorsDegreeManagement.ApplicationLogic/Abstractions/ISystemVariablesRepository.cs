﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface ISystemVariablesRepository: IRepositoryBase<SystemVariables>
    {
        public SystemVariables FindSystemVariableByCondition(Expression<Func<SystemVariables, bool>> expression);
        public List<SystemVariables> FindSystemVariablesByCondition(Expression<Func<SystemVariables, bool>> expression);
    }
}
