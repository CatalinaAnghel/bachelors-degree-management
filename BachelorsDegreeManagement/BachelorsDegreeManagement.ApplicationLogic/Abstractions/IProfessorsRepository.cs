﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IProfessorsRepository: IRepositoryBase<Professors>
    {
        public Task<List<Professors>> FindAllProfessors();
        public Task<Professors> FindProfessorByCondition(Expression<Func<Professors, bool>> expression);
        public Task<List<Professors>> FindProfessorsByCondition(Expression<Func<Professors, bool>> expression);
        public bool CheckIfProfessorExists(string GDPRCode);
    }
}
