﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IDeadlinesRepository: IRepositoryBase<Deadlines>
    {
        public Deadlines GetRecentDeadline();
        public Task<List<Deadlines>> FindAllDeadlines();
        public Task<Deadlines> FindDeadlineByCondition(Expression<Func<Deadlines, bool>> expression);
        public Task<List<Deadlines>> FindDeadlinesByCondition(Expression<Func<Deadlines, bool>> expression);
        public Deadlines GetNextDeadline();
    }
}
