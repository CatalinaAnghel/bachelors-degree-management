﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.ApplicationLogic.Abstractions
{
    public interface IOptionsRepository: IRepositoryBase<Options>
    {
        public Task<List<Options>> FindAllOptions();
        public Task<Options> FindOptionByCondition(Expression<Func<Options, bool>> expression);
        public Options FindOptionByConditionSync(Expression<Func<Options, bool>> expression);
        public Task<List<Options>> FindOptionsByCondition(Expression<Func<Options, bool>> expression);
        public List<Options> FindOptionsByConditionSync(Expression<Func<Options, bool>> expression);
    }
}
