﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class CustomOptionsRepository : RepositoryBase<CustomOptions>, ICustomOptionsRepository
    {
        public CustomOptionsRepository(BachelorsDegreeManagementDbContext repositoryContext)
            : base(repositoryContext)
        { }

        public async Task<List<CustomOptions>> FindAllCustomOptions()
        {
            return await FindAll()
                .Include(o => o.Student)
                .Include(o => o.Professor)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<CustomOptions> FindCustomOptionByCondition(Expression<Func<CustomOptions, bool>> expression)
        {
            try
            {
                return await FindByCondition(expression)
                .Include(o => o.Student)
                .AsNoTracking()
                .Include(o => o.Professor)
                .AsNoTracking()
                .SingleOrDefaultAsync();
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            
        }

        public CustomOptions FindCustomOptionByConditionSync(Expression<Func<CustomOptions, bool>> expression)
        {
            try
            {
                return FindByCondition(expression)
                .Include(o => o.Student)
                .AsNoTracking()
                .Include(o => o.Professor)
                .AsNoTracking()
                .SingleOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }

        public async Task<List<CustomOptions>> FindCustomOptionsByCondition(Expression<Func<CustomOptions, bool>> expression)
        {
            return await FindByCondition(expression)
                .Include(o => o.Student)
                .Include(o => o.Professor)
                .AsNoTracking()
                .Distinct()
                .ToListAsync();
        }
        public List<CustomOptions> FindCustomOptionsByConditionSync(Expression<Func<CustomOptions, bool>> expression)
        {
            return FindByCondition(expression)
                .Include(o => o.Student)
                .Include(o => o.Professor)
                .AsNoTracking()
                .Distinct()
                .ToList();
        }
    }
}
