﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class ProfessorsRepository: RepositoryBase<Professors>, IProfessorsRepository
    {
        public ProfessorsRepository(BachelorsDegreeManagementDbContext repositoryContext)
           : base(repositoryContext)
        {
        }

        public async Task<List<Professors>> FindAllProfessors()
        {
            return await FindAll().ToListAsync();
        }

        public Task<Professors> FindProfessorByCondition(Expression<Func<Professors, bool>> expression)
        {
            return FindByCondition(expression)
                .SingleOrDefaultAsync();
        }

        public Task<List<Professors>> FindProfessorsByCondition(Expression<Func<Professors, bool>> expression)
        {
            return FindByCondition(expression)
                .ToListAsync();
        }

        public bool CheckIfProfessorExists(string GDPRCode)
        {
            return FindByCondition(p => p.GDPRCode.Equals(GDPRCode)).SingleOrDefault() != null;
        }
    }
}
