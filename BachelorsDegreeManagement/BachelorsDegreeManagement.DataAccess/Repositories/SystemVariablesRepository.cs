﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class SystemVariablesRepository : RepositoryBase<SystemVariables>, ISystemVariablesRepository
    {
        public SystemVariablesRepository(BachelorsDegreeManagementDbContext repositoryContext): base(repositoryContext) {}

        public SystemVariables FindSystemVariableByCondition(Expression<Func<SystemVariables, bool>> expression)
        {
            return FindByCondition(expression).SingleOrDefault();
        }

        public List<SystemVariables> FindSystemVariablesByCondition(Expression<Func<SystemVariables, bool>> expression)
        {
            return FindByCondition(expression).ToList();
        }

    }
}
