﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class DeadlinesRepository: RepositoryBase<Deadlines>, IDeadlinesRepository
    {
        public DeadlinesRepository(BachelorsDegreeManagementDbContext repositoryContext):
            base(repositoryContext)
        {
        }

        public async Task<List<Deadlines>> FindAllDeadlines()
        {
            return await FindAll().ToListAsync();
        }

        public async Task<Deadlines> FindDeadlineByCondition(Expression<Func<Deadlines, bool>> expression)
        {
            return await FindByCondition(expression).SingleOrDefaultAsync();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns>the list of deadlines which were selected based on the provided expression</returns>
        public async Task<List<Deadlines>> FindDeadlinesByCondition(Expression<Func<Deadlines, bool>> expression)
        {
            return await FindByCondition(expression).ToListAsync();
        }
        /// <summary>
        /// This method selects the most recent inactive deadline
        /// </summary>
        /// <returns>the most recent deadline</returns>
        public Deadlines GetRecentDeadline()
        {
            var result = (from d in RepositoryContext.Deadlines
                          where d.Status.Equals("inactive")
                          orderby d.Date descending
                          select d).FirstOrDefault();
            return result;               
        }

        /// <summary>
        /// This method selects the next deadline, which is the only active deadline
        /// </summary>
        /// <returns>the next deadline</returns>
        public Deadlines GetNextDeadline()
        {
            var result = (from d in RepositoryContext.Deadlines
                          where d.Status.Equals("active")
                          orderby d.Date ascending
                          select d).FirstOrDefault();
            return result;
        }
    }
}
