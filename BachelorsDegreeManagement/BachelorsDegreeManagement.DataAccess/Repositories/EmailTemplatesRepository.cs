﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class EmailTemplatesRepository: RepositoryBase<EmailTemplates>, IEmailTemplatesRepository
    {
        public EmailTemplatesRepository(BachelorsDegreeManagementDbContext repositoryContext):
            base(repositoryContext)
        {}

        public List<EmailTemplates> FindAllEmailTemplates()
        {
            return FindAll().AsNoTracking().ToList();
        }

        public EmailTemplates FindEmailTemplateByCondition(Expression<Func<EmailTemplates, bool>> expression)
        {
            return FindByCondition(expression).AsNoTracking().SingleOrDefault();
        }

        public List<EmailTemplates> FindEmailTemplatesByCondition(Expression<Func<EmailTemplates, bool>> expression)
        {
            return FindByCondition(expression).AsNoTracking().ToList();
        }
    }
}
