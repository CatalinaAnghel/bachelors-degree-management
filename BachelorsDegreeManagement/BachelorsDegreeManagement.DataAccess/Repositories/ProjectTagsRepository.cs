﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class ProjectTagsRepository: RepositoryBase<ProjectTags>, IProjectTagsRepository
    {
        public ProjectTagsRepository(BachelorsDegreeManagementDbContext repositoryContext):
            base(repositoryContext)
        {}

        public async Task<List<ProjectTags>> FindAllProjectTags()
        {
            return await FindAll().ToListAsync();
        }

        public async Task<ProjectTags> FindProjectTagByCondition(Expression<Func<ProjectTags, bool>> expression)
        {
            return await FindByCondition(expression).SingleOrDefaultAsync();
        }

        public async Task<List<ProjectTags>> FindProjectTagsByCondition(Expression<Func<ProjectTags, bool>> expression)
        {
            return await FindByCondition(expression).ToListAsync();
        }

        public int GetNumberOfProjects(int tagId)
        {
            return (from pt in RepositoryContext.ProjectTags
                    join p in RepositoryContext.Projects on pt.ProjectId equals p.Id
                    where pt.TagId == tagId
                    select p).Count();
        }
    }
}
