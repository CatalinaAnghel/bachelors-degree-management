﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class ProjectsRepository: RepositoryBase<Projects>, IProjectsRepository
    {
        public ProjectsRepository(BachelorsDegreeManagementDbContext repositoryContext)
            : base(repositoryContext)
        {

        }

        public async Task<List<Projects>> FindAllProjects()
        {
            return await FindAll().ToListAsync();
        }

        public List<int> FindTakenProjects()
        {
            var projects = (from p in RepositoryContext.Projects
                            join o in RepositoryContext.Options on p.Id equals o.ProjectId
                            where o.Status.Equals("accepted")
                            select p.Id).ToList();

            return projects;
        }

        public async Task<Projects> FindProjectByCondition(Expression<Func<Projects, bool>> expression)
        {
            return await FindByCondition(expression).SingleOrDefaultAsync();
        }

        public async Task<List<Projects>> FindProjectsByCondition(Expression<Func<Projects, bool>> expression)
        {
            return await FindByCondition(expression).ToListAsync();
        }
    }
}
