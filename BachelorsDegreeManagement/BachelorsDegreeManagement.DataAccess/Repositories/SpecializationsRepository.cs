﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class SpecializationsRepository:RepositoryBase<Specializations>, ISpecializationsRepository
    {
        public SpecializationsRepository(BachelorsDegreeManagementDbContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
