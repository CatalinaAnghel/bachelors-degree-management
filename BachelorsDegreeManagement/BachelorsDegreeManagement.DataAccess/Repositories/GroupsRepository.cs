﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class GroupsRepository: RepositoryBase<Groups>, IGroupsRepository
    {
        public GroupsRepository(BachelorsDegreeManagementDbContext repositoryContext)
           : base(repositoryContext)
        {
        }

        public async Task<List<Groups>> FindAllGroups()
        {
            return await FindAll().Include(group => group.Specialization).ToListAsync();
        }

        public async Task<Groups> FindGroupByCondition(Expression<Func<Groups, bool>> expression)
        {
            return await FindByCondition(expression)
                .Include(group => group.Specialization)
                .SingleOrDefaultAsync();
        }

        public async Task<List<Groups>> FindGroupsByCondition(Expression<Func<Groups, bool>> expression)
        {
            return await FindByCondition(expression)
                            .Include(group => group.Specialization)
                            .ToListAsync();
        }
    }
}
