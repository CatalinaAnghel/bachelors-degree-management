﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class StudentsRepository : RepositoryBase<Students>, IStudentsRepository
    {
        public StudentsRepository(BachelorsDegreeManagementDbContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<List<Students>> FindAllStudents()
        {
            return await FindAll().ToListAsync();
        }

        public List<Students> FindAvailableStudents()
        {
            var students = FindAll()
                .Include(s => s.User)
                .Include(s => s.Options)
                .ThenInclude(o => o.Project)
                .ThenInclude(p => p.Professor)
                .Include(s => s.CustomOptions)
                .ThenInclude(co => co.Professor)
                .ToList();
            students = students.Where(s => (s.CustomOptions == null && s.Options == null) ||
                            (s.CustomOptions != null && s.CustomOptions.Where(co => co.Status.Equals("waiting")) != null) ||
                            (s.Options != null && s.Options.Where(o => o.Status.Equals("waiting")) != null)).OrderByDescending(s => s.Grade).ToList();

            return students;
        }

        public async Task<List<Students>> GetStudentsInfo()
        {
            var students =  await FindAll()
                .Include(s => s.Group)
                .ThenInclude(g => g.Specialization)
                .Include(s => s.User)
                .Include(s => s.Options)
                .ThenInclude(o => o.Project)
                .ThenInclude(p => p.Professor)
                .Include(s => s.CustomOptions)
                .ThenInclude(co => co.Professor)
                .ToListAsync();

            return students.Where(s => (s.CustomOptions.Count() > 0 || s.Options.Count() > 0) &&
                            (s.CustomOptions.Where(co => co.Status.Equals("accepted")) != null ||
                             s.Options.Where(o => o.Status.Equals("accepted")) != null))
                .OrderByDescending(s => s.Grade).ToList();
        }

        public bool CheckIfAvailable(Users user)
        {
            var student = FindByCondition(s => s.UserId.Equals(user.Id)).SingleOrDefault();
            var options = RepositoryContext.Options.Where(o => o.StudentId.Equals(student.GDPRCode) && o.Status.Equals("accepted")).ToList();
            if(options == null)
            {
                var customOptions = RepositoryContext.CustomOptions.Where(o => o.StudentId.Equals(student.GDPRCode) && o.Status.Equals("accepted")).ToList();
                if(customOptions != null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                return true;
            }
        }

        public Task<Students> FindStudentByCondition(Expression<Func<Students, bool>> expression)
        {
            return FindByCondition(expression).SingleOrDefaultAsync();
        }

        public Task<List<Students>> FindStudentsByCondition(Expression<Func<Students, bool>> expression)
        {
            return FindByCondition(expression).ToListAsync();
        }

        public string GetEmail(string studentId)
        {
            var student = FindByCondition(s => s.GDPRCode.Equals(studentId)).Include(s => s.User).FirstOrDefault();
            return student.User.Email;
        }

        public bool CheckIfStudentExists(string GDPRCode)
        {
            return FindByCondition(s => s.GDPRCode.Equals(GDPRCode)).SingleOrDefault() != null;
        }
    }
}
