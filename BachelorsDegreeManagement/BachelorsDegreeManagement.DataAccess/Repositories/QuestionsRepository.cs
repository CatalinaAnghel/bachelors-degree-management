﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class QuestionsRepository: RepositoryBase<Questions>, IQuestionsRepository
    {
        public QuestionsRepository(BachelorsDegreeManagementDbContext repositoryContext): base(repositoryContext)
        {
        }

        public async Task<List<Questions>> FindAllQuestions()
        {
            return await FindAll().ToListAsync();
        }

        public async Task<Questions> FindQuestionByCondition(Expression<Func<Questions, bool>> expression)
        {
            return await FindByCondition(expression).SingleOrDefaultAsync();
        }

        public async Task<List<Questions>> FindQuestionsByCondition(Expression<Func<Questions, bool>> expression)
        {
            return await FindByCondition(expression).ToListAsync();
        }
    }
}
