﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class UserTagsRepository: RepositoryBase<UserTags>, IUserTagsRepository
    {
        public UserTagsRepository(BachelorsDegreeManagementDbContext repositoryContext):
            base(repositoryContext){}


        public int GetNumberOfStudents(int tagId)
        {
            return (from ut in RepositoryContext.UserTags
                    join u in RepositoryContext.Users on ut.UserId equals u.Id
                    join s in RepositoryContext.Students on u.Id equals s.UserId
                    where ut.TagId == tagId
                    select ut).Count();
        }

        public int GetNumberOfProfessors(int tagId)
        {
            return (from ut in RepositoryContext.UserTags
                    join u in RepositoryContext.Users on ut.UserId equals u.Id
                    join p in RepositoryContext.Professors on u.Id equals p.UserId
                    where ut.TagId == tagId
                    select ut).Count();
        }
    }
}
