﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class OptionsRepository : RepositoryBase<Options>, IOptionsRepository
    {
        public OptionsRepository(BachelorsDegreeManagementDbContext repositoryContext)
            : base(repositoryContext)
        {

        }
        public async Task<List<Options>> FindAllOptions()
        {
            return await FindAll()
                .Include(o => o.Project)
                .ThenInclude(p => p.Professor).AsNoTracking()
                .Include(o => o.Student)
                .ThenInclude(s => s.User).AsNoTracking()
                .ToListAsync();
        }

        public async Task<Options> FindOptionByCondition(Expression<Func<Options, bool>> expression)
        {
            return await FindByCondition(expression)
                .Include(o => o.Project)
                .ThenInclude(p => p.Professor).AsNoTracking()
                .Include(o => o.Student)
                .ThenInclude(s => s.User).AsNoTracking()
                .SingleOrDefaultAsync();
        }

        public Options FindOptionByConditionSync(Expression<Func<Options, bool>> expression)
        {
            return FindByCondition(expression)
                .Include(o => o.Project)
                .ThenInclude(p => p.Professor).AsNoTracking()
                .Include(o => o.Student)
                .ThenInclude(s => s.User).AsNoTracking()
                .SingleOrDefault();
        }

        public async Task<List<Options>> FindOptionsByCondition(Expression<Func<Options, bool>> expression)
        {
            return await FindByCondition(expression)
                .Include(o => o.Project)
                .ThenInclude(p => p.Professor)
                .Include(o => o.Student)
                .ThenInclude(s=> s.User)
                .ToListAsync();
        }
        public List<Options> FindOptionsByConditionSync(Expression<Func<Options, bool>> expression)
        {
            return FindByCondition(expression)
                .Include(o => o.Project)
                .ThenInclude(p => p.Professor)
                .Include(o => o.Student)
                .ThenInclude(s => s.User)
                .ToList();
        }
    }
}
