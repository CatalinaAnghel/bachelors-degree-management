﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class DepartmentsRepository: RepositoryBase<Departments>, IDepartmentsRepository
    {
        public DepartmentsRepository(BachelorsDegreeManagementDbContext repositoryContext):
            base(repositoryContext)
        {

        }

        public async Task<List<Departments>> FindAllDepartments()
        {
            return await FindAll().ToListAsync();
        }

        public async Task<Departments> FindDepartmentByCondition(Expression<Func<Departments, bool>> expression)
        {
            return await FindByCondition(expression).SingleOrDefaultAsync();
        }

        public async Task<List<Departments>> FindDepartmentsByCondition(Expression<Func<Departments, bool>> expression)
        {
            return await FindByCondition(expression).ToListAsync();
        }

        public int GetNumberOfStudents(int departmentId)
        {
            int noStudents = (from student in RepositoryContext.Students
                              join g in RepositoryContext.Groups on student.GroupId equals g.Id
                              join s in RepositoryContext.Specializations on g.SpecializationId equals s.Id
                              join d in RepositoryContext.Departments on s.DepartmentId equals d.Id
                              where d.Id == departmentId
                              select student
                              ).Count();

            return noStudents;
        }

        public int GetNumberOfProfessors(int departmentId)
        {
            int noProfessors = (from professor in RepositoryContext.Professors
                                join d in RepositoryContext.Departments on professor.DepartmentId equals d.Id
                                where d.Id == departmentId
                                select professor
                              ).Count();

            return noProfessors;
        }

        public int GetNumberOfGroups(int departmentId)
        {
            int noGroups = (from g in RepositoryContext.Groups
                            join s in RepositoryContext.Specializations on g.SpecializationId equals s.Id
                            join d in RepositoryContext.Departments on s.DepartmentId equals d.Id
                            where d.Id == departmentId
                            select g
                              ).Count();

            return noGroups;
        }
    }
}
