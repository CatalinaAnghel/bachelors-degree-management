﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BachelorsDegreeManagementDbContext dbContext;
        public UnitOfWork(BachelorsDegreeManagementDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        private ICustomOptionsRepository _CustomOption;
        private IDeadlinesRepository _Deadline;
        private IDepartmentsRepository _Department;
        private IEmailTemplatesRepository _EmailTemplate;
        private IGroupsRepository _Group;
        private IOptionsRepository _Option;
        private IPersonalityTestResultsRepository _PersonalityTestResult;
        private IProfessorsRepository _Professor;
        private IProjectsRepository _Project;
        private IProjectTagsRepository _ProjectTag;
        private IQuestionsRepository _Question;
        private IStudentsRepository _Student;
        private ISystemVariablesRepository _SystemVariable;
        private ITagsRepository _Tag;
        private IUserTagsRepository _UserTag;
        private ISpecializationsRepository _Specialization;

        public ICustomOptionsRepository CustomOptionsRepository
        {
            get
            {
                if (this._CustomOption == null)
                {
                    this._CustomOption = new CustomOptionsRepository(dbContext);
                }
                return this._CustomOption;
            }
        }

        public IDeadlinesRepository DeadlinesRepository
        {
            get
            {
                if (this._Deadline == null)
                {
                    this._Deadline = new DeadlinesRepository(dbContext);
                }
                return this._Deadline;
            }
        }

        public IDepartmentsRepository DepartmentsRepository
        {
            get
            {
                if (this._Department == null)
                {
                    this._Department = new DepartmentsRepository(dbContext);
                }
                return this._Department;
            }
        }

        public IEmailTemplatesRepository EmailTemplatesRepository
        {
            get
            {
                if (this._EmailTemplate == null)
                {
                    this._EmailTemplate = new EmailTemplatesRepository(dbContext);
                }
                return this._EmailTemplate;
            }
        }

        public IGroupsRepository GroupsRepository
        {
            get
            {
                if (this._Group == null)
                {
                    this._Group = new GroupsRepository(dbContext);
                }
                return this._Group;
            }
        }
        public IOptionsRepository OptionsRepository
        {
            get
            {
                if (this._Option == null)
                {
                    this._Option = new OptionsRepository(dbContext);
                }
                return this._Option;
            }
        }
        public IPersonalityTestResultsRepository PersonalityTestResultsRepository
        {
            get
            {
                if (this._PersonalityTestResult == null)
                {
                    this._PersonalityTestResult = new PersonalityTestResultsRepository(dbContext);
                }
                return this._PersonalityTestResult;
            }
        }

        public IProfessorsRepository ProfessorsRepository
        {
            get
            {
                if (this._Professor == null)
                {
                    this._Professor = new ProfessorsRepository(dbContext);
                }
                return this._Professor;
            }
        }

        public IQuestionsRepository QuestionsRepository
        {
            get
            {
                if (this._Question == null)
                {
                    this._Question = new QuestionsRepository(dbContext);
                }
                return this._Question;
            }
        }

        public IStudentsRepository StudentsRepository
        {
            get
            {
                if (this._Student == null)
                {
                    this._Student = new StudentsRepository(dbContext);
                }
                return this._Student;
            }
        }

        public ISystemVariablesRepository SystemVariablesRepository
        {
            get
            {
                if (this._SystemVariable == null)
                {
                    this._SystemVariable = new SystemVariablesRepository(dbContext);
                }
                return this._SystemVariable;
            }
        }

        public IProjectsRepository ProjectsRepository
        {
            get
            {
                if (this._Project == null)
                {
                    this._Project = new ProjectsRepository(dbContext);
                }
                return this._Project;
            }
        }

        public IProjectTagsRepository ProjectTagsRepository
        {
            get
            {
                if (this._ProjectTag == null)
                {
                    this._ProjectTag = new ProjectTagsRepository(dbContext);
                }
                return this._ProjectTag;
            }
        }

        public ITagsRepository TagsRepository
        {
            get
            {
                if (this._Tag == null)
                {
                    this._Tag = new TagsRepository(dbContext);
                }
                return this._Tag;
            }
        }

        public IUserTagsRepository UserTagsRepository
        {
            get
            {
                if (this._UserTag == null)
                {
                    this._UserTag = new UserTagsRepository(dbContext);
                }
                return this._UserTag;
            }
        }

        public ISpecializationsRepository SpecializationsRepository
        {
            get
            {
                if (this._Specialization == null)
                {
                    this._Specialization = new SpecializationsRepository(dbContext);
                }
                return this._Specialization;
            }
        }

        public int Complete()
        {
            return dbContext.SaveChanges();
        }

        public async Task<int> CompleteAsync()
        {
            return await dbContext.SaveChangesAsync();
        }

        public void Dispose() => dbContext.Dispose();

        public void Detach()
        {
            dbContext.DetachAllEntities();
        }
    }
}
