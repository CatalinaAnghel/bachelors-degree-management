﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class TagsRepository: RepositoryBase<Tags>, ITagsRepository
    {
        public TagsRepository(BachelorsDegreeManagementDbContext repositoryContext)
           : base(repositoryContext)
        {
        }

        public List<Tags> GetUsersTags(Users user)
        {
            var tags = (from tag in RepositoryContext.Tags
                        join userTag in RepositoryContext.UserTags on tag.Id equals userTag.TagId
                        where userTag.UserId.Equals(user.Id)
                        select tag).ToList();
            return tags;
        }

        public List<Tags> GetAvailableTags(Users user)
        {
            var tags = (from tag in RepositoryContext.Tags
                        join userTag in RepositoryContext.UserTags on tag.Id equals userTag.TagId into grouping
                        from userTag in grouping.DefaultIfEmpty()
                        where !(from tag in RepositoryContext.Tags
                                join userTag in RepositoryContext.UserTags on tag.Id equals userTag.TagId
                                where userTag.UserId.Equals(user.Id)
                                select tag)
                                .Contains(tag)
                        select tag).Distinct().ToList();
            return tags;
        }

    }
}
