﻿using BachelorsDegreeManagement.ApplicationLogic.Abstractions;
using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using BachelorsDegreeManagement.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BachelorsDegreeManagement.DataAccess.Repositories
{
    public class PersonalityTestResultsRepository: RepositoryBase<PersonalityTestResults>, IPersonalityTestResultsRepository
    {
        public PersonalityTestResultsRepository(BachelorsDegreeManagementDbContext repositoryContext)
          : base(repositoryContext)
        {
        }
    }
}
