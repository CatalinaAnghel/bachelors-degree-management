﻿using BachelorsDegreeManagement.ApplicationLogic.DataModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BachelorsDegreeManagement.DataAccess.Data
{
    public class BachelorsDegreeManagementDbContext : IdentityDbContext
    {
        public BachelorsDegreeManagementDbContext(DbContextOptions<BachelorsDegreeManagementDbContext> options)
            : base(options)
        {
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Deadlines> Deadlines { get; set; }
        public DbSet<Departments> Departments { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<CustomOptions> CustomOptions { get; set; }
        public DbSet<Options> Options { get; set; }
        public DbSet<PersonalityTestResults> PersonalityTestResults { get; set; }
        public DbSet<Professors> Professors { get; set; }
        public DbSet<Projects> Projects { get; set; }
        public DbSet<ProjectTags> ProjectTags { get; set; }
        public DbSet<Questions> Questions { get; set; }
        public DbSet<Specializations> Specializations { get; set; }
        public DbSet<Students> Students { get; set; }
        public DbSet<Tags> Tags { get; set; }
        public DbSet<UserTags> UserTags { get; set; }
        public DbSet<SystemVariables> SystemVariables { get; set; }
        public DbSet<EmailTemplates> EmailTemplates { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            const string ADMIN_ID = "a18be9c0-aa65-4af8-bd17-00bd9344e575";
            const string ROLE_ID_1 = "a18be9c0-aa65-4af8-bd17-00bd9344e571";
            const string ROLE_ID_2 = "a18be9c0-aa65-4af8-bd17-00bd9344e572";
            const string ROLE_ID_3 = "a18be9c0-aa65-4af8-bd17-00bd9344e573";

            // Add roles:
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id = ROLE_ID_1,
                    Name = "Administrator",
                    NormalizedName = "Admin"
                },
                new IdentityRole
                {
                    Id = ROLE_ID_2,
                    Name = "Professor",
                    NormalizedName = "Professor"
                },
                new IdentityRole
                {
                    Id = ROLE_ID_3,
                    Name = "Student",
                    NormalizedName = "Student"
                }
           );

            // Add an administrator:
            var hasher = new PasswordHasher<Users>();
            builder.Entity<Users>().HasData(
                new Users
                {
                    Id = ADMIN_ID,
                    UserName = "admin@ace.ucv.ro",
                    NormalizedUserName = "ADMIN@ACE.UCV.RO",
                    Email = "admin@ace.ucv.ro",
                    NormalizedEmail = "ADMIN@ACE.UCV.RO",
                    EmailConfirmed = true,
                    PasswordHash = hasher.HashPassword(null, "Admin1234@"),
                    SecurityStamp = string.Empty
                }
           );

            // Assign a role to the administrator
            builder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    RoleId = ROLE_ID_1,
                    UserId = ADMIN_ID
                }
            );

            // insert the departments:
            builder.Entity<Departments>().HasData(
                new Departments
                {
                    Id = 1,
                    Name = "DCTI"
                },
                new Departments
                {
                    Id = 2,
                    Name = "DAE"
                },
                new Departments
                {
                    Id = 3,
                    Name = "DMR"
                }
                );

            // Add the specializations:
            builder.Entity<Specializations>().HasData(
                new Specializations
                {
                    Id = 1,
                    Name = "Calculatoare cu predare in limba romana",
                    Acronym = "CSR",
                    DepartmentId = 1
                },
                new Specializations
                {
                    Id = 2,
                    Name = "Calculatoare cu predare in limba engleza",
                    Acronym = "CSE",
                    DepartmentId = 1
                },
                new Specializations
                {
                    Id = 3,
                    Name = "Electronica aplicata",
                    Acronym = "ELA",
                    DepartmentId = 2
                },
                new Specializations
                {
                    Id = 4,
                    Name = "Automatica si informatica aplicata",
                    Acronym = "AIA",
                    DepartmentId = 2
                },
                new Specializations
                {
                    Id = 5,
                    Name = "Ingineria sistemelor multimedia",
                    Acronym = "ISM",
                    DepartmentId = 2
                },
                new Specializations
                {
                    Id = 6,
                    Name = "Mecatronica",
                    Acronym = "MEC",
                    DepartmentId = 3
                },
                new Specializations
                {
                    Id = 7,
                    Name = "Robotica",
                    Acronym = "ROB",
                    DepartmentId = 3
                }
            );

            builder.Entity<Groups>().HasData(
                new Groups
                {
                    Id = 1,
                    Name = "CR3.1",
                    SpecializationId = 1,
                    StudyYear = 3
                },
                new Groups
                {
                    Id = 2,
                    Name = "CR3.2",
                    SpecializationId = 1,
                    StudyYear = 3
                },
                new Groups
                {
                    Id = 3,
                    Name = "CR3.3",
                    SpecializationId = 1,
                    StudyYear = 3
                },
                new Groups
                {
                    Id = 4,
                    Name = "CEN3.1",
                    SpecializationId = 1,
                    StudyYear = 3
                },
                new Groups
                {
                    Id = 5,
                    Name = "CEN3.2",
                    SpecializationId = 2,
                    StudyYear = 3
                },
                new Groups
                {
                    Id = 6,
                    Name = "CEN3.3",
                    SpecializationId = 2,
                    StudyYear = 3
                }
            );

            builder.Entity<Categories>().HasData(
                new Categories
                {
                    Id = 1,
                    Name = "Surgency"
                },
                new Categories
                {
                    Id = 2,
                    Name = "Agreeableness"
                },
                new Categories
                {
                    Id = 3,
                    Name = "Conscientiousness"
                },
                new Categories
                {
                    Id = 4,
                    Name = "Emotional Stability"
                },
                new Categories
                {
                    Id = 5,
                    Name = "Imagination"
                }
                );

            builder.Entity<Questions>().HasData(
                new Questions
                {
                    Id = 1,
                    CategoryId = 1,
                    Keyed = "+",
                    Content = "I am the life of a party"
                },
                new Questions
                {
                    Id = 2,
                    CategoryId = 2,
                    Keyed = "-",
                    Content = "I feel little concern for others"
                },
                new Questions
                {
                    Id = 3,
                    CategoryId = 3,
                    Keyed = "+",
                    Content = "I am always prepared"
                },
                new Questions
                {
                    Id = 4,
                    CategoryId = 4,
                    Keyed = "-",
                    Content = "I get stressed out easily"
                },
                new Questions
                {
                    Id = 5,
                    CategoryId = 5,
                    Keyed = "+",
                    Content = "I have a rich vocabulary"
                },
                new Questions
                {
                    Id = 6,
                    CategoryId = 1,
                    Keyed = "-",
                    Content = "I don't talk a lot"
                },
                new Questions
                {
                    Id = 7,
                    CategoryId = 2,
                    Keyed = "+",
                    Content = "I am interested in people"
                },
                new Questions
                {
                    Id = 8,
                    CategoryId = 3,
                    Keyed = "-",
                    Content = "I leave my belongings around"
                },
                new Questions
                {
                    Id = 9,
                    CategoryId = 4,
                    Keyed = "+",
                    Content = "I am relaxed most of the time"
                },
                new Questions
                {
                    Id = 10,
                    CategoryId = 5,
                    Keyed = "-",
                    Content = "I have difficulty understanding abstract ideas"
                },
                new Questions
                {
                    Id = 11,
                    CategoryId = 1,
                    Keyed = "+",
                    Content = "I feel comfortable around people"
                },
                new Questions
                {
                    Id = 12,
                    CategoryId = 2,
                    Keyed = "-",
                    Content = "I insult people"
                },
                new Questions
                {
                    Id = 13,
                    CategoryId = 3,
                    Keyed = "+",
                    Content = "I pay attention to details"
                },
                new Questions
                {
                    Id = 14,
                    CategoryId = 4,
                    Keyed = "-",
                    Content = "I worry about things"
                },
                new Questions
                {
                    Id = 15,
                    CategoryId = 5,
                    Keyed = "+",
                    Content = "I have a vivid imagination"
                },
                new Questions
                {
                    Id = 16,
                    CategoryId = 1,
                    Keyed = "-",
                    Content = "I keep in the background"
                },
                new Questions
                {
                    Id = 17,
                    CategoryId = 2,
                    Keyed = "+",
                    Content = "I sympathize with others' feeling"
                },
                new Questions
                {
                    Id = 18,
                    CategoryId = 3,
                    Keyed = "-",
                    Content = "I make a mess of things"
                },
                new Questions
                {
                    Id = 19,
                    CategoryId = 4,
                    Keyed = "+",
                    Content = "I seldom feel blue"
                },
                new Questions
                {
                    Id = 20,
                    CategoryId = 5,
                    Keyed = "-",
                    Content = "I am not interested in abstract ideas"
                },
                new Questions
                {
                    Id = 21,
                    CategoryId = 1,
                    Keyed = "+",
                    Content = "I start conversations"
                },
                new Questions
                {
                    Id = 22,
                    CategoryId = 2,
                    Keyed = "-",
                    Content = "I am not interested in other people's problems"
                },
                new Questions
                {
                    Id = 23,
                    CategoryId = 3,
                    Keyed = "+",
                    Content = "I get chores done right away"
                }, 
                new Questions
                {
                    Id = 24,
                    CategoryId = 4,
                    Keyed = "-",
                    Content = "I am easily disturbed"
                }, 
                new Questions
                {
                    Id = 25,
                    CategoryId = 5,
                    Keyed = "+",
                    Content = "I have excellent ideas"
                }, 
                new Questions
                {
                    Id = 26,
                    CategoryId = 1,
                    Keyed = "-",
                    Content = "I have a little to say"
                }, 
                new Questions
                {
                    Id = 27,
                    CategoryId = 2,
                    Keyed = "+",
                    Content = "I have a soft heart"
                }, 
                new Questions
                {
                    Id = 28,
                    CategoryId = 3,
                    Keyed = "-",
                    Content = "I often forget to put things back in their proper place"
                }, 
                new Questions
                {
                    Id = 29,
                    CategoryId = 4,
                    Keyed = "+",
                    Content = "I get upset easily"
                }, 
                new Questions
                {
                    Id = 30,
                    CategoryId = 5,
                    Keyed = "-",
                    Content = "I do not have a good imagination"
                }, 
                new Questions
                {
                    Id = 31,
                    CategoryId = 1,
                    Keyed = "+",
                    Content = "I talk to a lot of different people at parties"
                }, 
                new Questions
                {
                    Id = 32,
                    CategoryId = 2,
                    Keyed = "-",
                    Content = "I am not really interested in others"
                }, 
                new Questions
                {
                    Id = 33,
                    CategoryId = 3,
                    Keyed = "+",
                    Content = "I like order"
                }, 
                new Questions
                {
                    Id = 34,
                    CategoryId = 4,
                    Keyed = "-",
                    Content = "I change my mood a lot"
                }, 
                new Questions
                {
                    Id = 35,
                    CategoryId = 5,
                    Keyed = "+",
                    Content = "I am quick to understand things"
                }, 
                new Questions
                {
                    Id = 36,
                    CategoryId = 1,
                    Keyed = "-",
                    Content = "I don't like to draw attention to myself"
                }, 
                new Questions
                {
                    Id = 37,
                    CategoryId = 2,
                    Keyed = "+",
                    Content = "I take time out for others"
                }, 
                new Questions
                {
                    Id = 38,
                    CategoryId = 3,
                    Keyed = "-",
                    Content = "I shrik my duties"
                }, 
                new Questions
                {
                    Id = 39,
                    CategoryId = 4,
                    Keyed = "+",
                    Content = "I have frequent mood swings"
                },
                new Questions
                {
                    Id = 40,
                    CategoryId = 5,
                    Keyed = "-",
                    Content = "I use difficult words"
                },
                new Questions
                {
                    Id = 41,
                    CategoryId = 1,
                    Keyed = "+",
                    Content = "I don't mind being the center of attention"
                },
                new Questions
                {
                    Id = 42,
                    CategoryId = 2,
                    Keyed = "-",
                    Content = "I feel others' emotions"
                },
                new Questions
                {
                    Id = 43,
                    CategoryId = 3,
                    Keyed = "+",
                    Content = "I follow a schedule"
                },
                new Questions
                {
                    Id = 44,
                    CategoryId = 4,
                    Keyed = "-",
                    Content = "I get irritated easily"
                },
                new Questions
                {
                    Id = 45,
                    CategoryId = 5,
                    Keyed = "+",
                    Content = "I spend time reflecting on things"
                },
                new Questions
                {
                    Id = 46,
                    CategoryId = 1,
                    Keyed = "-",
                    Content = "I am quiet around strangers"
                },
                new Questions
                {
                    Id = 47,
                    CategoryId = 2,
                    Keyed = "+",
                    Content = "I make people feel at ease"
                },
                new Questions
                {
                    Id = 48,
                    CategoryId = 3,
                    Keyed = "-",
                    Content = "I am exacting in my work"
                },
                new Questions
                {
                    Id = 49,
                    CategoryId = 4,
                    Keyed = "+",
                    Content = "I often feel blue"
                },
                new Questions
                {
                    Id = 50,
                    CategoryId = 5,
                    Keyed = "-",
                    Content = "I am full of ideas"
                }
                );
            builder.Entity<SystemVariables>().HasIndex(sv => sv.Name).IsUnique();
            builder.Entity<SystemVariables>().HasData(
                new SystemVariables
                {
                    Id = 1,
                    Name = "ValidationNumberOfHours",
                    Value = "24"
                },
                new SystemVariables
                {
                    Id = 2,
                    Name = "SortingNumberOfDays",
                    Value = "3"
                },
                new SystemVariables
                {
                    Id = 3,
                    Name = "NumberOfOptions",
                    Value = "3"
                }
                ,
                new SystemVariables
                {
                    Id = 4,
                    Name = "ProjectsInsertionNumberOfDays",
                    Value = "5"
                },
                new SystemVariables
                {
                    Id = 5,
                    Name = "ComplexityLevel1",
                    Value = "Easy"
                },
                new SystemVariables
                {
                    Id = 6,
                    Name = "ComplexityLevel2",
                    Value = "Medium"
                },
                new SystemVariables
                {
                    Id = 7,
                    Name = "ComplexityLevel3",
                    Value = "Advanced"
                },
                new SystemVariables
                {
                    Id = 8,
                    Name = "TagsPercentage",
                    Value = "0.5"
                },
                new SystemVariables
                {
                    Id = 9,
                    Name = "PersonalityPercentage",
                    Value = "0.25"
                },
                new SystemVariables
                {
                    Id = 10,
                    Name = "ComplexityScore0",
                    Value = "1.0"
                },
                new SystemVariables
                {
                    Id = 11,
                    Name = "ComplexityScore1",
                    Value = "0.5"
                },
                new SystemVariables
                {
                    Id = 12,
                    Name = "ComplexityScore2",
                    Value = "0.0"
                },
                new SystemVariables
                {
                    Id = 13,
                    Name = "ProjectPersonalityPercentage",
                    Value = "0.2"
                },
                new SystemVariables
                {
                    Id = 14,
                    Name = "ProjectTagsPercentage",
                    Value = "0.3"
                },
                new SystemVariables
                {
                    Id = 15,
                    Name = "ProjectComplexityPercentage",
                    Value = "0.3"
                },
                new SystemVariables
                {
                    Id = 16,
                    Name = "SupervisorStyle_0",
                    Value = "Laissez-faire"
                },
                new SystemVariables
                {
                    Id = 17,
                    Name = "SupervisorStyle_1",
                    Value = "Directorial"
                },
                new SystemVariables
                {
                    Id = 18,
                    Name = "SupervisorStyle_2",
                    Value = "Pastoral"
                },
                new SystemVariables
                {
                    Id = 19,
                    Name = "SupervisorStyle_3",
                    Value = "Contractual"
                },
                new SystemVariables
                {
                    Id = 20,
                    Name = "ProjectSupervisorStylePercentage",
                    Value = "0.2"
                },
                new SystemVariables
                {
                    Id = 21,
                    Name = "SupervisorStylePercentage",
                    Value = "0.25"
                },
                new SystemVariables
                {
                    Id = 22,
                    Name = "SupervisorStyleScore0",
                    Value = "1.0"
                },
                new SystemVariables
                {
                    Id = 23,
                    Name = "SupervisorStyleScore1",
                    Value = "0.5"
                },
                new SystemVariables
                {
                    Id = 24,
                    Name = "SupervisorStyleScore2",
                    Value = "0.0"
                }
            );

           builder.Entity<EmailTemplates>().HasIndex(sv => sv.Usage).IsUnique();
        }
        public void DetachAllEntities()
        {
            var changedEntriesCopy = this.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }
    }
}
