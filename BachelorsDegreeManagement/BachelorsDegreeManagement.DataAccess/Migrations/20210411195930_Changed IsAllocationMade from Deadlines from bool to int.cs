﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class ChangedIsAllocationMadefromDeadlinesfrombooltoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IsAllocationMade",
                table: "Deadlines",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "ca8442cb-8cdd-42e4-90ac-00629cd7d5d8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "bf223cc3-b7f8-4f7d-9245-d74236f12375");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "10e4bc91-1044-4d0e-9cf4-b7067027702d");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "4236c01d-7204-41a4-bf53-4028b08779b7", "AQAAAAEAACcQAAAAEAuEfrjJ6Mgi6bL6fE3CbGCOaWc04CpJ+K97GCCdV5eMqB00X33mUdPiAeK0b2cJmA==" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 5,
                column: "Name",
                value: "ComplexityLevel1");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "ComplexityLevel2");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 7,
                column: "Name",
                value: "ComplexityLevel3");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsAllocationMade",
                table: "Deadlines",
                type: "bit",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "b3398517-47ae-404e-8ce7-cade60c19136");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "99b3b9e7-e9e1-47ec-9bd8-beca94582325");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "c9155a7a-3f16-45ea-a07f-0f3eb400dbf7");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "ca44fda5-928d-4a16-933f-11b35eaf4c1b", "AQAAAAEAACcQAAAAEMnBSUXP6IfNSe9xWjHGcRsi7My8nQlpebtDJlKO0GtpvhPGcYBogfziSYSIPyyHRA==" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 5,
                column: "Name",
                value: "Complexity");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "Complexity");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 7,
                column: "Name",
                value: "Complexity");
        }
    }
}
