﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class AddEmailTemplates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmailTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Usage = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTemplates", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "e2d69b3b-fede-46e6-b74a-39f413ec8e20");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "60fa9f1c-4d60-4c4c-8170-ca53055f93bf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "2fc6da1f-a0d1-4611-a6cb-b2c738a68da1");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "d447a0ed-bc00-4fc1-9c0a-07c084e35396", "AQAAAAEAACcQAAAAEBJ5I4NDcjI/gRNLJG49/I+fPEbRLyukKLbY1mNCj28W6nuLtII8t/JJpxkaSkbRvA==" });

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplates_Usage",
                table: "EmailTemplates",
                column: "Usage",
                unique: true,
                filter: "[Usage] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailTemplates");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "c698fdb1-2fa4-4c99-8a0c-20fea34265c6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "ca824fad-dcb3-447a-a629-913a98a34a5c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "ada64850-92a9-42c1-977f-b00c7aeab237");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "8180acef-a5d5-403a-9338-839e07e1b901", "AQAAAAEAACcQAAAAEKmfUS0elQrD63XnuyDdsUIRFTle+m6hgWcEHwe9p/OpYs/EifzqZVfQ0faA/Ipthg==" });
        }
    }
}
