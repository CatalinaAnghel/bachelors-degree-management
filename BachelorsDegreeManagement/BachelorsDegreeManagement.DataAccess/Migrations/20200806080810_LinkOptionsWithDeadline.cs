﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class LinkOptionsWithDeadline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeadlineId",
                table: "Options",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Deadlines",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "DeadlineId",
                table: "CustomOptions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "f368f371-01ae-4a82-a542-1ee8109d917d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "0d2148b1-7f75-44ff-bd52-d3716a0dbf94");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "0162f7ee-4891-44c4-9996-d94d24aea249");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6bbb178f-4107-4d8c-9cb4-b3b6d5334e3f", "AQAAAAEAACcQAAAAEOD73aL7ay04XAN6FiO5DVOg+Op8qr0+Ssq5YMDAy/nBlK548LE/GXWeJ5KkCD4mlw==" });

            migrationBuilder.CreateIndex(
                name: "IX_Options_DeadlineId",
                table: "Options",
                column: "DeadlineId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomOptions_DeadlineId",
                table: "CustomOptions",
                column: "DeadlineId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomOptions_Deadlines_DeadlineId",
                table: "CustomOptions",
                column: "DeadlineId",
                principalTable: "Deadlines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Options_Deadlines_DeadlineId",
                table: "Options",
                column: "DeadlineId",
                principalTable: "Deadlines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomOptions_Deadlines_DeadlineId",
                table: "CustomOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Options_Deadlines_DeadlineId",
                table: "Options");

            migrationBuilder.DropIndex(
                name: "IX_Options_DeadlineId",
                table: "Options");

            migrationBuilder.DropIndex(
                name: "IX_CustomOptions_DeadlineId",
                table: "CustomOptions");

            migrationBuilder.DropColumn(
                name: "DeadlineId",
                table: "Options");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Deadlines");

            migrationBuilder.DropColumn(
                name: "DeadlineId",
                table: "CustomOptions");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "4268d485-9031-4c31-b747-479c27021e7c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "29470f8a-9022-4444-9b4c-1f182e0f1a03");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "4773c5f9-ac5d-4192-b07e-1ab4c239bdf0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "620b380d-6caf-417e-8e7e-8a38e44a6515", "AQAAAAEAACcQAAAAEJM3HqHvhH57pt6TU3KAFrVAURT2eBkqUd3Q3ZDxwKr/o50oyNax6aOzJji1kuuu2g==" });
        }
    }
}
