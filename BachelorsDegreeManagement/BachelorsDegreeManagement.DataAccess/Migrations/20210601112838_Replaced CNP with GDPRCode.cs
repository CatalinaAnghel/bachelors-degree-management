﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class ReplacedCNPwithGDPRCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomOptions_Professors_ProfessorId",
                table: "CustomOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomOptions_Students_StudentId",
                table: "CustomOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Options_Students_StudentId",
                table: "Options");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Professors_ProfessorId",
                table: "Projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Students",
                table: "Students");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Professors",
                table: "Professors");

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "CNP",
                keyValue: "1861212223344");

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "CNP",
                keyValue: "2861212223345");

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "CNP",
                keyValue: "1981101111111");

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "CNP",
                keyValue: "1981101111112");

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "CNP",
                keyValue: "2981101111111");

            migrationBuilder.DropColumn(
                name: "CNP",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "CNP",
                table: "Professors");

            migrationBuilder.AddColumn<string>(
                name: "GDPRCode",
                table: "Students",
                maxLength: 13,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "GDPRCode",
                table: "Professors",
                maxLength: 13,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Students",
                table: "Students",
                column: "GDPRCode");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Professors",
                table: "Professors",
                column: "GDPRCode");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "698d09df-b689-4380-9339-e955207edbb3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "b6039014-4ac7-41d2-b7f3-e79cf828ec0b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "722fa9a0-f872-4bc9-b1c4-f5d8ac63b6f3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "5a232cbc-74cf-4ff8-9585-4f4d6a3ce586", "AQAAAAEAACcQAAAAEJp6cz4xgn1GZtB2ZhJMQrgMGOOx+lxzGlV6SYpXPfNMJ1KxVx0RMoUnis9ZxdA2+Q==" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 2, "DAE" },
                    { 3, "DMR" }
                });

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 1,
                column: "DepartmentId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 2,
                column: "DepartmentId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 3,
                column: "DepartmentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 4,
                column: "DepartmentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 5,
                column: "DepartmentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 6,
                column: "DepartmentId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 7,
                column: "DepartmentId",
                value: 3);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomOptions_Professors_ProfessorId",
                table: "CustomOptions",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "GDPRCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomOptions_Students_StudentId",
                table: "CustomOptions",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "GDPRCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Options_Students_StudentId",
                table: "Options",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "GDPRCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Professors_ProfessorId",
                table: "Projects",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "GDPRCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomOptions_Professors_ProfessorId",
                table: "CustomOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomOptions_Students_StudentId",
                table: "CustomOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Options_Students_StudentId",
                table: "Options");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Professors_ProfessorId",
                table: "Projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Students",
                table: "Students");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Professors",
                table: "Professors");

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Departments",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Departments",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DropColumn(
                name: "GDPRCode",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "GDPRCode",
                table: "Professors");

            migrationBuilder.AddColumn<string>(
                name: "CNP",
                table: "Students",
                type: "nvarchar(13)",
                maxLength: 13,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "CNP",
                table: "Professors",
                type: "nvarchar(13)",
                maxLength: 13,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Students",
                table: "Students",
                column: "CNP");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Professors",
                table: "Professors",
                column: "CNP");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "4df13dda-79c5-420c-b067-2b5770b367d3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "13989092-74e7-4b99-8376-1c4a303b56f8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "c0847727-fa63-4fbc-9e28-9bdef41a4a4f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "803a60c1-a54c-4f57-935e-6568f8d71ed4", "AQAAAAEAACcQAAAAEKrgieu6V4Taw/HIMGUUtKaFaY5pZTmF5PHjMjPZ8S8G/gbE5sj1Aa+2ZVhfMF5R5w==" });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "CNP", "DepartmentId", "FirstName", "LastName", "ProjectsNumber", "UserId" },
                values: new object[,]
                {
                    { "1861212223344", 1, "Vasile", "Constantin", 30, null },
                    { "2861212223345", 1, "Ionela", "Popovici", 35, null }
                });

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 1,
                column: "DepartmentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 2,
                column: "DepartmentId",
                value: 0);

            migrationBuilder.InsertData(
                table: "Specializations",
                columns: new[] { "Id", "Acronym", "DepartmentId", "Name" },
                values: new object[,]
                {
                    { 7, "ROB", 0, "Robotica" },
                    { 5, "ISM", 0, "Ingineria sistemelor multimedia" },
                    { 4, "AIA", 0, "Automatica si informatica aplicata" },
                    { 3, "ELA", 0, "Electronica aplicata" },
                    { 6, "MEC", 0, "Mecatronica" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "CNP", "Complexity", "FirstName", "Grade", "GroupId", "LastName", "UserId" },
                values: new object[,]
                {
                    { "1981101111111", null, "Ion", 9.68f, 1, "Popescu", null },
                    { "2981101111111", null, "Alina", 8.9f, 1, "Ionescu", null },
                    { "1981101111112", null, "Andrei", 6.58f, 2, "Andreescu", null }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomOptions_Professors_ProfessorId",
                table: "CustomOptions",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "CNP",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomOptions_Students_StudentId",
                table: "CustomOptions",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "CNP",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Options_Students_StudentId",
                table: "Options",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "CNP",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Professors_ProfessorId",
                table: "Projects",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "CNP",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
