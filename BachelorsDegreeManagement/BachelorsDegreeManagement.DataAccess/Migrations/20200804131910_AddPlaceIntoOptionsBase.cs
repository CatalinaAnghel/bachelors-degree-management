﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class AddPlaceIntoOptionsBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Place",
                table: "Options",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Deadlines",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Place",
                table: "CustomOptions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "f76f4336-b61f-4960-a460-5142a05df9f4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "5fcdb273-fa91-4064-a88e-fb240620f9dc");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "2238a14a-7f55-47e9-ba15-eb8cc3338c9a");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "7723fd94-684d-41fd-a057-1c3f9d31e06c", "AQAAAAEAACcQAAAAEBg0jq9wROHA6e69qk5mrNki8hsiJYR0jF4+DLtpBMILWsvbymOGCWxOydLYbju/WQ==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Place",
                table: "Options");

            migrationBuilder.DropColumn(
                name: "Place",
                table: "CustomOptions");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Deadlines",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "0d771194-5b26-43b1-be53-8889bc480186");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "2cdd265c-d597-4ec8-9f17-e3c072bb53f0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "f2465a46-0186-44be-9b01-a03809832f0f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "b66642fd-84e3-41bc-958c-99a03505031c", "AQAAAAEAACcQAAAAEF1pHcDB7s59zfUYw6FNCXqjHlBSZvknsjS8MPar9JQ5RD9tbm7Bf0ZXGic/uaABEg==" });
        }
    }
}
