﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class Integratedthesupervisorstylematching : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupervisorStyle",
                table: "Students",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SupervisorStyle",
                table: "Professors",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "daecfb2f-8887-445e-888a-70b84ec1ecf4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "aa42c239-a0da-44ab-a5d8-6f60c66df26c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "ae4123f5-1a62-4ce9-8754-0241bfb8f031");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "39219d4c-d29b-415f-9303-55b0c9fe6698", "AQAAAAEAACcQAAAAEFaFSqylN7yftDKrj2ojW8yHeW8Ji2JJZotnVpsW3m5QkebfObWckS2ifK7mT8It1A==" });

            migrationBuilder.InsertData(
                table: "SystemVariables",
                columns: new[] { "Id", "Name", "Value" },
                values: new object[,]
                {
                    { 16, "SupervisorStyle_0", "Laissez-faire" },
                    { 17, "SupervisorStyle_1", "Directorial" },
                    { 18, "SupervisorStyle_2", "Pastoral" },
                    { 19, "SupervisorStyle_3", "Contractual" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DropColumn(
                name: "SupervisorStyle",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "SupervisorStyle",
                table: "Professors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "698d09df-b689-4380-9339-e955207edbb3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "b6039014-4ac7-41d2-b7f3-e79cf828ec0b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "722fa9a0-f872-4bc9-b1c4-f5d8ac63b6f3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "5a232cbc-74cf-4ff8-9585-4f4d6a3ce586", "AQAAAAEAACcQAAAAEJp6cz4xgn1GZtB2ZhJMQrgMGOOx+lxzGlV6SYpXPfNMJ1KxVx0RMoUnis9ZxdA2+Q==" });
        }
    }
}
