﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class ComplexityScores : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "35aeb413-1fdb-4d7e-8aad-34b93d6b905e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "5855d008-7ac2-443c-923b-4f9a495b85be");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "329b134c-d885-4b0b-8e8d-52caf30d98ad");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "fb2085aa-9118-4d55-878d-9696a955d928", "AQAAAAEAACcQAAAAED+RbAt/19lfmKbytAooppiAm0t5poz9kGPkqIJwtd/SvvsQF9FvudDu0ChTn4+VIg==" });

            migrationBuilder.InsertData(
                table: "SystemVariables",
                columns: new[] { "Id", "Name", "Value" },
                values: new object[,]
                {
                    { 10, "ComplexityScore0", "1.0" },
                    { 11, "ComplexityScore1", "0.5" },
                    { 12, "ComplexityScore2", "0.0" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "9edefd68-8206-498f-8ecb-1c1fc8c2ec20");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "a88eaec4-b8df-49cc-bc26-e7b17694bec1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "d0b327a8-2429-46f9-89a2-51406adb738e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "cbb24832-ae56-451b-9d26-4593b92b16db", "AQAAAAEAACcQAAAAEIh3AOA29GxAqIqzaPFyGzE0+tZqZem6KMTRFwcFTJqzK9yDwlwxpVHHzBHB0I4eLg==" });
        }
    }
}
