﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class DataSeedingMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "a18be9c0-aa65-4af8-bd17-00bd9344e573", "d7ec3298-aa3f-4e79-8d7e-3298144cc772", "Student", "Student" },
                    { "a18be9c0-aa65-4af8-bd17-00bd9344e571", "735f945e-dace-47b8-bde6-d7d224e26620", "Administrator", "Admin" },
                    { "a18be9c0-aa65-4af8-bd17-00bd9344e572", "be94cd6d-2219-470b-8953-cf3d326eb1fa", "Professor", "Professor" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "a18be9c0-aa65-4af8-bd17-00bd9344e575", 0, "db5fb05a-bfa2-4a8f-9e45-5dfcec3a989a", "Users", "admin@ace.ucv.ro", true, false, null, "ADMIN@ACE.UCV.RO", "ADMIN@ACE.UCV.RO", "AQAAAAEAACcQAAAAEGBolfzf08aeRkD1AMIvfCDn7EuictEtMaITK+u/sQ+3cSnYlBkzFNeUQudBQxIsUA==", null, false, "", false, "admin@ace.ucv.ro" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Surgency" },
                    { 4, "Emotional Stability" },
                    { 3, "Conscientiousness" },
                    { 2, "Agreeableness" },
                    { 5, "Imagination" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "DCTI" });

            migrationBuilder.InsertData(
                table: "Specializations",
                columns: new[] { "Id", "Acronym", "Name" },
                values: new object[,]
                {
                    { 1, "CSR", "Calculatoare cu predare in limba romana" },
                    { 2, "CSE", "Calculatoare cu predare in limba engleza" },
                    { 4, "AIA", "Automatica si informatica aplicata" },
                    { 5, "ISM", "Ingineria sistemelor multimedia" },
                    { 6, "MEC", "Mecatronica" },
                    { 7, "ROB", "Robotica" },
                    { 3, "ELA", "Electronica aplicata" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "a18be9c0-aa65-4af8-bd17-00bd9344e575", "a18be9c0-aa65-4af8-bd17-00bd9344e571" });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name", "SpecializationId", "StudyYear" },
                values: new object[,]
                {
                    { 1, "CR3.1", 1, 3 },
                    { 2, "CR3.2", 1, 3 },
                    { 6, "CEN3.3", 2, 3 },
                    { 4, "CEN3.1", 1, 3 },
                    { 5, "CEN3.2", 2, 3 },
                    { 3, "CR3.3", 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "CNP", "DepartmentId", "FirstName", "LastName", "ProjectsNumber", "UserId" },
                values: new object[,]
                {
                    { "2861212223345", 1, "Ionela", "Popovici", 35, null },
                    { "1861212223344", 1, "Vasile", "Constantin", 30, null }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "Content", "Keyed" },
                values: new object[,]
                {
                    { 9, 4, "I am relaxed most of the time", "+" },
                    { 14, 4, "I worry about things", "-" },
                    { 19, 4, "I seldom feel blue", "+" },
                    { 24, 4, "I am easily disturbed", "-" },
                    { 29, 4, "I get upset easily", "+" },
                    { 34, 4, "I change my mood a lot", "-" },
                    { 39, 4, "I have frequent mood swings", "+" },
                    { 44, 4, "I get irritated easily", "-" },
                    { 49, 4, "I often feel blue", "+" },
                    { 10, 5, "I have difficulty understanding abstract ideas", "-" },
                    { 15, 5, "I have a vivid imagination", "+" },
                    { 4, 4, "I get stressed out easily", "-" },
                    { 25, 5, "I have excellent ideas", "+" },
                    { 30, 5, "I do not have a good imagination", "-" },
                    { 35, 5, "I am quick to understand things", "+" },
                    { 40, 5, "I use difficult words", "-" },
                    { 45, 5, "I spend time reflecting on things", "+" },
                    { 50, 5, "I am full of ideas", "-" },
                    { 5, 5, "I have a rich vocabulary", "+" },
                    { 20, 5, "I am not interested in abstract ideas", "-" },
                    { 48, 3, "I am exacting in my work", "-" },
                    { 38, 3, "I shirk my duties", "-" },
                    { 6, 1, "I don't talk a lot", "-" },
                    { 11, 1, "I feel comfortable around people", "+" },
                    { 16, 1, "I keep in the background", "-" },
                    { 21, 1, "I start conversations", "+" },
                    { 26, 1, "I have a little to say", "-" },
                    { 31, 1, "I talk to a lot of different people at parties", "+" },
                    { 36, 1, "I don't like to draw attention to myself", "-" },
                    { 41, 1, "I don't mind being the center of attention", "+" },
                    { 46, 1, "I am quiet around strangers", "-" },
                    { 2, 2, "I feel little concern for others", "-" },
                    { 7, 2, "I am interested in people", "+" },
                    { 12, 2, "I insult people", "-" },
                    { 43, 3, "I follow a schedule", "+" },
                    { 17, 2, "I sympathize with others' feeling", "+" },
                    { 27, 2, "I have a soft heart", "+" },
                    { 32, 2, "I am not really interested in others", "-" },
                    { 37, 2, "I take time out for others", "+" },
                    { 42, 2, "I feel others' emotions", "-" },
                    { 47, 2, "I make people feel at ease", "+" },
                    { 3, 3, "I am always prepared", "+" },
                    { 8, 3, "I leave my belongings around", "-" },
                    { 13, 3, "I pay attention to details", "+" },
                    { 18, 3, "I make a mess of things", "-" },
                    { 23, 3, "I get chores done right away", "+" },
                    { 28, 3, "I often forget to put things back in their proper place", "-" },
                    { 33, 3, "I like order", "+" },
                    { 22, 2, "I am not interested in other people's problems", "-" },
                    { 1, 1, "I am the life of a party", "+" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "CNP", "FirstName", "Grade", "GroupId", "LastName", "UserId" },
                values: new object[] { "1981101111111", "Ion", 9.68f, 1, "Popescu", null });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "CNP", "FirstName", "Grade", "GroupId", "LastName", "UserId" },
                values: new object[] { "2981101111111", "Alina", 8.9f, 1, "Ionescu", null });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "CNP", "FirstName", "Grade", "GroupId", "LastName", "UserId" },
                values: new object[] { "1981101111112", "Andrei", 6.58f, 2, "Andreescu", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "a18be9c0-aa65-4af8-bd17-00bd9344e575", "a18be9c0-aa65-4af8-bd17-00bd9344e571" });

            migrationBuilder.DeleteData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "CNP",
                keyValue: "1861212223344");

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "CNP",
                keyValue: "2861212223345");

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "CNP",
                keyValue: "1981101111111");

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "CNP",
                keyValue: "1981101111112");

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "CNP",
                keyValue: "2981101111111");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Departments",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Specializations",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
