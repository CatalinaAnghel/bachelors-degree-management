﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class Projectscomplexityandlinktheprojectstothedeadlines : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Complexity",
                table: "Projects",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "DeadlineId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Discriminator",
                table: "Options",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Discriminator",
                table: "CustomOptions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "b3398517-47ae-404e-8ce7-cade60c19136");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "99b3b9e7-e9e1-47ec-9bd8-beca94582325");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "c9155a7a-3f16-45ea-a07f-0f3eb400dbf7");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "ca44fda5-928d-4a16-933f-11b35eaf4c1b", "AQAAAAEAACcQAAAAEMnBSUXP6IfNSe9xWjHGcRsi7My8nQlpebtDJlKO0GtpvhPGcYBogfziSYSIPyyHRA==" });

            migrationBuilder.InsertData(
                table: "SystemVariables",
                columns: new[] { "Id", "Name", "Value" },
                values: new object[,]
                {
                    { 4, "ProjectsInsertionNumberOfDays", "5" },
                    { 5, "ComplexityLevel1", "Easy" },
                    { 6, "ComplexityLevel2", "Medium" },
                    { 7, "ComplexityLevel3", "Advanced" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_DeadlineId",
                table: "Projects",
                column: "DeadlineId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Deadlines_DeadlineId",
                table: "Projects",
                column: "DeadlineId",
                principalTable: "Deadlines",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Deadlines_DeadlineId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_DeadlineId",
                table: "Projects");

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DropColumn(
                name: "Complexity",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "DeadlineId",
                table: "Projects");

            migrationBuilder.AlterColumn<string>(
                name: "Discriminator",
                table: "Options",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Discriminator",
                table: "CustomOptions",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "e2d69b3b-fede-46e6-b74a-39f413ec8e20");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "60fa9f1c-4d60-4c4c-8170-ca53055f93bf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "2fc6da1f-a0d1-4611-a6cb-b2c738a68da1");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "d447a0ed-bc00-4fc1-9c0a-07c084e35396", "AQAAAAEAACcQAAAAEBJ5I4NDcjI/gRNLJG49/I+fPEbRLyukKLbY1mNCj28W6nuLtII8t/JJpxkaSkbRvA==" });
        }
    }
}
