﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class Systemvariablesupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "2a135640-a6c4-4f5a-9573-a105df20356f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "59667eef-130c-4208-b868-9d867ec0bde0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "853414f7-2303-4d4c-a52a-b41d575567e0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "ae5dbeb6-91bb-451e-ba65-a6d2a0662dac", "AQAAAAEAACcQAAAAECSImXpBBJdO1n4Yq3lG1uthuPw4GWycWYa3cTCdTOk2utpcycnR965ce0dcy9BVNw==" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 8,
                column: "Value",
                value: "0.5");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 9,
                column: "Value",
                value: "0.25");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Name", "Value" },
                values: new object[] { "ProjectPersonalityPercentage", "0.2" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Name", "Value" },
                values: new object[] { "ProjectTagsPercentage", "0.3" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Name", "Value" },
                values: new object[] { "ProjectComplexityPercentage", "0.3" });

            migrationBuilder.InsertData(
                table: "SystemVariables",
                columns: new[] { "Id", "Name", "Value" },
                values: new object[,]
                {
                    { 20, "ProjectSupervisorStylePercentage", "0.2" },
                    { 21, "SupervisorStylePercentage", "0.25" },
                    { 22, "SupervisorStyleScore0", "1.0" },
                    { 23, "SupervisorStyleScore1", "0.5" },
                    { 24, "SupervisorStyleScore2", "0.0" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "daecfb2f-8887-445e-888a-70b84ec1ecf4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "aa42c239-a0da-44ab-a5d8-6f60c66df26c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "ae4123f5-1a62-4ce9-8754-0241bfb8f031");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "39219d4c-d29b-415f-9303-55b0c9fe6698", "AQAAAAEAACcQAAAAEFaFSqylN7yftDKrj2ojW8yHeW8Ji2JJZotnVpsW3m5QkebfObWckS2ifK7mT8It1A==" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 8,
                column: "Value",
                value: "0.7");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 9,
                column: "Value",
                value: "0.3");

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Name", "Value" },
                values: new object[] { "ProjectPersonalityProcentage", "0.4" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Name", "Value" },
                values: new object[] { "ProjectTagsProcentage", "0.4" });

            migrationBuilder.UpdateData(
                table: "SystemVariables",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Name", "Value" },
                values: new object[] { "ProjectComplexityProcentage", "0.2" });
        }
    }
}
