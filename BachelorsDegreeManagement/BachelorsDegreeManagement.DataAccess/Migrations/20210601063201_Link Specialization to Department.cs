﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BachelorsDegreeManagement.DataAccess.Migrations
{
    public partial class LinkSpecializationtoDepartment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Specializations",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "4df13dda-79c5-420c-b067-2b5770b367d3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "13989092-74e7-4b99-8376-1c4a303b56f8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "c0847727-fa63-4fbc-9e28-9bdef41a4a4f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "803a60c1-a54c-4f57-935e-6568f8d71ed4", "AQAAAAEAACcQAAAAEKrgieu6V4Taw/HIMGUUtKaFaY5pZTmF5PHjMjPZ8S8G/gbE5sj1Aa+2ZVhfMF5R5w==" });

            migrationBuilder.CreateIndex(
                name: "IX_Specializations_DepartmentId",
                table: "Specializations",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Specializations_Departments_DepartmentId",
                table: "Specializations",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Specializations_Departments_DepartmentId",
                table: "Specializations");

            migrationBuilder.DropIndex(
                name: "IX_Specializations_DepartmentId",
                table: "Specializations");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Specializations");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e571",
                column: "ConcurrencyStamp",
                value: "21b20df6-c21e-4ae1-936b-ee3815654d36");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e572",
                column: "ConcurrencyStamp",
                value: "3faca45a-2610-4706-a8e6-6c955a30a71e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e573",
                column: "ConcurrencyStamp",
                value: "4e4dfdc0-1c31-4cff-abaa-65d295729270");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "33221f44-5d06-423f-a6a8-8467fcd2b8d9", "AQAAAAEAACcQAAAAEErJueMu+Ei9++86wp3zMQqpiYVNV8j2oGL6rdrzSNy9KoWDkvrG8O0Mkkc2PnHojA==" });
        }
    }
}
